# Summerstudent project

Git for summerstudent project on evolution of open cluster (see project description)

A manual to run the code and get output in correct format is present: `nbody6_quick_guide.md`.

# TODO list: 
## Discussed with Imran:
- [ ] Fix workplace
- [x] Get user accounts
- [ ] Get cluster access
- [ ] Be clear about the project in general and the goals
- [ ] Find clear reading material on:
    - [ ] Globular clusters 
    - [ ] Young/open clusters
    - [ ] stellar evolution/stellar formation
- [x] Make a slack channel
- [ ] Be clear about coding goals
- [ ] Think of some hacks that can be fun to implement
- [ ] Play with code to get a feeling of possibilities


## Practicalities
- [ ] Get nbody6 working and get to know the code
  - [x]  Make appointment with max and go through things
- [x] Make it fairly easy for the student to work with nbody6
  - [xx] make output scripts
  - [x] Create environment to run things on. i.e. go to IT
- [x] Ask someone to send us a couple of review papers on clusters
- [x] Write up a more solid form of the project description
- [ ] Stay in contact with Mark and Fabio, keep them in the loop of any further advice. Also discuss with them semi regularly about the topic. 
- [x] Create quickstart manual for us
- [x] Try out mcluster for the setup of Nbody simulation settings. They have a nice manual
- [x] Fix script for mcluster to put everything in a nice place with the logs
- [ ] Deprecated: Finish the python config generator.

## Scientific
- [ ] Decide upon the following scientific things:
  - [ ] Total mass: What range of cluster masses/Total number of stars will we decide to evolve?
  - [ ] Density profile: What will be the density profile that is the most sensible? See paper of Douglas and Heggie for inspiration
  - [ ] Tidal field: Vary between no tidal field and tidal field? Think about this
  - [ ] Binary fraction: Binary fraction for stars, and will we do a uniform one or mass dependant?
  - [ ] Eccentricity distribution: Thermal is fine, but flat is also okay i think. 
  - [ ] Period distribution: Sana 2012 if possible.
  - [ ] Initial mass function: Lets decide to start by using the standard kroupa for now, no reason to do so otherwise, however choosing single mass is maybe good as a reference to other work.
    - [ ] Mass limits: Decide which mass limits to take. Is 100 solarmass enough?
  - [ ] Mass Segregation: I think no initial segregation, because we want to see, with an initial cluster, how the segregation occurs. But i am not sure if forming clusters have any segregation at all?
  - [ ] Fractality: This is to simulate the imperfectness of the spatial distribution of stars at formation. I wonder how this can affect the outcomes.
  - [ ] Stellar evolution settings:
    - [ ] Use pre-evolved setting with cluster, might be fun to test out the paper via a shortcut
