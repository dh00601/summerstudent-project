#!/bin/bash

#####
# Script to execute mcluster to set up inital settings for a cluster.
# Useage: run the script from anywhere . Change the values of NAME_RUN, MCLUSTER_COMMANDS and TARGET_DIR to your liking. After running this script you can use the output to feed it to nbody6.
# Input: name of run
# 17-05-2019 changing the target directory to the directory that this script is being executed from
######

# Dont change these settings

# Check if a name is supplied
if [ $# -gt 0 ]; then
    NAME_RUN=$1
else
    NAME_RUN="default"
fi
echo "Name run: $NAME_RUN"

MCLUSTER_CONFIG_DIR=$PWD
#MCLUSTER_CONFIG_DIR='/vol/ph/astro_code/dhendriks/work_dir/mcluster'
MCLUSTER_EXECUTABLE='/user/HS103/m13239/Desktop/mcluster/mcluster'

# Change these settings
TARGET_DIR=$MCLUSTER_CONFIG_DIR

# Store settings for mcluster in here.
MCLUSTER_COMMANDS="-C5 -M 5000 -R 1 -P 0 -f 1 -m 0.08 -m 150 -B 0 -Z 0.001 -o"
#MCLUSTER_COMMANDS="-C5 -M 5000 -R 0.5 -P 0 -f 1 -m 0.08 -m 150 -B 0 -Z 0.001 -o"
#MCLUSTER_COMMANDS="-C5 -M 50000 -R 2 -P 0 -f 1 -m 0.08 -m 150 -B 0 -Z 0.0001 -o"
#MCLUSTER_COMMANDS="-C5 -M 50000 -R 2 -P 1 -W 3.0 -f 1 -B 0 -o"

echo "Writing files to $MCLUSTER_CONFIG_DIR"

# Execute mcluster cmd
$MCLUSTER_EXECUTABLE $MCLUSTER_COMMANDS $NAME_RUN | tee $MCLUSTER_CONFIG_DIR/$NAME_RUN.summary

# Write command to file
echo "$MCLUSTER_EXECUTABLE $MCLUSTER_COMMANDS $NAME_RUN | tee $MCLUSTER_CONFIG_DIR/$NAME_RUN.summary" > $MCLUSTER_CONFIG_DIR/command.txt

# Rename fïles:
if [ -f "$MCLUSTER_CONFIG_DIR/$NAME_RUN.input.fort.10" ]; then 
    mv -f $MCLUSTER_CONFIG_DIR/$NAME_RUN.input.fort.10 $MCLUSTER_CONFIG_DIR/fort.10
fi

if [ -f "$MCLUSTER_CONFIG_DIR/$NAME_RUN.input.fort.12" ]; then 
    mv -f $MCLUSTER_CONFIG_DIR/$NAME_RUN.input.fort.12 $MCLUSTER_CONFIG_DIR/fort.12
fi

if [ -f "$MCLUSTER_CONFIG_DIR/$NAME_RUN.dat.10" ]; then 
    mv -f $MCLUSTER_CONFIG_DIR/$NAME_RUN.dat.10 $MCLUSTER_CONFIG_DIR/dat.10
fi
