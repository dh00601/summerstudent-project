# Summer project: Black hole retention in star clusters:
## Description:
Black holes in clusters of stars play a significant role in the evolution of these clusters and depending on how many there are at the start of the evolution of the cluster, the cluster may evolve to end up with as little as 0% of its mass fraction composed of black holes or up to nearly 100% of its mass fraction composed of black holes. 
This in turn has implications for the possibility of intermediate black holes and further cluster evolution.

How many black holes there are at the early stages of the cluster evolution, also called black hole retention fraction, depends on several factors, among which are the cluster properties (e.g.: initial density and mass) and stellar evolution (e.g.: initial-final mass relation for black holes, kick distributions). Our plan is to to use some cluster evolution models to probe how sensitive the outcome is to certain parameter variations like the kick distributions of black holes. 

We will use a code called NBODY6  evolve the cluster, so you will learn how to deal with running codes and processing the output of this. Other skills like Python and LateX will be useful as well.

To run the code with different setting we shall use mcluster to generate these different initial setting files


## Student:
Sergi Pradas

## Period:
1 july - 24 August 

## Meetings:
Meetings will be on monday and thursday 12 o clock. 
Monday will be a bit more extensive. 
Thursday will be just to make sure things are going smooth through the week.


## Week to week plan:
### Week 1 (1-7 July): Getting into the theory:
- [ ] Reading into Globular clusters and Open/Young clusters
- [ ] Reading into stellar evolution
- [ ] Get familiar in the group
- [ ] Look at python (see coding tab)

### Week 2 (8-14 July): continuing theory and doing some calculations:
- [ ] Get more into stellar evolution
- [ ] Read into stellar formation
- [ ] Get familiar with concepts like initial mass function, black hole formation
- [ ] Calculate 'goldilock zone' for cluster (calculating escape velocity based on cluster parameters to see which parameters allow for escaping of black holes)
- [ ] Calculate, using the IMF and a range of masses, how many black holes will form in a cluster of a given mass.

### Week 3 (15-21 July): Starting up some simulations:
- [ ] Using the results of last week, decide on initial conditions of the cluster that we will evolve.
- [ ] Read a bit about the N-Body6/7 code 
- [ ] Set up some initial simulations and get the output

### Week 4 (22-28 July): Work on simulations and scripts to analyse:
- [ ] Do simulation of some clusters
- [ ] Start with analysing results of the first simulations 
- [ ] Make some scripts to analyse the data, try to interpret it and understand it

### Week 5 (29 July - 4 Aug):
- [ ] Run some higher resolution simulations
- [ ] Possibly set up a very big simulation
- [ ] Analyse the results

### Week 6 (5-11 Aug): Play with extremes:
- [ ] Decide whether to do more simulations
- [ ] Modify some physics and try out some hacks  (e.g. putting some initial black holes in the cluster)

### Week 7 (12-18 Aug): Wrapping up project:
This is coming to the end but its also quite far away, so the goals here are a bit general
- [ ] Analyse data from calculations
- [ ] Write up results and write up a form of report 

### Week 8 (19-24 Aug):
- [ ] Same as week 7

## Coding
We will need some programming language to analyze the data, as well as using a simulation tool to generate the data.

The simulation tool will be NBODY6(++/7)

The analysis tootl will be python.
The most important python packages in this case will be the following:
* `matplotlib` (For plotting)
* `numpy` (For numerical operations mostly)
* `scipy` (For statistical tools)
* `HDF5` (to read out the files)
* `pandas` (For data manipulation this tool is quite nifty)
* Common things like `sys`, `os`, 

## Reading material:
### Introductionary reading material:
* https://www.ph.unimelb.edu.au/~mtrenti/Site/slides/StarClusters_Lecture1.pdf : Introductionary text on star clusters. Goes into the demography of clusters and their properties
* https://www.ast.cam.ac.uk/~vasily/Lectures/SDSG/sdsg_7_clusters.pdf : Same but a bit more indepth on globular clusters
* http://www.astro.caltech.edu/~george/ay20/eaa-globcl.pdf : Introduction on globular clusters from an observational point of view
* http://www.thijskouwenhoven.net/nbody.html : Website with lecutre notes on nbody simulations and the nbodyX software
* Reading material in materials/ dir
 
### In depth reading material
* [http://arxiv.org/abs/1902.07718](http://arxiv.org/abs/1902.07718): Paper on the implementation of new physics prescriptions into NBODY6. Shows some interesting results on BH retention
* https://www.sns.ias.edu/tremaine/lectures/ast513/globular : Overview of indepth papers on globular clusters
* [Website containing some lectures on N-body integration](http://silkroad.bao.ac.cn/web/index.php/seminars/lectures)
* https://arxiv.org/pdf/1708.09530.pdf : Nice paper on the initial mass function of stars in globular clusters, and how black hole retention plays a role in mass segregation
* https://arxiv.org/pdf/1712.03979.pdf : Paper on observational signatures of black hole populations in globular clusters
* https://arxiv.org/abs/1711.09100 : paper on the evolution of kicked black holes; what does the surrounding environment do to influence where they end up after a kick?
* http://articles.adsabs.harvard.edu/cgi-bin/nph-iarticle_query?1991ARA%26A..29..543H&amp;data_type=PDF_HIGH : observational paper on cluster properties: Catalogue of known clusters. This is a popular paper which contains a collection of the then known GCs in the local group.
* http://epubs.surrey.ac.uk/812065/1/MNRAS-2016-Peuten-2333-42.pdf : Paper which tries to determine whether a specific GC has a BH population, based on analyzed properties compared to nbody simulations
* https://arxiv.org/abs/1906.11855 : (!) Paper just released which contains part of our project goal. Quite an indepth paper which covers a lot of material that will be of interest to us.

### Other:
* http://adsabs.harvard.edu/abs/2013MNRAS.432.2779B : Paper where we got the inspiration for the project. Important paper for the general idea, but difficult in theoretical level

## Some watching material:
* https://www.cita.utoronto.ca/presentation-archive/?talk_id=745
* https://www.cita.utoronto.ca/presentation-archive/?talk_id=768
* http://online.kitp.ucsb.edu/online/bholes_c13/sigurdsson/
* https://www.cita.utoronto.ca/presentation-archive/?talk_id=646
* http://online.kitp.ucsb.edu/online/gravast_c19/mapelli/rm/jwvideo.html !

## Contact:
[David Hendriks](mailto:d.hendriks@surrey.ac.uk)
[Imran Nasim](mailto:i.nasim@surrey.ac.uk)
