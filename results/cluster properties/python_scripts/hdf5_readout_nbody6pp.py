import h5py
import os
from extra_functions import *

source_dir = '/user/HS103/m13239/Desktop/results_nbody6'
filename = 'snap.40_0.h5part'

f = h5py.File(os.path.join(source_dir, filename), 'r')

# For a tree view of the file
#for (path, dset) in hdf_recursive(f):
#    print(path, dset)

# # List all groups
#print("Keys: %s" % f.keys())

#a_group_key = list(f.keys())[0]
#print(a_group_key)

# Get the data of e.g. step 0
#data_step0 = list(f['Step#0'].keys())
data_step0 = list(f['Step#0'])
#print(data_step0)

# get the data of e.g. binaries in step 0
data_step0_binaries = list(f['Step#0']['Binaries'])
#print(data_step0_binaries)

# get the data of e.g. the eccentricity of binaries in step 0
#data_step0_binaries_ecc = list(f['Step#0']['Binaries']['ECC'])
#print(data_step0_binaries_ecc)

data_step0_binaries_m1 = list(f['Step#0']['Binaries']['M1'])
print(data_step0_binaries_m1)

