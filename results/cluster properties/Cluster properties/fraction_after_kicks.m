%% FUNCTION THAT GIVES THE MASS AND NUMBER FRACTION OF BH AFTER SUPERNOVA KICKS
%Input:
%{
We coud set:
      Z:The metallicity will let us obtain M_zams_min if we consider M_bh_min=4.4
        We get this value from Farr et al. 2011. (They say between 4.3 and
        4.5 with 90% confidence.
But the SEVN code cites M_bh_min as 3, and we have to be consistent with
that.
%}
%      alfa1 and alfa2: in order to use the IMF.
%      k: parameter that defines whether there is fallback or not. 
%        k=0 ==> no fallback
%        k=1 ==> fallback considered
%Output:
%      f_N: number fraction of BHs that remain after supernova kicks.
%      f_M: mass fraction of BHs that remain after supernova kicks.
function [M_z_min,f_N,f_m]=fraction_after_kicks(Z,alfa1,alfa2,alfa3,v_esc,k)
vec_M_zams=linspace(7,40,2000);
vec_M_bh=fun_M_rem(Z,vec_M_zams);
%Then we find the value for M_zams that gives M_bh=3.5:
kk=find(abs(vec_M_bh-3)<1e-2);
M_z_min=vec_M_zams(kk(1));
%But we need to take into account the retention fraction, that is, we need
%to compute the probability of escaping, so p_ret=1-p_esc.
sig_ns=265;M_ns=1.4;
fun= @(M) (M<0.08).*((M.^(-alfa1)).*(1-feval(@prob_escape,Z,M,M_ns,sig_ns,v_esc,k))) + (M<0.5 & M>=0.08).*((M.^(-alfa2)).*(1-feval(@prob_escape,Z,M,M_ns,sig_ns,v_esc,k))*(0.08)) + (M>=0.5).*((M.^(-alfa3)).*(1-feval(@prob_escape,Z,M,M_ns,sig_ns,v_esc,k))*(0.5));
%This way we can compute the number fraction:
%fun is the imf times the retention probability.
q1=integral(fun,M_z_min,150,'RelTol',1e-6,'AbsTol',1e-6);
%q1=integral(feval(@p_times_imf,Z,alfa1,alfa2,M,M_ns,sig_ns,v_esc),M_z_min,150,'RelTol',1e-6,'AbsTol',1e-6);
fun2= @(M) (M<0.08).*(M.^(-alfa1)) + (M<0.5 & M>=0.08).*((M.^(-alfa2))*(0.08)) + (M>=0.5).*((M.^(-alfa3))*(0.5));
%But we also need to take into account the retention fraction for neutron
%stars(NS). So we need to find M_z_min_ns and M_z_max_ns corresponding to a
%minimum mass given by M_zams=7 and a maximum mass given by 3.
M_z_min_ns=vec_M_zams(1);
f_ret_ns=1-feval(@prob_esc_ns,sig_ns,v_esc);
fun_ns= @(M) (M<0.08).*((M.^(-alfa1))*f_ret_ns) + (M<0.5 & M>=0.08).*((M.^(-alfa2))*f_ret_ns*(0.08)) + (M>=0.5).*((M.^(-alfa3))*f_ret_ns*(0.5));
q2_1=integral(fun2,0.07,M_z_min_ns,'RelTol',1e-6,'AbsTol',1e-6);
q2_2=integral(fun_ns,M_z_min_ns,M_z_min,'RelTol',1e-6,'AbsTol',1e-6);
q2_3=integral(fun,M_z_min,150,'RelTol',1e-6,'AbsTol',1e-6);
f_N=q1/(q2_1+q2_2+q2_3);

%Defining fun2 we obtain the BH  mass as a function of the initial total
%mass (or total number of stars) of the cluster.

%Now, in order to compute the mass fraction we have to multiply the previous function by
%the mass. Meaning:
alfa1=alfa1-1;alfa2=alfa2-1;alfa3=alfa3-1;
%Here fun refers to the imf times the mass times the retention probability(as we are looking for the mass
%fraction).
fun= @(M) (M<0.08).*((M.^(-alfa1)).*(1-feval(@prob_escape,Z,M,M_ns,sig_ns,v_esc,k))) + (M<0.5 & M>=0.08).*((M.^(-alfa2)).*(1-feval(@prob_escape,Z,M,M_ns,sig_ns,v_esc,k))*(0.08)) + (M>=0.5).*((M.^(-alfa3)).*(1-feval(@prob_escape,Z,M,M_ns,sig_ns,v_esc,k))*(0.5));
q1=integral(fun,M_z_min,150,'RelTol',1e-6,'AbsTol',1e-6);
fun2= @(M) (M<0.08).*(M.^(-alfa1)) + (M<0.5 & M>=0.08).*((M.^(-alfa2))*(0.08)) + (M>=0.5).*((M.^(-alfa3))*(0.5));
fun_ns= @(M) (M<0.08).*((M.^(-alfa1))*f_ret_ns) + (M<0.5 & M>=0.08).*((M.^(-alfa2))*f_ret_ns*(0.08)) + (M>=0.5).*((M.^(-alfa3))*f_ret_ns*(0.5));q2_1=integral(fun2,0.07,M_z_min_ns,'RelTol',1e-6,'AbsTol',1e-6);
q2_2=integral(fun_ns,M_z_min_ns,M_z_min,'RelTol',1e-6,'AbsTol',1e-6);
q2_3=integral(fun,M_z_min,150,'RelTol',1e-6,'AbsTol',1e-6);
f_m=q1/(q2_1+q2_2+q2_3);
%}
end

