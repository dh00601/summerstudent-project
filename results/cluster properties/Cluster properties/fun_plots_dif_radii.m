%% FUNCTION THAT COMPUTES THE RETENTION FRACTION AND THE MASS FRACTION FOR DIFFERENT RADII AND A MASS CHOSEN.
%{
Inputs:
       M_cl: Mass of the cluster (in solar masses)
       Z: Metallicity
Output:
        plots the retentrion fraction and mass fraction as functions of the
        half-mass radius for the cluster mass chosen.

IMPORTANT: THIS CODE ONLY WORKS FOR A SCALAR M_cl and Z.
For more information on the sampling and how we compute the retained stars see SAMPLING.m
%}

function f_M_bh=fun_plots_dif_radii(M_cl,Z)
load('cpdf')
vec_m_initial=[];%We will use this matrix to save the different masses. We initialize this variable so that we store every mass vector in a different row:
vec_r_h=linspace(0.1,15,150);
tic
%And then we can sample the IMF:
        M_tot_initial=0;
        jj=1;%We initialize this variable before starting the while loop.
  while M_tot_initial<M_cl
    r=rand;
    kk=find(abs(eval_fun-r)<1e-3);
    m=vec_M_examples(kk(randi(length(kk))));
    M_tot_initial=M_tot_initial+m;
    vec_m_initial(jj)=m;% As it can be seen we store in each row the masses of a given sampling (for a given cluster mass)
    jj=jj+1;
  end  
N_tot_initial=length(vec_m_initial);%In this vector we save the total number of stars.
%We now have to define the minimum M_zams for NS and BHs:
vec_M_zams_test=linspace(7,40,2000);
vec_M_bh_test=fun_M_rem(Z,vec_M_zams_test);
ll=find(abs(vec_M_bh_test-3)<1e-2);
M_z_min_bh_test=vec_M_zams_test(ll(1));
%And to find the minimum NS mass: The minimum mass of 1.17 solar masses is
%given by Yudai Suwa, Takashi Yoshida, Masaru Shibata, Hideyuki Umeda & Koh
%Takahashi in "On the minimum mass of neutron stars" in 2009. The problem
%is that we cannot get M_rem=1.17 with the function we obtained via the
%PARSEC paper.That is why we set a minimum value of 1.4:
ll=find(abs(vec_M_bh_test-1.4)<1e-2);
M_z_min_ns_test=vec_M_zams_test(ll(1));

%And then we compute the total number of BHs (stars that will become one):
N_bh_initial=sum(vec_m_initial>M_z_min_bh_test);
toc
f_N_bh_relative=[];f_M_bh=[];
for k=0:2
   if k==0
    kick='Neutrino-driven NK';
   elseif k==1 
    kick='Hybrid model';
   elseif k==2
    kick='Standard-fallback controlled NK';
   end
%Now, we take the values we obtained before and use them to determine which
%stars are retained and which ones are not:

for dd=1:length(vec_r_h)
v_esc=fun_v_esc(vec_r_h(dd),M_cl);   
vec_m_final=[];%We initialize the vector that will store the retained masses.
vec_m_bh_final=[];
tic  
%In order to not use so many if statements we are going to find which
%positions in the vector vec_m_initial have a mass smaller than the minimum
%neutron star mass. Because this stars are retained and considering them in
%an if statement only slows the computation:
pos_stars=vec_m_initial<M_z_min_ns_test;%Positioon of such stars in the vector vec_m_initial. 
%(we get 1's in the positions of the stars that verify the requirement, and 0 for the others)
vec_m_final_i=vec_m_initial.*pos_stars;
%But we need to eliminate the 0's and only consider those that are
%different  from 0.
vec_m_final=nonzeros(vec_m_final_i)';
%And the vector of masses for the NS and BHs before considering the
%probabilities for them to be kicked out are:
vec_bh_ns=vec_m_initial-vec_m_final_i;
vec_bh_ns=nonzeros(vec_bh_ns)';%Vector that contains the initial stars that are (or will be, basically, as these are ZAMS masses) BHs or NSs.
%And now we do an if statement for this stars:
ll=length(vec_m_final); %We initialize ll so that we fill the vector after that position.
%We calculate only once the probability for the initial vector of masses:
    f_ret=(1-feval(@prob_escape,Z,vec_bh_ns,1.4,265,v_esc,k));
    f_ret_ns=(1-feval(@prob_esc_ns,265,v_esc));
    %As it can be seen by default we choose
    %a velocity dispersion of 265km/s and a mean neutron star mass of 1.4
    %solar masses.
%It is also important to see that later, in order to really compute the
%number of BHs and NSs we need to create a vector that stores the BHs and
%neutron stars:
vec_bh_ns_final=[];
%For that we initialize the variable nn
nn=1;
%But we need to transform this into the masses that the remnant stars will
%really have (not the ZAMS masses that the BHs or NSs previously had):
vec_bh_ns=fun_M_rem(Z,vec_bh_ns);
     for jj=1:length(vec_bh_ns)
         if vec_bh_ns(jj)>3
            rr=rand;
            if rr<=f_ret(jj)
                vec_m_final(ll)=vec_bh_ns(jj);
                vec_bh_ns_final(nn)=vec_bh_ns(jj);
                ll=ll+1;nn=nn+1;
             else
                ll=ll;nn=nn;
             end
         else
            rr=rand;
            if rr<=f_ret_ns
                vec_m_final(ll)=vec_bh_ns(jj);
                vec_bh_ns_final(nn)=vec_bh_ns(jj);
                ll=ll+1;nn=nn+1;
            else
                ll=ll;nn=nn;
            end
         end
     end
     %And now we store the values for the fraction of BHs retained in the
     %cluster after considering the supernova kicks
    N_bh_final(dd)=sum(vec_bh_ns_final>3);
    f_N_bh_relative(dd,k+1)=N_bh_final(dd)./N_bh_initial;
    %And to obtain the mass fractions:
    vec_m_bh_final=vec_bh_ns_final(find(vec_bh_ns_final>3));
    f_M_bh(dd,k+1)=sum(vec_m_bh_final)/(M_cl);
   toc
end
   figure(1)
   h1(k+1)=plot(vec_r_h,f_N_bh_relative(:,k+1));
   title(['NUMBER FRACTION OF BH RETAINED. M_{cl} ' num2str(M_cl) ' M_{sun} & Z=' num2str(Z)])
   xlabel('r_{h} (pc)')
   ylabel('Retention fraction')
   hold on
   figure(2)
   h2(k+1)=plot(vec_r_h,f_M_bh(:,k+1));
   title(['MASS FRACTION OF BH RETAINED. M_{cl} ' num2str(M_cl) ' M_{sun} & Z=' num2str(Z)])
   xlabel('r_{h} (pc)')
   ylabel('Mass fraction')
   hold on
end
legend(h1,{'Neutrino-driven NK','Hybrid model','Standard fallback-controlled NK'});
legend(h2,{'Neutrino-driven NK','Hybrid model','Standard fallback-controlled NK'});
end
