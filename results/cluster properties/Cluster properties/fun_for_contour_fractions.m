%% ONLY USABLE AFTER THE USE OF A MESHGRID
%% FUNCTION THAT WILL GIVE US WHAT WE NEED IN ORDER TO FORM THE CONTOUR PLOT:
%In this case we only want to consider the number fraction of black holes,
%but not compared to the current cluster, but compared to the initial
%number of black holes in such cluster. That is, compare the number of
%black holes after kicks (w/ or w/o fallbkack) to the numbers obtained via
%the sampling of the IMF.
%

%For more information on the processes followed see sampling.m
function f_N_bh_relative=fun_for_contour_fractions(M_cl,Z,vec_v_esc)
%We start by sampling the IMF. And for that we need to load the cpdf
%(cumulative probability density function):
load('cpdf')
load('precomp_probs')
%And then we can sample the IMF:
jj=1;%We initialize this variable before starting the while loop.
vec_m_initial=[];%We will use this vector to save the different masses.
for dd=1:length(vec_v_esc(1,:))
for ff=1:length(vec_v_esc(:,1))
        %M_cl_it=M_cl(dd,ff);
        M_tot_initial=0;
tic
while M_tot_initial<M_cl(dd,ff)
    r=rand;
    kk=find(abs(eval_fun-r)<1e-3);
    m=vec_M_examples(kk(randi(length(kk))));
    M_tot_initial=M_tot_initial+m;
    vec_m_initial(jj)=m;
    jj=jj+1;
end  
toc
N_tot_initial(dd,ff)=length(vec_m_initial);
%We now have to define the minimum M_zams for NS and BHs:
vec_M_zams_test=linspace(7,40,2000);
vec_M_bh_test=fun_M_rem(Z,vec_M_zams_test);
ll=find(abs(vec_M_bh_test-3)<1e-2);
M_z_min_bh_test=vec_M_zams_test(ll(1));
%And to find the minimum NS mass: (The minimum mass of 1.17 solar masses is
%given by Yudai Suwa, Takashi Yoshida, Masaru Shibata, Hideyuki Umeda & Koh
%Takahashi in "On the minimum mass of neutron stars" in 2009. The problem
%is that we cannot get M_rem=1.17 with the function we obtained via the
%PARSEC paper.That is why we set a minimum value of 1.4)
ll=find(abs(vec_M_bh_test-1.4)<1e-2);
M_z_min_ns_test=vec_M_zams_test(ll(1));

%And then we compute the total number of stars of each of them:
N_bh_initial(dd,ff)=sum(vec_m_initial>M_z_min_bh_test);

%The important thing to consider in this case is that we are not interested
%in storing the different masses, only the fraction of BHs. This helps a
%lot because it means that do not have to worry about storing all those
%variables.
f_N_bh=[];
vec_m_final=[];
    ll=1;%We initialize this and then change the value by one if the star is retained.
    tic
    %We calculate only once the probability for the initial vector of masses:
    f_ret=(1-feval(@prob_escape,Z,vec_m_initial,1.4,265,vec_v_esc(dd,ff),1));
    for jj=1:length(vec_m_initial)        
        if m<M_z_min_ns_test
            vec_m_final(ll)=vec_m_initial(jj);
            ll=ll+1;%Because it is always retained.
        elseif m>M_z_min_bh_test
            %k=1 ==>with fallback
            %f_ret=(1-feval(@prob_escape,Z,m,1.4,265,vec_v_esc(dd,ff),1));
            %We now generate a random number, if it is below f_ret it is
            %retained, if not it is kicked out of the cluster.
            rr=rand;
            if rr<=f_ret(jj)
                vec_m_final(ll)=vec_m_initial(jj);
                ll=ll+1;
            else
                ll=ll;
            end
        else
            %Again we compute the probability of it being retained:
            %f_ret=(1-feval(@prob_esc_ns,265,v_esc(dd,ff)));
            rr=rand;
            if rr<=(1-feval(@prob_esc_ns,265,vec_v_esc(dd,ff)))
                vec_m_final(ll)=vec_m_initial(jj);
                ll=ll+1;
            else
                ll=ll;
            end
        end
    N_bh_final(dd,ff)=sum(vec_m_final>M_z_min_bh_test);
    f_N_bh_relative(dd,ff)=N_bh_final(dd,ff)./N_bh_initial(dd,ff);
   
    end
    toc
end
end
end
