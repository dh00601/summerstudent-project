%% FALLBACK FRACTION (f_fb) AS A FUNCTION OF Z AND M_zams
%{
Description:
             This function computes the fallback fraction according to the
             results presented by Spera, Mapelli & Bressan (2015), and Fryer
             et al. (2012) for the delayed supernova mechanism.
%}
%Input:
%      Z:Metallicity
%      M_zams:Zero age main sequence mass (in solar masses)

%Output:
%      f_fb: Fallback fraction
%      M_fb: Fallback mass (in solar masses)
function f_fb=fun_fb(Z,M_zams)
%We start by computing the M_co via fun_M_co.m
vec_M_co=fun_M_co(Z,M_zams);
%We then compute the value of M_proto:
M_proto=zeros(1,length(vec_M_co)); 
for ii=1:length(vec_M_co)
    M_co=vec_M_co(ii);
if M_co<3.5 
    M_proto(ii)=1.2;
elseif (3.5<=M_co)&(M_co<6.0)
    M_proto(ii)=1.3;
elseif (6.0<=M_co)&(M_co<11.0)
    M_proto(ii)=1.4;
else
    M_proto(ii)=1.6;
end
%Now that we have M_proto we can compute a2 and b2:
a2=(0.133)-((0.093)./(M_zams(ii)-M_proto(ii)));
b2=-(11)*a2+1;
%And then, in order to obtain f_fb:
if M_co<2.5
    M_fb(ii)=0.2;
    f_fb(ii)=0;%I don't know if this is what I have to do
elseif (2.5<=M_co)&(M_co<3.5)
    M_fb(ii)=0.5*M_co-1.05;
    f_fb(ii)=0;%I don't know if this is what I have to do
elseif (3.5<=M_co)&(M_co<11.0)
    f_fb(ii)=a2.*M_co+b2;
    M_fb(ii)=f_fb(ii).*(M_zams(ii)-M_proto(ii));
else
    f_fb(ii)=1.0;
    M_fb(ii)=f_fb(ii).*(M_zams(ii)-M_proto(ii));
end
end
end
    
    