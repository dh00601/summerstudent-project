%% FUNCTION THAT GIVES THE PROBABILITY OF ESCAPE FOR A BLACK HOLE (BH)
%{
Description:
             This function computes the probability of escape for a given
             BH, according to the kick prescription, considering a
             Maxwellian distribution.
%}
%Input:
%                Z: Metallicity
%                M_zams: Zero-age main sequence mass (in solar masses) 
%                M_ns: neutron star mass (in solar masses)
%                sig_ns: neutron star velocity dispersion (in km/s)
%                v_esc: escape velocity (in km/s)
%                k: parameter that defines the kick prescription:
%                       k=0 ==> neutrino-driven NK
%                       k=1 ==> hybrid model
%                       K=2 ==> standard kick

%Output:
%                p: probability of v>v_esc
function p=prob_escape(Z,M_zams,M_ns,sig_ns,v_esc,k)
%In order to compute the BH mass we make use of fun_M_rem.m that gives the
%mass of the stellar remnant as a function of Z and M.
M_bh=fun_M_rem(Z,M_zams);
%And then we can obtain the BH's velocity dispersion and the fallback
%depending on the prescription:
f_fb=fun_fb(Z,M_zams);
%And we have to take into account that the probability of escape is 0 if
%sigma is 0, so we have to distinguish between two cases due to the
%fallback fraction reaching one. To do that we define a M_zams_crit using
%the vec_M_examples from cpdf.mat so that it does not depend on the cluster
%generated.
load('cpdf');
M_bh_examples=fun_M_rem(Z,vec_M_examples);f_fb_examples=fun_fb(Z,vec_M_examples);
ll=find(abs(f_fb_examples-0.99)<1e-2); M_zams_crit=vec_M_examples(ll(1));
sig_bh=zeros(1,length(M_zams));
for ii=1:length(M_zams)
 if M_zams(ii)<M_zams_crit
   if k==1
    sig_bh(ii)=(M_ns./M_bh(ii)).*(1-f_fb(ii)).*sig_ns;
    p(ii)=sqrt(2/pi)*(v_esc./sig_bh(ii)).*exp(-(v_esc.^2)./(2.*(sig_bh(ii)).^2))+(1-erf(v_esc./(sqrt(2).*sig_bh(ii))));
   elseif k==0
    sig_bh(ii)=(M_ns./M_bh(ii)).*sig_ns;
    f_fb(ii)=0;
    p(ii)=sqrt(2/pi)*(v_esc./sig_bh(ii)).*exp(-(v_esc.^2)./(2.*(sig_bh(ii)).^2))+(1-erf(v_esc./(sqrt(2).*sig_bh(ii))));
   elseif k==2
    sig_bh(ii)=(1-f_fb(ii)).*sig_ns;
    p(ii)=sqrt(2/pi)*(v_esc./sig_bh(ii)).*exp(-(v_esc.^2)./(2.*(sig_bh(ii)).^2))+(1-erf(v_esc./(sqrt(2).*sig_bh(ii))));
   end 
 else 
   if k==1
     sig_bh(ii)=(M_ns./M_bh(ii)).*(1-f_fb(ii)).*sig_ns;
     p(ii)=0;
   elseif k==0
    sig_bh(ii)=(M_ns./M_bh(ii)).*sig_ns;
    f_fb(ii)=0;
    p(ii)=sqrt(2/pi)*(v_esc./sig_bh(ii)).*exp(-(v_esc.^2)./(2.*(sig_bh(ii)).^2))+(1-erf(v_esc./(sqrt(2).*sig_bh(ii))));
   elseif k==2
    sig_bh(ii)=(1-f_fb(ii)).*sig_ns;
    p(ii)=0;
   end 
 end
end
end

