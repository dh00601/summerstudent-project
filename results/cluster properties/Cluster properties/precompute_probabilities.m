clear all;close all; format compact; format long g;
%% SCRIPT THAT PRECOMPUTES PROBABILITIES.
load('cpdf')
%% Z=2e-2:
Z=2e-2;
vec_v_esc=[10 15 20 25 30 35 40 45 50];
%We need to decide which possible escape velocities we will give:
for ii=1:length(vec_v_esc)
    v_esc=vec_v_esc(ii);
    prob_esc(ii,:)=feval(@prob_escape,2e-2,vec_M_examples,1.4,265,v_esc,1);
    vec_f_ret(ii,:)=1-prob_esc(ii,:);
    vec_f_ret_ns(ii)=1-feval(@prob_esc_ns,265,v_esc);
end
    