%% FUNCTION THAT COMPUTES THE THE MASS HISTOGRAM FOR THE THREE PRESCRIPTIONS AT THE SAME TIME:

function fun_for_mass_histograms_all_kicks(Z,M_cl,r_h)
v_esc=fun_v_esc(r_h,M_cl);
%We start by sampling the IMF. And for that we need to load the cpdf
%(cumulative probability distribution function):

load('cpdf')

vec_m_initial=[];%We will use this vector to save the different masses. We initialize this variable so that we store every mass.
%And then we can sample the IMF:
 M_tot_initial=0;
jj=1;%We initialize this variable before starting the while loop.
  while M_tot_initial<M_cl
    r=rand;
    kk=find(abs(eval_fun-r)<1e-3);
    m=vec_M_examples(kk(randi(length(kk))));
    M_tot_initial=M_tot_initial+m;
    vec_m_initial(jj)=m;% As it can be seen we store in each row the masses of a given sampling (for a given cluster mass)
    jj=jj+1;
  end  
N_tot_initial=length(vec_m_initial);%In this vector we save the total number of stars.
%We now have to define the minimum M_zams for NS and BHs:
vec_M_zams_test=linspace(7,40,2000);
vec_M_bh_test=fun_M_rem(Z,vec_M_zams_test);
ll=find(abs(vec_M_bh_test-3)<1e-2);
M_z_min_bh_test=vec_M_zams_test(ll(1));
%And to find the minimum NS mass: The minimum mass of 1.17 solar masses is
%given by Yudai Suwa, Takashi Yoshida, Masaru Shibata, Hideyuki Umeda & Koh
%Takahashi in "On the minimum mass of neutron stars" in 2009. The problem
%is that we cannot get M_rem=1.17 with the function we obtained via the
%PARSEC paper.That is why we set a minimum value of 1.4:
ll=find(abs(vec_M_bh_test-1.4)<1e-2);
M_z_min_ns_test=vec_M_zams_test(ll(1));

 
%Now, we take the values we obtained before and use them to determine which
%stars are retained and which ones are not:
vec_m_final=[];%We initialize the vector that will store the retained masses. 
%In order to not use so many if statements we are going to find which
%positions in the vector vec_m_initial have a mass smaller than the minimum
%neutron star mass. Because this stars are retained and considering them in
%an if statement only slows the computation:
pos_stars=vec_m_initial<M_z_min_ns_test;%Position of such stars in the vector vec_m_initial. 
%(we get 1's in the positions of the stars that verify the requirement, and 0 for the others)
vec_m_final_i=vec_m_initial.*pos_stars;
%But we need to eliminate the 0's and only consider those that are
%different  from 0.
vec_m_final=nonzeros(vec_m_final_i)';
%And the vector of masses for the NS and BHs before considering the
%probabilities for them to be kicked out are:
vec_bh_ns=vec_m_initial-vec_m_final_i;
vec_bh_ns=nonzeros(vec_bh_ns)';%Vector that contains the initial stars that are (or will be, basically, as these are ZAMS masses) BHs or NSs.

%But we want a vector that contains the initial masses of BHs:

%But we want a vector that contains the initial masses of BHs:
pos_bh=fun_M_rem(Z,vec_bh_ns)>3;
vec_m_bh_initial=fun_M_rem(Z,vec_bh_ns).*pos_bh;
vec_m_bh_initial=nonzeros(vec_m_bh_initial)';
%And now we do an if statement for this stars:
ll=length(vec_m_final); %We initialize ll so that we fill the vector after that position.
%We calculate only once the probability for the initial vector of masses for the different kick prescriptions:
  


%And before computing anything else, we make a histogram for these initial
%masses:
figure(1)
subplot(2,2,1)
h1=histogram(vec_m_bh_initial,35,'FaceColor','k','FaceAlpha',0.6);
h1.BinWidth = 1.5;
title(['Number of BHs for M_{cl}=' num2str(M_cl) ' M_{sun}, r_h=' num2str(r_h) 'pc & Z=' num2str(Z) '. '])
xlabel('M_{BH} (M_{sun})')
ylabel('Number of BHs')
xlim([0 100])
xticks([0 10 20 30 40 50 60 70 80 90 100])
hold on


for k=0:2  
     f_ret(k+1,:)=(1-feval(@prob_escape,Z,vec_bh_ns,1.4,265,v_esc,k)); 
end
%It is also important to see that later, in order to really compute the
%number of BHs and NSs we need to create a vector that stores the BHs and
%neutron stars:
vec_bh_ns_final=[];
vec_m_final=[];

pos_stars=vec_m_initial<M_z_min_ns_test;%Position of such stars in the vector vec_m_initial. 
%(we get 1's in the positions of the stars that verify the requirement, and 0 for the others)
vec_m_final_i=vec_m_initial.*pos_stars;
%But we need to eliminate the 0's and only consider those that are
%different  from 0.
vec_m_final=nonzeros(vec_m_final_i)';
%For that we initialize the variable nn
nn=1;
%But we need to transform this into the masses that the remnant stars will
%really have after the explosion(not the ZAMS masses that the BHs or NSs previously had):
vec_bh_ns=fun_M_rem(Z,vec_bh_ns);


%And now we need to study which are retained and which are not for the
%three different kick prescriptions:
for k=0:2
     for jj=1:length(vec_bh_ns)
         if vec_bh_ns(jj)>3
            rr=rand;
            if rr<=f_ret(k+1,jj)
                vec_m_final(ll)=vec_bh_ns(jj);
                vec_bh_ns_final(nn)=vec_bh_ns(jj);
                ll=ll+1;nn=nn+1;
             else
                ll=ll;nn=nn;
             end
         else
            rr=rand;
            if rr<=(1-feval(@prob_esc_ns,265,v_esc))
                vec_m_final(ll)=vec_bh_ns(jj);
                vec_bh_ns_final=vec_bh_ns(jj);
                ll=ll+1;nn=nn+1;
            else
                ll=ll;nn=nn;
            end
         end
     end
  %And now we store the values of the masses of black holes retained
     %after the kicks:
     vec_m_bh_final=vec_bh_ns_final(find(vec_bh_ns_final>3));
  %And now we plot the histogram for the final masses for the different
  %kick prescriptions on top of the one for the initial masses that we
  %computed above
  figure(1)
  subplot(2,2,k+2)
  h3=histogram(vec_m_bh_initial,35,'FaceColor','k','FaceAlpha',0.6);
  h3.BinWidth = 1.5;
  title(['Number of BHs for M_{cl}=' num2str(M_cl) ' M_{sun}, r_h=' num2str(r_h) 'pc & Z=' num2str(Z) '. '])
  xlabel('M_{BH} (M_{sun})')
  ylabel('Number of BHs')
  xlim([0 100])
  xticks([0 10 20 30 40 50 60 70 80 90 100])
  hold on
  h3=histogram(vec_m_bh_final,35,'FaceAlpha',0.6);
  h3.BinWidth = 1.5;
  hold on
end
%And then the legend:
%legend(h1,{'Neutrino-driven NK','W/ fallback & reduced \sigma','Standard fallback-controlled NK','IMF (before kicks)'})



