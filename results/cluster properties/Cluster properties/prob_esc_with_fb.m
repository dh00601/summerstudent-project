%% FUNCTION THAT GIVES THE PROBABILITY OF v>vesc TAKING INTO ACCOUNT FALLBACK f_fb
%Input:
%                Z: Metallicity
%                M_zams:Zero age main sequence mass (in solar masses)
%                M_ns: neutron star mass (in solar masses)
%                sig_ns: neutron star velocity dispersion (in km/s)
%                v_esc: escape velocity

%Output:
%                p: probability of v>v_esc
function p=prob_esc_with_fb(Z,M_zams,M_ns,sig_ns,v_esc)
%In order to compute the BH mass we make use of fun_M_rem.m that gives the
%mass of the stellar remnant as a function of Z and M_zams.
M_bh=fun_M_rem(Z,M_zams);
%And then we can obtain the BH's velocity dispersion.
sig_bh=(M_ns./M_bh).*sig_ns;
%We define the fallback fraction:
    f_fb=fun_fb(Z,M_zams);
%And then we can compute the probability of escaping:
p=sqrt(2/pi)*(v_esc./sig_bh).*exp(-(v_esc.^2)./(2*((1-f_fb).^2).*(sig_bh).^2))+(1-f_fb).*(1-erf(v_esc./(sqrt(2)*(1-f_fb).*sig_bh)));
end

