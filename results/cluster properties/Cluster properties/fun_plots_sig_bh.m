%% FUNCTION TO OBTAIN THE PLOTS FOR THE VELOCITY DISPERSION FOR DIFFERENT KICKS
%% FUNCTION THAT YIELDS THE VELOCITY DISPERSION FOR THE DIFFERENT KICK PRESCRIPTIONS
%{
Description:
            This function yields the plots for the velocity dispersion for the different
            kick prescriptions.
%}
%Input:
%                Z: Metallicity
%                M_ns: neutron star mass (in solar masses)
%                sig_ns: neutron star velocity dispersion (in km/s)
%                k: parameter that defines the kick prescription:
%                       k=0 ==> neutrino-driven NK
%                       k=1 ==> hybrid model
%                       K=2 ==> standard kick

%Output:
%                plots for the different velocity dispersions according to
%                the kick

function sig_bh=fun_plots_sig_bh(Z,M_ns,sig_ns)
vec_M_zams_test=linspace(7,150,50000);
vec_M_bh_test=fun_M_rem(Z,vec_M_zams_test);
ll=find(abs(vec_M_bh_test-3)<1e-2);
M_zams=vec_M_zams_test(ll(1):end);
M_bh=fun_M_rem(Z,M_zams);
sig_bh=[];
 for k=0:2
    sig_bh(k+1,:)=fun_sigma_bh(Z,M_ns,sig_ns,k);
 end
 figure(1)
 h1(1)=plot(M_bh,sig_bh(1,:));
 title(['VELOCITY DISPERSION. <M_{NS}>= ' num2str(M_ns) ' M_{sun}, \sigma_{NS}=' num2str(sig_ns) ' km/s & Z=' num2str(Z)])
 xlabel('M_{BH} (M_{sun})')
 ylabel('\sigma_{BH} (km/s)')
 xlim([3 max(M_bh)])
 hold on
 h1(2)=plot(M_bh,sig_bh(2,:));
 hold on
 h1(3)=plot(M_bh,sig_bh(3,:));
 legend(h1,{'Neutrino-driven NK','Hybrid model','Standard fallback-controlled NK'})
end

 
    
    