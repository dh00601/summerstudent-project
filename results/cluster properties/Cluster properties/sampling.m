clearvars -except eval_fun vec_M_examples
%% INFORMATION ABOUT THE SCRIPT
%INPUT:
%      Z: metallicity
%      v_esc: escape velocity (in km/s)
%OUTPUT:
%{
We get the number of stars, NSs and BHs for 100 different samplings, as
well as the mass and number fractions of NSs and BHs.
We obtain this results considering only the IMF, but also for the cluster
after the supernova kicks. We do this by considering the scenario with
fallback, but also without it.
%}
%We then define the metallicity:
Z=2e-2;
%And the escape velocity:
v_esc=30; %km/s.
%We also need the cumulative probability distribution function (cpdf) so we
%load the results obtained from another script:
load('cpdf')
%% SAMPLING OF THE IMF
%{
%We start by determining the cumulative probability densitiy function (cpdf) for the masses in the vector vec_M_examples.
This is a costly process, so we load theresults from a previous
calculation by writing load('cpdf'). The idea of the process is then to
asign a random value between 0 and 1 for every star we "generate" so that
then we find which mass yields a value for the cpdf most similar to the
random value generated.
%}
%Below we show what that process is:
%{
%We start by defining the IMF:
alfa1=0.3;alfa2=1.3;alfa3=2.3;
imf= @(M) (M<0.08).*(M.^(-alfa1)) + (M<0.5 & M>=0.08).*((M.^(-alfa2))*(0.08)) + (M>=0.5).*((M.^(-alfa3))*(0.5));
%We then define the maximum value for the cumulative probability density
%function so that we can normalise the function itself so it can be treated
%as a probability.
cpdf_max=integral(imf,0.07,150,'RelTol',1e-6,'AbsTol',1e-6);
%Once we have this function defined we need to invert it. That it, we need
%to have it such that we give a random number from 0 to 1 and it returns a
%mass. So that we asign different masses until we reach the maximum value
%we want it to have.
%For example:
%We initialize the total mass to be 0 before we start "creating" stars:
M_tot=0;
vec_M_examples=[linspace(0.07,2,15000) linspace(2,150,25000)];
eval_fun=[];
for ii=1:length(vec_M_examples)
    M_example=vec_M_examples(ii);
    fun=@(M) (integral(imf,0.07,M,'RelTol',1e-6,'AbsTol',1e-6)/(cpdf_max));
    eval_fun(ii)=feval(fun,M_example);
end
%}
%Below we have the calculations followed in order to plot the histogram for
%the stars "generated" in a single sampling of the IMF. This single result
%does not show the stochastic nature of this process, so we will focus on
%doing 100 samplings in order to obtain the mean value and the sigma for
%the results we obtain. However, the process is described below anyway.
%{
M_tot=0;
vec_m=[];
jj=1;
while M_tot<10^5
    r=rand;
    kk=find(abs(eval_fun-r)<1e-3);
    m=vec_M_examples(kk(randi(length(kk))));
    vec_m(jj)=m;
    M_tot=M_tot+m;
    jj=jj+1;
end

%For stars less massive than 5 solar masses:
less=find(vec_m<25);
vec_m_stars=vec_m(less);
figure(1)
histogram(vec_m_stars)
title('Number of stars for M_{cl}=10^5')
xlabel('M_{zams}')
ylabel('Number of stars')
set(gca,'YScale','log')
massive=find(vec_m>25);
vec_m_massive=vec_m(massive);
figure(2)
title('Number of stars for M_{cl}=10^5')
xlabel('M_{zams}')
ylabel('Number of stars')
histogram(vec_m_massive)

%% FRACTIONS

%Total number of stars:
N_tot=length(vec_m);
%We define the metallicity so we find M_z_min that gives M_rem=3 solar
%masses:
Z=2e-2;
vec_M_zams_test=linspace(7,40,2000);
vec_M_bh_test=fun_M_rem(Z,vec_M_zams_test);
ll=find(abs(vec_M_bh_test-3)<1e-2);
M_z_min_bh_test=vec_M_zams_test(ll(1));
%And to find the minimum NS mass:
ll=find(abs(vec_M_bh_test-1.4)<1e-2);
M_z_min_ns_test=vec_M_zams_test(ll(1));
%{
%And then we need to transform this test values into the most similar ones
%for the given masses we have:
M_z_min_bh=min(vec_m-M_z_min_bh_test);
M_z_min_ns=min(vec_m-M_z_min_ns_test);
%}
%And then we compute the total number of stars of each of them:
N_bh=sum(vec_m>M_z_min_bh_test);
N_stars=sum(vec_m<M_z_min_ns_test);
N_ns=N_tot-N_bh-N_stars;
f_N_bh=N_bh/N_tot;f_N_ns=N_ns/N_tot;f_N_stars=N_stars/N_tot;
results_N_before=[N_bh/N_tot N_stars/N_tot N_ns/N_tot];
figure(3)
label={'BH','stars','NS'};
pie(results_N_before,label)
title('Number fraction of stars (just IMF)')
%And then we do the same for the mass:
M_bh=sum(vec_m(find(vec_m>M_z_min_bh_test)));
M_stars=sum(vec_m(find(vec_m<M_z_min_ns_test)));
M_ns=M_tot-M_bh-M_stars;
f_m_bh=M_bh/M_tot;f_m_ns=M_ns/M_tot;f_m_stars=M_stars/M_tot;
results_M_before=[M_bh/M_tot M_stars/M_tot M_ns/M_tot];
figure(4)
label={'BH','stars','NS'};
pie(results_M_before,label)
title('Number fraction of stars (just IMF)')
%}
%% FLUCTUATIONS IN THE TOTAL NUMBER OF STARS AND DIFFERENT FRACTIONS (JUST THE IMF)
%We start by computing the number of stars, number of BHs and NS, and their
%mass and number fractions for different samplings in order to observe the
%stochastic nature of the sampling itself.
%We first start by studying this upon formation, that is, just focusing on
%the IMF:
N_tot=[];
vec_ii=[1:100];
mat_m=[];%We save by rows the different samplings taken.
for ii=1:length(vec_ii)
  jj=1;
  vec_m=[];%We will use this vector to obtain N_tot because if we try to do it via the matrix m_mat, as it is padded with 0's,we always get the same N_tot
   M_tot=0;
  while M_tot<10^5
    r=rand;
    kk=find(abs(eval_fun-r)<1e-3);
    m=vec_M_examples(kk(randi(length(kk))));%we use this randi command because for a given r, given the resolution we can ask for, 
    %there might be more than one mass that suits such requirements. This
    %way, if there is more than one possible value, we asign that value
    %randomly.
    mat_m(ii,jj)=m;
    M_tot=M_tot+m;
    vec_m(jj)=m;
    jj=jj+1;
   end  
   N_tot(ii)=length(vec_m);
vec_M_zams_test=linspace(7,40,2000);
vec_M_bh_test=fun_M_rem(Z,vec_M_zams_test);
ll=find(abs(vec_M_bh_test-3)<1e-2);
M_z_min_bh_test=vec_M_zams_test(ll(1));
%And to find the minimum NS mass: (The minimum mass of 1.17 solar masses is
%given by Yudai Suwa, Takashi Yoshida, Masaru Shibata, Hideyuki Umeda & Koh
%Takahashi in "On the minimum mass of neutron stars" in 2009. The problem
%is that we cannot get M_rem=1.17 with the function we obtained via the
%PARSEC paper.That is why we set a minimum value of 1.4)
ll=find(abs(vec_M_bh_test-1.4)<1e-2);
M_z_min_ns_test=vec_M_zams_test(ll(1));
%And then we compute the total number of stars of each of them:
N_bh(ii)=sum(vec_m>M_z_min_bh_test);
N_stars(ii)=sum(vec_m<M_z_min_ns_test);
N_ns(ii)=N_tot(ii)-N_bh(ii)-N_stars(ii);
f_N_bh(ii)=N_bh(ii)/N_tot(ii);f_N_ns(ii)=N_ns(ii)/N_tot(ii);f_N_stars(ii)=N_stars(ii)/N_tot(ii);

M_bh(ii)=sum(vec_m(find(vec_m>M_z_min_bh_test)));
M_stars(ii)=sum(vec_m(find(vec_m<M_z_min_ns_test)));
M_ns(ii)=M_tot-M_bh(ii)-M_stars(ii);
f_m_bh(ii)=M_bh(ii)/M_tot;f_m_ns(ii)=M_ns(ii)/M_tot;f_m_stars=M_stars(ii)/M_tot;
end
figure(5)
semilogy(vec_ii,N_tot,'-k')
title('Number of stars in a cluster of M_{cl}=10^5 M_{sun} (just IMF)')
xlabel('Different samplings')
ylabel('Number of stars')
figure(6)
semilogy(vec_ii,N_bh,'-b')
title('Number of stars in a cluster of M_{cl}=10^5 M_{sun} (just IMF)')
xlabel('Different samplings')
ylabel('Number of stars')
hold on
semilogy(vec_ii,N_ns,'-r')
legend('N_{bh}','N_{ns}')
figure(7)
plot(vec_ii,f_m_bh,'-b')
title('Mass fractions in a cluster of M_{cl}=10^5 M_{sun} (just IMF)')
xlabel('Different samplings')
ylabel('Mass fractions')
hold on
plot(vec_ii,f_m_ns,'-r')
legend('f_{bh}','f_{ns}')

%% FLUCTUATIONS IN THE TOTAL NUMBER OF STARS AND DIFFERENT FRACTIONS (AFTER SUPERNOVA KICKS) (WITH FALLBACK)
%We now have to take every mass one by one and see, given the probabilities
%that we have computed, if they are retained or not. For this we make use
%of the masses that we have computed in the previous section. It is
%important to remember that we were storing in each row of mat_m the masses
%for a given sampling. It is also important to remember that we do not have
%the same number of stars in each sampling, and that matlab (given we have
%formed a matrix) fills the remaining positions with 0 so that they all
%have the same length. That is why the first thing to do, when studying one
%sampling, is to eliminate those 0's, and we can do that easily because we
%know N_tot in every iteration.
for ii=1:length(vec_ii)
    vec_m=mat_m(ii,1:N_tot(ii));
    vec_m_final=[];
%Now that we have defined the mass vector we make use of the probabilities:
    ll=1;
    for jj=1:length(vec_m)
        m=vec_m(jj);
        if m<M_z_min_ns_test
            vec_m_final(ll)=m;
            ll=ll+1;%Because it is always retained.
        elseif m>M_z_min_bh_test
            %k=1 ==>with fallback
            f_ret=(1-feval(@prob_escape,Z,m,1.4,265,v_esc,1));
            %We now generate a random number, if it is below f_ret it is
            %retained, if not it is kicked out of the cluster.
            rr=rand;
            if rr<=f_ret
                vec_m_final(ll)=m;
                ll=ll+1;
            else
                ll=ll;
            end
        else
            %Again we compute the probability of it being retained:
            f_ret=(1-feval(@prob_esc_ns,265,v_esc));
            rr=rand;
            if rr<=f_ret
                vec_m_final(ll)=m;
                ll=ll+1;
            else
                ll=ll;
            end
        end
    end
N_tot_final(ii)=length(vec_m_final);
M_tot_final(ii)=sum(vec_m_final);
N_bh_final(ii)=sum(vec_m_final>M_z_min_bh_test);
N_stars_final(ii)=sum(vec_m_final<M_z_min_ns_test);
N_ns_final(ii)=N_tot_final(ii)-N_bh_final(ii)-N_stars_final(ii);
f_N_bh_final(ii)=N_bh_final(ii)/N_tot_final(ii);f_N_ns_final(ii)=N_ns_final(ii)/N_tot_final(ii);f_N_stars_final(ii)=N_stars_final(ii)/N_tot_final(ii);

M_bh_final(ii)=sum(vec_m_final(find(vec_m_final>M_z_min_bh_test)));
M_stars_final(ii)=sum(vec_m_final(find(vec_m_final<M_z_min_ns_test)));
M_ns_final(ii)=M_tot_final(ii)-M_bh_final(ii)-M_stars_final(ii);
f_m_bh_final(ii)=M_bh_final(ii)/M_tot_final(ii);f_m_ns_final(ii)=M_ns_final(ii)/M_tot_final(ii);f_m_stars_final=M_stars_final(ii)/M_tot_final(ii);
end
figure(8)
semilogy(vec_ii,N_tot_final,'-k')
title('Number of stars in a cluster of M_{cl}=10^5 M_{sun} (after supernova kicks)')
xlabel('Different samplings')
ylabel('Number of stars')
figure(9)
semilogy(vec_ii,N_bh_final,'-b')
title('Number of stars in a cluster of M_{cl}=10^5 M_{sun} (after supernova kicks)')
xlabel('Different samplings')
ylabel('Number of stars')
hold on
semilogy(vec_ii,N_ns_final,'-r')
legend('N_{bh}','N_{ns}')
figure(10)
plot(vec_ii,f_m_bh_final,'-b')
title('Mass fractions in a cluster of M_{cl}=10^5 M_{sun} (after supernova kicks)')
xlabel('Different samplings')
ylabel('Mass fractions')
hold on
plot(vec_ii,f_m_ns_final,'-r')
legend('f_{bh}','f_{ns}')


%% MEAN VALUES AND DEVIATIONS WITH FALLBACK
%IMF:
mean_f_m_bh_initial=sum(f_m_bh)/100;
disp('Mean BH mass fraction (IMF)')
disp(mean_f_m_bh_initial)
var=(f_m_bh-mean_f_m_bh_initial).^2;
sigma_f_m_bh_initial=sqrt(sum(var)/100);
disp('Mean deviation in the BH mass fraction (IMF)')
disp(sigma_f_m_bh_initial)

%After supernova kicks:
mean_f_m_bh_final=sum(f_m_bh_final)/100;
disp('Mean BH mass fraction (after kicks)')
disp(mean_f_m_bh_final)
var=(f_m_bh_final-mean_f_m_bh_final).^2;
sigma_f_m_bh_final=sqrt(sum(var)/100);
disp('Mean deviation in the BH mass fraction (after kicks)')
disp(sigma_f_m_bh_final)

%% FLUCTUATIONS IN THE TOTAL NUMBER OF STARS AND DIFFERENT FRACTIONS (AFTER SUPERNOVA KICKS) (WITHOUT FALLBACK)
%We do the same again, after supernova kicks, but in this case without
%considering fallback. In this case we would expect a higher number (and
%fraction as a consequence) of BHs lost compared to the results obtained in
%the previous section (with fallback implemented):
for ii=1:length(vec_ii)
    vec_m=mat_m(ii,1:N_tot(ii));
    vec_m_final=[];
%Now that we have defined the mass vector we make use of the probabilities:
    ll=1;
    for jj=1:length(vec_m)
        m=vec_m(jj);
        if m<M_z_min_ns_test
            vec_m_final(ll)=m;
            ll=ll+1;%Because it is always retained.
        elseif m>M_z_min_bh_test
            %k=0 ==>without fallback
            f_ret=(1-feval(@prob_escape,Z,m,1.4,265,v_esc,0));
            %We now generate a random number, if it is below f_ret it is
            %retained, if not it is kicked out of the cluster.
            rr=rand;
            if rr<=f_ret
                vec_m_final(ll)=m;
                ll=ll+1;
            else
                ll=ll;
            end
        else
            %Again we compute the probability of it being retained:
            f_ret=(1-feval(@prob_esc_ns,265,v_esc));
            rr=rand;
            if rr<=f_ret
                vec_m_final(ll)=m;
                ll=ll+1;
            else
                ll=ll;
            end
        end
    end
N_tot_final_wo_fb(ii)=length(vec_m_final);
M_tot_final_wo_fb(ii)=sum(vec_m_final);
N_bh_final_wo_fb(ii)=sum(vec_m_final>M_z_min_bh_test);
N_stars_final_wo_fb(ii)=sum(vec_m_final<M_z_min_ns_test);
N_ns_final_wo_fb(ii)=N_tot_final_wo_fb(ii)-N_bh_final_wo_fb(ii)-N_stars_final_wo_fb(ii);
f_N_bh_final_wo_fb(ii)=N_bh_final_wo_fb(ii)/N_tot_final_wo_fb(ii);f_N_ns_final_wo_fb(ii)=N_ns_final_wo_fb(ii)/N_tot_final_wo_fb(ii);f_N_stars_final_wo_fb(ii)=N_stars_final_wo_fb(ii)/N_tot_final_wo_fb(ii);

M_bh_final_wo_fb(ii)=sum(vec_m_final(find(vec_m_final>M_z_min_bh_test)));
M_stars_final_wo_fb(ii)=sum(vec_m_final(find(vec_m_final<M_z_min_ns_test)));
M_ns_final_wo_fb(ii)=M_tot_final_wo_fb(ii)-M_bh_final_wo_fb(ii)-M_stars_final_wo_fb(ii);
f_m_bh_final_wo_fb(ii)=M_bh_final_wo_fb(ii)/M_tot_final_wo_fb(ii);f_m_ns_final_wo_fb(ii)=M_ns_final_wo_fb(ii)/M_tot_final_wo_fb(ii);f_m_stars_final_wo_fb=M_stars_final_wo_fb(ii)/M_tot_final_wo_fb(ii);
end
%Now we compare the results without fallback with the ones obtained for the
%IMF and the case with fallback considered:
%And now we compare the initial and final state
figure(11)
plot(vec_ii,N_tot,'-k')
title(['Number of stars in a cluster of M_{cl}=10^5 M_{sun}  (Z=' num2str(Z) ' & v_{esc}=' num2str(v_esc) 'km/s)'])
xlabel('Different samplings')
ylabel('Number of stars')
hold on
plot(vec_ii,N_tot_final,'-r')
hold on
plot(vec_ii,N_tot_final_wo_fb,'-b')
legend('IMF','w/ fb','w/o fb')

figure(12)
plot(vec_ii,N_bh,'-k')
title(['Number of BHs in a cluster of M_{cl}=10^5 M_{sun}  (Z=' num2str(Z) ' & v_{esc}=' num2str(v_esc) 'km/s)'])
xlabel('Different samplings')
ylabel('Number of BHs')
hold on
plot(N_bh_final,'-r')
hold on
plot(N_bh_final_wo_fb,'-b')
legend('IMF','w/ fb','w/o fb')

figure(13)
plot(vec_ii,f_m_bh,'-k')
title(['Mass fraction of BHs in a cluster of M_{cl}=10^5 M_{sun}  (Z=' num2str(Z) ' & v_{esc}=' num2str(v_esc) 'km/s)'])
xlabel('Different samplings')
ylabel('Mass fraction of BHs')
hold on
plot(vec_ii,f_m_bh_final,'-r')
hold on
plot(vec_ii,f_m_bh_final_wo_fb,'-b')
legend('IMF','w/ fb','w/o fb')

%% MEAN VALUES AND DEVIATIONS WITHOUT FALLBACK

%After supernova kicks: (without fallback)
mean_f_m_bh_final_wo_fb=sum(f_m_bh_final_wo_fb)/100;
disp('Mean BH mass fraction (after kicks) (w/o fallback)')
disp(mean_f_m_bh_final_wo_fb)
var=(f_m_bh_final_wo_fb-mean_f_m_bh_final).^2;
sigma_f_m_bh_final_wo_fb=sqrt(sum(var)/100);
disp('Mean deviation in the BH mass fraction (after kicks) (w/o fallback)')
disp(sigma_f_m_bh_final_wo_fb)


