%% FUNCTION THAT GIVES THE PROBABILITY OF v>vesc
%Input:
%                M_ns: neutron star mass (in solar masses)
%                M_bh: black hole mass (in solar masses)
%                sig_ns: neutron star velocity dispersion (in km/s)
%                v_esc: escape velocity

%Output:
%                p: probability of v>vesc
function p=prob_esc(M_ns,M_bh,sig_ns,v_esc)
sig_bh=(M_ns./M_bh).*sig_ns;
%{
%We could maybe try to implement fallback by doing:
if M_bh<15 
    f=0;
else
    f=(1/10)*atan((M_bh-15)/15);
end
p=sqrt(2/pi)*(v_esc./sig_bh).*exp(-(v_esc.^2)./(2*(sig_bh).^2))+(1-erf(v_esc./(sqrt(2)*sig_bh)))-f;
if p>0
    p=p;
else 
    p=0;
end
%}
p=sqrt(2/pi)*(v_esc./sig_bh).*exp(-(v_esc.^2)./(2*(sig_bh).^2))+(1-erf(v_esc./(sqrt(2)*sig_bh)));
end