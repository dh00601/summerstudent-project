clear all; close all; format compact; format long g;
%% CALCULATION OF THE ESCAPE VELOCITY AS A FUNCTION OF DENSITY AND MASS OF THE CLUSTER
%We start by defining some vectors for the possible masses and radius that
%we are going to give to the cluster.
vec_M=linspace(10^3,10^6,10^3); %In solar masses.
vec_r=linspace(0.5,10,1000); %In parsecs.
%We then define a meshgrid for this two vectors:
[M,r]=meshgrid(vec_M,vec_r);
%We then use the model to determine the escape velocity from the cluster:
rho_h=3*M./(8*pi*(r).^3);
M_5=M./(10^5);
rho_5=rho_h./(10^5);
f_c=1;
v_esc=50*((M_5).^(1/3)).*((rho_5).^(1/6)).*f_c;
%Then we plot it:
figure(1)
contour(M_5,r,v_esc,[10,20,30,40,50,75,100],'ShowText','on')
title('Contour plot for the v escape as a function of M_5 and r_h')
xlabel('M_5    (M_{cl}/(10^5 M_{sun})')
ylabel('r_h (pc)')

%% CONNECTION WITH THE PROBABILITY OF ESCAPE FOR DIFFERENT BLACK HOLE MASSES
v_esc=linspace(20,200,1000); %In km/s
M=linspace(3,40,1000); %In solar masses;
[M,v_esc]=meshgrid(M,v_esc);
M_ns=1.5; %Mass of the neutron star (in solar masses)
sig_ns=190;%Velocity dispersion for the neutron star (in km/s);
p=prob_esc(M_ns,M,sig_ns,v_esc); %Probability of escape
%And then we plot it:
figure(2)
contour(M,v_esc,p,[0.01, 0.25, 0.50, 0.75, 0.9],'ShowText','on')
title('Probability of escape for different BH masses and escape velocities')
xlabel('BH mass (in solar masses)')
ylabel('Escape velocity (in km/s)')

%% PROBABILITIES OF ESCAPING FOR  A CERTAIN BH MASS IN DIFFERENT CLUSTERS
%We start with a BH of 10 solar masses:
v_esc=linspace(0,200,1000); %In km/s;
M_ns=1.5;M_bh=10; %In solar masses
sig_ns=190; %In km/s;
p=prob_esc(M_ns,M_bh,sig_ns,v_esc); %Probability of escape
figure(3)
plot(v_esc,p,'-k')
title('Probabilities of escape')
xlabel('Escape velocity (km/s)')
ylabel('Probability')
hold on
%20 solar masses:
M_bh=20;
p=prob_esc(M_ns,M_bh,sig_ns,v_esc);
plot(v_esc,p,'-b')
hold on
%30 solar masses:
M_bh=30;
p=prob_esc(M_ns,M_bh,sig_ns,v_esc);
plot(v_esc,p,'-r')
%40 solar masses:
M_bh=40;
p=prob_esc(M_ns,M_bh,sig_ns,v_esc);
plot(v_esc,p,'-g')
legend('M=10','M=20','M=30','M=40')

%% PROBABILITIES OF ESCAPING FOR  A CERTAIN BH MASS IN DIFFERENT CLUSTERS 
%% BUT NOW AS A FUNCTION OF M_5 AND RHO_5 IN THE FORM OF A CONTOUR PLOT
vec_M=linspace(10^3,10^6,10^3); %In solar masses.
vec_r=linspace(0.5,5,1000); %In parsecs.
%We then define a meshgrid for this two vectors:
[M,r]=meshgrid(vec_M,vec_r);
%We then use the model to determine the escape velocity from the cluster:
rho_h=3*M./(8*pi*(r).^3);
M_5=M./(10^5);
rho_5=rho_h./(10^5);
f_c=1;
v_esc=50*((M_5).^(1/3)).*((rho_5).^(1/6)).*f_c;
M_ns=1.5;sig_ns=190;

%10 solar mass BH:
M_bh=10;
p=prob_esc(M_ns,M_bh,sig_ns,v_esc);
figure(4)
subplot(2,2,1)
contour(M_5,r,p,[0.01, 0.25, 0.50, 0.75, 0.9],'ShowText','on')
title('Prob escape')
xlabel('M_5')
ylabel('r_h (pc)')
legend('M=10')

%20 solar mass BH:
M_bh=20;
p=prob_esc(M_ns,M_bh,sig_ns,v_esc);
figure(4)
subplot(2,2,2)
contour(M_5,r,p,[0.01, 0.25, 0.50, 0.75, 0.9],'ShowText','on')
title('Prob escape')
xlabel('M_5')
ylabel('r_h (pc)')
legend('M=20')

%30 solar mass BH:
M_bh=30;
p=prob_esc(M_ns,M_bh,sig_ns,v_esc);
figure(4)
subplot(2,2,3)
contour(M_5,r,p,[0.01, 0.25, 0.50, 0.75, 0.9],'ShowText','on')
title('Prob escape')
xlabel('M_5')
ylabel('r_h (pc)')
legend('M=30')

%40 solar mass BH:
M_bh=40;
p=prob_esc(M_ns,M_bh,sig_ns,v_esc);
figure(4)
subplot(2,2,4)
contour(M_5,r,p,[0.01, 0.25, 0.50, 0.75, 0.9],'ShowText','on')
title('Prob escape')
xlabel('M_5')
ylabel('r_h (pc)')
legend('M=40')








