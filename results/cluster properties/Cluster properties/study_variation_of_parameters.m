clear all
%% ANALYSIS OF THE CONSEQUENCES OF THE VARIATION OF PARAMETERS IN THE RESULTS OBTAINED VIA IMF SAMPLING:
%First of all we need to load the values for the cumulative probability
%distribution function (cpdf) corresponding to the vector of possible
%masses vec_M_examples
tic

load('cpdf')
load('precomp_probs')
%For doubts regarding the nature of these calculations see sampling.m
%{
tic
%% VARATIONS IN THE ESCAPE VELOCITY  (M_cl=10^5 M_sun)
%We start by considering how does a variation in the escape velocity affect
%the retention fraction of BHs. Later we will try to connect this to the
%half-mass radius r_h so that we get a sense of the cluster itself.

%First of all we need to sample the IMF. Once that is done we can start
%considering what are the effects of a variation in the escape velocity.


%We start by defining a vector for the values of the escape velocity we
%want to consider so that then we can treat 
%We consider the following metallicity:
vec_v_esc=[10 15 20 25 30 35 40 45 50];
Z=2e-2;

jj=1;%We initialize this variable before starting the while loop.
vec_m_initial=[];%We will use this vector to save the different masses.
M_tot_initial=0;%We initialize M_tot before "filling" it with stars as we go along.
while M_tot_initial<10^5
    r=rand;
    kk=find(abs(eval_fun-r)<1e-3);
    m=vec_M_examples(kk(randi(length(kk))));
    M_tot_initial=M_tot_initial+m;
    vec_m_initial(jj)=m;
    jj=jj+1;
end  
N_tot_initial=length(vec_m_initial);
%We now have to define the minimum M_zams for NS and BHs:
vec_M_zams_test=linspace(7,40,2000);
vec_M_bh_test=fun_M_rem(Z,vec_M_zams_test);
ll=find(abs(vec_M_bh_test-3)<1e-2);
M_z_min_bh_test=vec_M_zams_test(ll(1));
%And to find the minimum NS mass: (The minimum mass of 1.17 solar masses is
%given by Yudai Suwa, Takashi Yoshida, Masaru Shibata, Hideyuki Umeda & Koh
%Takahashi in "On the minimum mass of neutron stars" in 2009. The problem
%is that we cannot get M_rem=1.17 with the function we obtained via the
%PARSEC paper.That is why we set a minimum value of 1.4)
ll=find(abs(vec_M_bh_test-1.4)<1e-2);
M_z_min_ns_test=vec_M_zams_test(ll(1));

%And then we compute the total number of stars of each of them:
N_bh_initial=sum(vec_m_initial>M_z_min_bh_test);
N_stars_initial=sum(vec_m_initial<M_z_min_ns_test);
N_ns_initial=N_tot_initial-N_bh_initial-N_stars_initial;
f_N_bh_initial=N_bh_initial/N_tot_initial;f_N_ns_initial=N_ns_initial/N_tot_initial;f_N_stars_initial=N_stars_initial/N_tot_initial;

M_bh_initial=sum(vec_m_initial(find(vec_m_initial>M_z_min_bh_test)));
M_stars_initial=sum(vec_m_initial(find(vec_m_initial<M_z_min_ns_test)));
M_ns_initial=M_tot_initial-M_bh_initial-M_stars_initial;
f_m_bh_initial=M_bh_initial/M_tot_initial;f_m_ns_initial=M_ns_initial/M_tot_initial;f_m_stars_initial=M_stars_initial/M_tot_initial;

toc
%This way we have the initial values for everything of interest for us, so
%we can study what are the effects of a variation in the escape velocity:

%Now that we have defined the mass vector we make use of the probabilities:
%But we need to consider that for each value of v_esc we have to save a
%vector of different masses. For that we form a matrix, but it is important
%to consider that we will not have the same number of stars for every
%escape velocity, and matlab fills the ones that are "missing" with 0's. In
%order, then, to not consider these zeros we have to take only till the
%N_tot_final position for every escape velocity.
tic
mat_m=[];
for ii=1:length(vec_v_esc)
    v_esc=vec_v_esc(ii);
    vec_m_final=[];
    ll=1;%We initialize this and then change the value by one if the star is retained.
    for jj=1:length(vec_m_initial)
        m=vec_m_initial(jj);
        if m<M_z_min_ns_test
            vec_m_final(ll)=m;
            ll=ll+1;%Because it is always retained.
            
        elseif m>M_z_min_bh_test
            %k=1 ==>with fallback
            %f_ret=(1-feval(@prob_escape,Z,m,1.4,265,v_esc,1));
            %We now generate a random number, if it is below f_ret it is
            %retained, if not it is kicked out of the cluster.
            rr=rand;
            if rr<=vec_f_ret(ii,find(vec_M_examples==m))
                vec_m_final(ll)=m;
                ll=ll+1;
            else
                ll=ll;
            end
        else
            %Again we compute the probability of it being retained:
            %f_ret=vec_f_ret_ns(ii);
            %f_ret=(1-feval(@prob_esc_ns,265,v_esc));
            rr=rand;
            if rr<=vec_f_ret_ns(ii)
                vec_m_final(ll)=m;
                ll=ll+1;
            else
                ll=ll;
            end
        end
%We save in each row of the matrix mat_m every mass retained for a given
%escape velocity:
    mat_m(ii,1:length(vec_m_final))=vec_m_final;
    N_tot_final(ii)=length(vec_m_final);
    M_tot_final(ii)=sum(vec_m_final);
    N_bh_final(ii)=sum(vec_m_final>M_z_min_bh_test);
    N_stars_final(ii)=sum(vec_m_final<M_z_min_ns_test);
    N_ns_final(ii)=N_tot_final(ii)-N_bh_final(ii)-N_stars_final(ii);
    
    
    M_bh_final(ii)=sum(vec_m_final(find(vec_m_final>M_z_min_bh_test)));
    M_stars_final(ii)=sum(vec_m_final(find(vec_m_final<M_z_min_ns_test)));
    M_ns_final(ii)=M_tot_final(ii)-M_bh_final(ii)-M_stars_final(ii);
    %Fractions compared to the current cluster:
    f_N_bh_final(ii)=N_bh_final(ii)/N_tot_final(ii);f_N_ns_final(ii)=N_ns_final(ii)/N_tot_final(ii);f_N_stars_final(ii)=N_stars_final(ii)/N_tot_final(ii);
    f_m_bh_final(ii)=M_bh_final(ii)/M_tot_final(ii);f_m_ns_final(ii)=M_ns_final(ii)/M_tot_final(ii);f_m_stars_final=M_stars_final(ii)/M_tot_final(ii);
    %(BHs now)/(BH initially):
    f_N_bh_relative(ii)=N_bh_final(ii)/N_bh_initial;
    f_m_bh_relative(ii)=M_bh_final(ii)/M_bh_initial;
    end
end
toc
%And now with this we can obtain a table:
T=table(vec_v_esc',N_bh_final',N_ns_final',f_m_bh_final');
T.Properties.VariableNames = {'v_esc','N_BH','N_NS','f_m_BH'};
disp(['Initially we had: Z=' num2str(Z) ' & N_tot=' num2str(N_tot_initial)])
disp('And taking into account fallback:')
disp(T)
%}

%% CONTOUR PLOT FOR THE NUMBER FRACTION OF BLACK HOLES AS A FUNCTION OF M_cl AND r_h:

Z=0.0001;k=1;

%We define the following vectors:
%vec_M_cl=[5*10^3 10^4 2*10^4 3*10^4 5*10^4 10^5 2*10^5 5*10^5 10^6];
vec_M_cl=linspace(10^3,10^6,125);
%vec_M_cl=[10^3 2*10^3 3*10^3 4*10^3 5*10^3 10^4 2*10^4 5*10^4 10^5];

%vec_M_cl=[10^4 2*10^4 3*10^4 5*10^4 7.5*10^4 10^5 5*10^5];
%vec_r_h=[0.2 0.5 0.75 1 1.5 2 2.5 3 4]; 
%vec_r_h=[0.25 0.5 0.75 1 1.25 1.5 1.75 2];
%vec_v_esc=[10 15 20 25 30 35 40 45 50];
vec_v_esc=linspace(5,60,125);
%And with this we form the meshgrid:
[M_cl,mat_v_esc]=meshgrid(vec_M_cl,vec_v_esc);
%And we need to precompute the probabilities so
%study_variation_of_parameters.m does not need to compute them in every
%iteration
%So that:
f_N_bh_relative=fun_for_contour_fractions_3(M_cl,Z,mat_v_esc,k);
%But we want to express it as a function of r_h and nor mat_v_esc
r_h=(1/40)*((3/(8*pi))^(1/3))*((M_cl)./((mat_v_esc).^2));
figure(1)
[C,h]=contour(M_cl/(10^5),r_h,f_N_bh_relative,'ShowText','on');
clabel(C,h)
title('BH retention fraction (Z=0.0001) (with fb & sigma=265 km/s)')
xlabel('M_{cl} (10^5 M_{sun})')
ylabel('r_h (in pc)')
set(gca, 'YScale', 'log')
%}
%% CONTOUR PLOT FOR LOW MASS CLUSTERS
%{
% When working with low mass globular clusters we have to consider that one
% sampling is not really representative of a cluster with that mass, as
% one can get really different results depending on the sampling. This is
% something that is not that important in more massive clusters with masses
% of 10^5 or 10^6 solar masses.
%That is why in this case we will do more than one sampling and then
%compute and plot the mean values:

%We define the following vectors:
%vec_M_cl=[5*10^3 10^4 2*10^4 3*10^4 5*10^4 10^5 2*10^5 5*10^5 10^6];
vec_M_cl=linspace(10^3,10^4,100);
%vec_M_cl=[10^3 2*10^3 3*10^3 4*10^3 5*10^3 10^4 2*10^4 5*10^4 10^5];

%vec_M_cl=[10^4 2*10^4 3*10^4 5*10^4 7.5*10^4 10^5 5*10^5];
%vec_r_h=[0.2 0.5 0.75 1 1.5 2 2.5 3 4]; 
%vec_r_h=[0.25 0.5 0.75 1 1.25 1.5 1.75 2];
%vec_v_esc=[10 15 20 25 30 35 40 45 50];
vec_v_esc=linspace(5,40,100);
%And with this we form the meshgrid:
[M_cl,mat_v_esc]=meshgrid(vec_M_cl,vec_v_esc);
%But we want to express it as a function of r_h and nor mat_v_esc
r_h=(1/40)*((3/(8*pi))^(1/3))*((M_cl)./((mat_v_esc).^2));

%So that:
n_times=20;
ii=2;
f_N_bh_relative=fun_for_contour_fractions_3(M_cl,2e-2,mat_v_esc);
vec_N_bh_initial=[];
while ii<n_times
f_N_bh_relative=f_N_bh_relative + fun_for_contour_fractions_3(M_cl,2e-2,mat_v_esc);
ii=ii+1;
end 
%And now we determine the mean value by dividing the sum we have done in
%the while loop by the number of different samplings done for each mass.
mean_f_bh_relative=f_N_bh_relative/(n_times);

figure(1)
[C,h]=contour(M_cl/(10^3),r_h,f_bh_relative,'ShowText','on');
clabel(C,h)
title('BH retention fraction (Z=2e-2) (w/ fallback)')
xlabel('M_{cl} (10^3 M_{sun})')
ylabel('r_h (in pc)')
%set(gca, 'YScale', 'log')

%}
%% HISTOGRAM WITH THE MASSES RETAINED AND THE INITIAL MASSES
M_cl=5*10^4;r_h=2;Z=0.0001;
v_esc=fun_v_esc(r_h,M_cl);

[vec_m_bh_initial,vec_m_bh_final]=fun_for_mass_histograms(M_cl,Z,v_esc,2);
%But we have to take into account that we obtain the vector of masses, but
%as M_zams, we net to transform that into black hole masses:
vec_m_bh_initial=fun_M_rem(Z,vec_m_bh_initial);
vec_m_bh_final=fun_M_rem(Z,vec_m_bh_final);
figure(1)
h1=histogram(vec_m_bh_initial,45,'FaceColor','b');
%h1.BinWidth = 0.25;
title('Number of BHs for [M_{cl}=5*10^4 M_{sun}, r_h=2 pc, Z=0.0001] With fb, sig=265 km/s')
xlabel('M_{BH} (M_{sun})')
ylabel('Number of BHs')
xlim([0 100])
%xticks([0 10 20 30 40 50 60 70 80 90 100])
%set(gca,'YScale','log')
hold on
h2=histogram(vec_m_bh_final,45,'FaceColor','r');
%h2.BinWidth = 0.25;
legend('IMF','after kicks') 
%}