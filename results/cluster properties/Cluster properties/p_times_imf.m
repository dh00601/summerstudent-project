function p_imf=p_times_imf(Z,alfa1,alfa2,M_zams,M_ns,sig_ns,v_esc)
p=feval(@prob_esc_with_fb,Z,M_zams,M_ns,sig_ns,v_esc);
imf= @(M) (M<0.5).*(M.^(-alfa1)) + (M>=0.5).*(M.^(-alfa1));
p_imf=p*imf(M_zams);
end