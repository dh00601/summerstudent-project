%% FUNCTION THAT STUDIES THE STOCHASTICITY FOR THE THREE KICK PRESCRIPTIONS WE HAVE WORKED ON
%{
Inputs:
       M_cl: Mass of the cluster (in solar masses)
       Z: Metallicity
       r_h: half-mass radius
Output:
        plots the retention fraction for a cluster chosen with the inputs
        for 100 different samplings (to change this number change vec_ii)

IMPORTANT: THIS CODE ONLY WORKS FOR A SCALAR M_cl and Z.
For more information on the sampling and how we compute the retained stars see SAMPLING.m
%}
function fun_stochasticy_all_kicks(Z,M_cl,r_h)
%We start by loading the cumulative probability distribution function 
load('cpdf')
%In order to do the sampling and then compute how many BHs remain in the
%cluster after the supernova kicks we will use fun_for_mass_histograms.m
%For that we first need to compute the escape velocity:
v_esc=fun_v_esc(r_h,M_cl);

vec_ii=[1:100];
mat_m=[];
N_tot=zeros(1,length(vec_ii));
M_tot=zeros(1,length(vec_ii));
for ii=1:length(vec_ii)
  jj=1;
  vec_m=[];%We will use this vector to obtain N_tot because if we try to do it via the matrix m_mat, as it is padded with 0's,we always get the same N_tot.
  while M_tot(ii)<M_cl
    r=rand;
    kk=find(abs(eval_fun-r)<1e-3);
    m=vec_M_examples(kk(randi(length(kk))));%we use this randi command because for a given r, given the resolution we can ask for, 
    %there might be more than one mass that suits such requirements. This
    %way, if there is more than one possible value, we asign that value
    %randomly.
    M_tot(ii)=M_tot(ii)+m;
    mat_m(ii,jj)=m;
    vec_m(jj)=m;
    jj=jj+1;
   end  
   N_tot(ii)=length(vec_m);
   vec_M_zams_test=linspace(7,40,2000);
   vec_M_bh_test=fun_M_rem(Z,vec_M_zams_test);
   ll=find(abs(vec_M_bh_test-3)<1e-2);
   M_z_min_bh_test=vec_M_zams_test(ll(1));
%And to find the minimum NS mass: (The minimum mass of 1.17 solar masses is
%given by Yudai Suwa, Takashi Yoshida, Masaru Shibata, Hideyuki Umeda & Koh
%Takahashi in "On the minimum mass of neutron stars" in 2009. The problem
%is that we cannot get M_rem=1.17 with the functions presented by Spera, Mapelli & Bressan (2015), and Fryer 
%et al. (2012) for the delayed supernova mechanism because the mass is too low (minimum M_zams of 7 solar masses).
%That is why we set a minimum value of 1.4:
ll=find(abs(vec_M_bh_test-1.4)<1e-2);
M_z_min_ns_test=vec_M_zams_test(ll(1));
%And then we compute the total number of stars of each of them:
N_bh(ii)=sum(vec_m>M_z_min_bh_test);
N_stars(ii)=sum(vec_m<M_z_min_ns_test);
N_ns(ii)=N_tot(ii)-N_bh(ii)-N_stars(ii);

M_bh(ii)=sum(feval(@fun_M_rem,Z,vec_m(find(vec_m>M_z_min_bh_test))));
M_stars(ii)=sum(vec_m(find(vec_m<M_z_min_ns_test)));
M_ns(ii)=M_tot(ii)-M_bh(ii)-M_stars(ii);
end
%Now that we have sampled it different times we have to compute the results
%after the supernova kicks according to the three prescriptions we have
%studied:
for k=0:2
    if k==0
     kick='Neutrino-driven NK.';
    elseif k==1 
     kick='W/ fallback,reduced sigma NK';
    elseif k==2
     kick='Standard falback-controlled NK';
    end
for ii=1:length(vec_ii)
%In order to not use so many if statements we are going to find which
%positions in the vector vec_m_initial have a mass smaller than the minimum
%neutron star mass. Because this stars are retained and considering them in
%an if statement only slows the computation:
pos_stars=mat_m(ii,:)<M_z_min_ns_test;%Position of such stars in the vector vec_m_initial. 
%(we get 1's in the positions of the stars that verify the requirement, and 0 for the others)
vec_m_final_i=mat_m(ii,:).*pos_stars;
%But we need to eliminate the 0's and only consider those that are
%different  from 0.
vec_m_final=nonzeros(vec_m_final_i)';
%And the vector of masses for the NS and BHs before considering the
%probabilities for them to be kicked out are:
vec_bh_ns_i=mat_m(ii,:)-vec_m_final_i;
vec_bh_ns=nonzeros(vec_bh_ns_i)';%Vector that contains the initial stars that are (or will be, basically, as these are ZAMS masses) BHs or NSs.
%And now we do an if statement for this stars:
ll=length(vec_m_final); %We initialize ll so that we fill the vector after that position.
%We calculate only once the probability for the initial vector of masses:
    f_ret=(1-feval(@prob_escape,Z,vec_bh_ns,1.4,265,v_esc,k)); 
%It is also important to see that later, in order to really compute the
%number of BHs and NSs we need to create a vector that stores the BHs and
%neutron stars:
vec_bh_ns_final=[];
%For that we initialize the variable nn
nn=1;
%But we need to transform this into the masses that the remnant stars will
%really have after the explosion(not the ZAMS masses that the BHs or NSs previously had):
vec_bh_ns=fun_M_rem(Z,vec_bh_ns);
     for jj=1:length(vec_bh_ns)
         if vec_bh_ns(jj)>3
            rr=rand;
            if rr<=f_ret(jj)
                vec_m_final(ll)=vec_bh_ns(jj);
                vec_bh_ns_final(nn)=vec_bh_ns(jj);
                ll=ll+1;nn=nn+1;
             else
                ll=ll;nn=nn;
             end
         else
            rr=rand;
            if rr<=(1-feval(@prob_esc_ns,265,v_esc))
                vec_m_final(ll)=vec_bh_ns(jj);
                vec_bh_ns_final=vec_bh_ns(jj);
                ll=ll+1;nn=nn+1;
            else
                ll=ll;nn=nn;
            end
         end
     end
     N_tot_final(ii)=length(vec_m_final);
     M_tot_final(ii)=sum(vec_m_final);
     N_bh_final(ii)=sum(vec_bh_ns_final>3);
     N_ns_final(ii)=sum(vec_bh_ns_final<3);
     N_stars_final(ii)=N_tot_final(ii)-N_bh_final(ii)-N_ns_final(ii);
     f_N_bh(ii)=N_bh_final(ii)/N_bh(ii);f_N_ns(ii)=N_ns_final(ii)/N_ns(ii);f_N_stars(ii)=N_stars_final(ii)/N_tot(ii);

     M_bh_final(ii)=sum(vec_bh_ns_final(find(vec_bh_ns_final>3)));
     M_ns_final(ii)=sum(vec_bh_ns_final(find(vec_bh_ns_final<3)));
     M_stars_final(ii)=M_tot_final(ii)-M_bh_final(ii)-M_ns_final(ii);
     f_m_bh(ii)=M_bh_final(ii)/M_tot(ii);f_m_ns(ii)=M_ns_final(ii)/M_ns(ii);f_m_stars=M_stars_final(ii)/M_tot(ii);
     f_m_bh_current(ii)=M_bh_final(ii)/M_tot_final(ii);
end
%And now we can plot this:
figure(1)

h1(k+1)=plot(vec_ii,N_tot_final);
title(['Number of stars in a cluster of M_{cl}=' num2str(M_cl) ' M_{sun}, r_h=' num2str(r_h) ' pc & Z=' num2str(Z)])
xlabel('Different samplings')
ylabel('Number of stars')
%legend(['After kicks.' num2str(kick)],'Location','southwest')
%legend('boxoff')
hold on

figure(2)
subplot(4,1,1)
h2(k+1)=plot(vec_ii,N_bh_final);
title(['Number of BHs in a cluster of M_{cl}=' num2str(M_cl) ' M_{sun}, r_h=' num2str(r_h) ' pc & Z=' num2str(Z)])
xlabel('Different samplings')
ylabel('Number of BHs')
%legend(['After kicks.' num2str(kick)],'Location','southwest')
%legend('boxoff')
hold on
subplot(4,1,2)
h3(k+1)=plot(vec_ii,f_N_bh);
title('Fraction of BHs retained')
xlabel('Different samplings')
ylabel('Fraction of BHs retained')
hold on
subplot(4,1,3)
h4(k+1)=plot(vec_ii,f_m_bh);
title('Mass fraction of BHs after NK')
xlabel('Different samplings')
ylabel('Mass fraction of BHs')
hold on
subplot(4,1,4)
h5(k+1)=plot(vec_ii,f_m_bh_current);
title('Fraction of BHs retained')
xlabel('Different samplings')
ylabel('Fraction of BHs retained')
hold on



%And for these calculations we can study the stochastic nature, that is,
%what is the mean value, standard deviations, etc:
%IMF:
disp('                       VARIATIONS IN THE NUMBER OF BHs')
disp('Mean number of initial BHs:')
mean_N_bh_initial=sum(N_bh)/length(vec_ii);
disp(mean_N_bh_initial)
var=(N_bh-mean_N_bh_initial).^2;
sigma_N_bh_initial=sqrt(sum(var)/length(vec_ii));
disp('Standard deviation in the initial number of BHs:')
disp(sigma_N_bh_initial)

%After supernova kicks:
mean_N_bh_final=sum(N_bh_final)/length(vec_ii);
disp(['Mean number of BHs after supernova kicks:' ' ( ' num2str(kick) ' )'])
disp(mean_N_bh_final)
var=(N_bh_final-mean_N_bh_final).^2;
sigma_N_bh_final=sqrt(sum(var)/length(vec_ii));
disp(['Standard deviation in the number of BHs after supernova kicks:' ' ( ' num2str(kick) ' )'] )
disp(sigma_N_bh_final)

%And now we do the same but for the number fraction of black holes:
disp('                       VARIATIONS IN THE NUMBER FRACTION OF BHs RETAINED')
disp(['                      ' num2str(kick)])
disp('Mean number fraction of BHs retained:')
mean_f_N_bh=sum(f_N_bh)/length(vec_ii);
disp(mean_f_N_bh)
var=(f_N_bh-mean_f_N_bh).^2;
sigma_f_N_bh=sqrt(sum(var)/length(vec_ii));
disp('Standard deviation in the number fraction of BHs retained:')
disp(sigma_f_N_bh)
end
%And we now need to add to the plots above the number of BHs that you find
%initially (due simply to the IMF)
figure(1)
h1(4)=plot(vec_ii,N_tot,'-k');

figure(2)
subplot(4,1,1)
h2(4)=plot(vec_ii,N_bh,'-k');
%And now we define the legends for the plots defined above:
legend(h1,{'Neutrino-driven NK','Hybrid model','Standard fallback-controlled NK','IMF (before kicks)'})
legend(h2,{'Neutrino-driven NK','Hybrid model','Standard fallback-controlled NK','IMF (before kicks)'})
end