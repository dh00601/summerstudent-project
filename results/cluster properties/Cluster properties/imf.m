%% IMF
%This will be a function that will give us the IMF if we determine the
%values of alfa in dN/dm proportional to m^(-alfa).

%Input:
%      alfa1:exponent for M<0.08 solar masses
%      alfa2:exponent for 0.08<M<0.5 solar masses
%      alfa3:exponent for M>0.5 solar masses
%      M_zams: zero age main sequence mass (in solar masses)
%Output:
%      phi: dN/dm
function phi=imf(alfa1,alfa2,alfa3,M_zams)
for ii=1:length(M_zams)
    M_zams_i=M_zams(ii);
if M_zams_i<0.08
    phi(ii)=(M_zams_i).^(-alfa1);
elseif M_zams_i<0.5 && M_zams_i>=0.08
    phi(ii)=((M_zams_i).^(-alfa2))*0.08;
elseif M_zams_i>=0.5
    phi(ii)=((M_zams_i).^(-alfa3))*0.5;
end
end
end
