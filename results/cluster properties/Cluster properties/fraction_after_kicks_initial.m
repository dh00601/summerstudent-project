%% FUNCTION THAT GIVES THE MASS AND NUMBER FRACTION OF BH AFTER SUPERNOVA KICKS (AS A FUNCTION OF THE INITIAL CLUSTER)
%Input:
%{
      Z:The metallicity will let us obtain M_zams_min if we consider M_bh_min=4.4
        We get this value from Farr et al. 2011. (They say between 4.3 and
        4.5 with 90% confidence.
%}
%      alfa1 and alfa2: in order to use the IMF.
%Output:
%      f_N: number fraction of BHs that remain after supernova kicks.
%      f_M: mass fraction of BHs that remain after supernova kicks.
function [M_z_min,f_N,f_m]=fraction_after_kicks_initial(Z,alfa1,alfa2,alfa3,v_esc)
vec_M_zams=linspace(10,40,2000);
vec_M_bh=fun_M_rem(Z,vec_M_zams);
%Then we find the value for M_zams that gives M_bh=3.5:
kk=find(abs(vec_M_bh-4.4)<1e-2);
M_z_min=vec_M_zams(kk(1));
%But we need to take into account the retention fraction, that is, we need
%to compute the probability of escaping, so p_ret=1-p_esc
sig_ns=265;M_ns=1.5;
fun= @(M) (M<0.08).*((M.^(-alfa1)).*(1-feval(@prob_esc_with_fb,Z,M,M_ns,sig_ns,v_esc))) + (M<0.5 & M>=0.08).*((M.^(-alfa2)).*(1-feval(@prob_esc_with_fb,Z,M,M_ns,sig_ns,v_esc))) + (M>=0.5).*((M.^(-alfa3)).*(1-feval(@prob_esc_with_fb,Z,M,M_ns,sig_ns,v_esc)));
%This way we can compute the number fraction:
%fun is the imf times the retention probability.
q1=integral(fun,M_z_min,150,'RelTol',1e-6,'AbsTol',1e-6);
%q1=integral(feval(@p_times_imf,Z,alfa1,alfa2,M,M_ns,sig_ns,v_esc),M_z_min,150,'RelTol',1e-6,'AbsTol',1e-6);
fun2= @(M) (M<0.08).*(M.^(-alfa1)) + (M<0.5 & M>=0.08).*(M.^(-alfa2)) + (M>=0.5).*(M.^(-alfa3));
q2=integral(fun2,0.07,150,'RelTol',1e-6,'AbsTol',1e-6);
f_N=q1/q2;

%Defining fun2 we obtain the BH  mass as a function of the initial total
%mass (or total number of stars) of the cluster.

%Now, in order to compute the mass fraction we have to multiply the previous function by
%the mass. Meaning:
alfa1=alfa1-1;alfa2=alfa2-1;alfa3=alfa3-1;
%Here fun refers to the imf times the mass times the retention probability(as we are looking for the mass
%fraction).
fun= @(M) (M<0.08).*((M.^(-alfa1)).*(1-feval(@prob_esc_with_fb,Z,M,M_ns,sig_ns,v_esc))) + (M<0.5 &M>=0.08).*((M.^(-alfa2)).*(1-feval(@prob_esc_with_fb,Z,M,M_ns,sig_ns,v_esc))) + (M>=0.5).*((M.^(-alfa3)).*(1-feval(@prob_esc_with_fb,Z,M,M_ns,sig_ns,v_esc)));
q1=integral(fun,M_z_min,150,'RelTol',1e-6,'AbsTol',1e-6);
fun2= @(M) (M<0.08).*(M.^(-alfa1)) + (M<0.5 & M>=0.08).*(M.^(-alfa2)) + (M>=0.5).*(M.^(-alfa3));
q2=integral(fun2,0.07,150,'RelTol',1e-6,'AbsTol',1e-6);
f_m=q1/q2;
%}
end