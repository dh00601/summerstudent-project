%% FUNCTION THAT WILL GIVE US WHAT WE NEED IN ORDER TO FORM THE CONTOUR PLOT FOR ALL PRESCRIPTIONS AT ONCE
%% FOR THE MEAN VALUES OF LOW-MASS CLUSTERS
%{
Description: This function allows for the plotting of a contour plot for
the retention fraction of BHs for different stellar cluster masses and
half-mass radii. Concretely it focuses on low-mass clusters, such that it
computes the mean value for different samplings, as for this lower masses
the sampling can really affect the results.
%}
%{
Inputs:
       M_cl: Mass of the cluster (in solar masses)
       Z: Metallicity
       mat_v_esc: escape velocity (in km/s)
       k: parameter that defines the kick prescription:
            k=0 ==> no fallback and reduced velocity dispersion
            k=1 ==> fallback considered and reduced velocity dispersion
            K=2 ==> fallback considered and not a reduced velocity dispersion.
Output:
        f_N_bh_relative: number fraction of black holes retained compared
        to the initial number of black holes (as defined by the IMF)

IMPORTANT: THIS CODE ONLY WORKS IF THE MATRIX FOR THE POSSIBLE M_cl AND
v_esc HAVE THE SAME SIZE. (SO IN THE END ONLY WORKS AFTER THE MESHGRID IS
DONE)
For more information on the sampling and how we compute the retained stars see sampling.m
%}

function [f_N_bh_mean,f_M_bh_mean]=fun_for_contour_fractions_low_mass(M_cl,Z,r_h)
%We start by sampling the IMF. And for that we need to load the cpdf
%(cumulative probability distribution function):
vec_ii=[1:5];
load('cpdf')
%{
vec_m_initial=[];%We will use this matrix to save the different masses. We initialize this variable so that we store every mass vector in a different row:
vec_M_cl=M_cl(1,:); %We define this vector so that the sampling for every cluster mass is only done once. This way it is faster.
tic
%And then we can sample the IMF:
 for s=1:length(vec_M_cl)
        %M_cl_it=M_cl(dd,ff);
        M_tot_initial=0;
jj=1;%We initialize this variable before starting the while loop.
  while M_tot_initial<vec_M_cl(s)
    r=rand;
    kk=find(abs(eval_fun-r)<1e-3);
    m=vec_M_examples(kk(randi(length(kk))));
    M_tot_initial=M_tot_initial+m;
    vec_m_initial(s,jj)=m;% As it can be seen we store in each row the masses of a given sampling (for a given cluster mass)
    jj=jj+1;
  end  
N_tot_initial(s,:)=length(vec_m_initial(s,:));%In this vector we save the total number of stars.
%We now have to define the minimum M_zams for NS and BHs:
vec_M_zams_test=linspace(7,40,2000);
vec_M_bh_test=fun_M_rem(Z,vec_M_zams_test);
ll=find(abs(vec_M_bh_test-3)<1e-2);
M_z_min_bh_test=vec_M_zams_test(ll(1));
%And to find the minimum NS mass: The minimum mass of 1.17 solar masses is
%given by Yudai Suwa, Takashi Yoshida, Masaru Shibata, Hideyuki Umeda & Koh
%Takahashi in "On the minimum mass of neutron stars" in 2009. The problem
%is that we cannot get M_rem=1.17 with the function we obtained via the
%PARSEC paper.That is why we set a minimum value of 1.4:
ll=find(abs(vec_M_bh_test-1.4)<1e-2);
M_z_min_ns_test=vec_M_zams_test(ll(1));

%And then we compute the total number of BHs (stars that will become one):
N_bh_initial(s)=sum(vec_m_initial(s,:)>M_z_min_bh_test);
toc
 end
%}
 
for k=0:2
f_N_bh_total=zeros(size(M_cl));f_M_bh_total=zeros(size(M_cl));
   if k==0
    kick='W/o fallback,reduced sigma';
   elseif k==1 
    kick='W/ fallback,reduced sigma';
   elseif k==2
    kick='W/ fallback,sigma not reduced';
   end
 for ii=length(vec_ii) 
     
     vec_m_initial=[];%We will use this matrix to save the different masses. We initialize this variable so that we store every mass vector in a different row:
vec_M_cl=M_cl(1,:); %We define this vector so that the sampling for every cluster mass is only done once. This way it is faster.
tic
%And then we can sample the IMF:
 for s=1:length(vec_M_cl)
        %M_cl_it=M_cl(dd,ff);
        M_tot_initial=0;
jj=1;%We initialize this variable before starting the while loop.
  while M_tot_initial<vec_M_cl(s)
    r=rand;
    kk=find(abs(eval_fun-r)<1e-3);
    m=vec_M_examples(kk(randi(length(kk))));
    M_tot_initial=M_tot_initial+m;
    vec_m_initial(s,jj)=m;% As it can be seen we store in each row the masses of a given sampling (for a given cluster mass)
    jj=jj+1;
  end  
N_tot_initial(s,:)=length(vec_m_initial(s,:));%In this vector we save the total number of stars.
%We now have to define the minimum M_zams for NS and BHs:
vec_M_zams_test=linspace(7,40,2000);
vec_M_bh_test=fun_M_rem(Z,vec_M_zams_test);
ll=find(abs(vec_M_bh_test-3)<1e-2);
M_z_min_bh_test=vec_M_zams_test(ll(1));
%And to find the minimum NS mass: The minimum mass of 1.17 solar masses is
%given by Yudai Suwa, Takashi Yoshida, Masaru Shibata, Hideyuki Umeda & Koh
%Takahashi in "On the minimum mass of neutron stars" in 2009. The problem
%is that we cannot get M_rem=1.17 with the function we obtained via the
%PARSEC paper.That is why we set a minimum value of 1.4:
ll=find(abs(vec_M_bh_test-1.4)<1e-2);
M_z_min_ns_test=vec_M_zams_test(ll(1));

%And then we compute the total number of BHs (stars that will become one):
N_bh_initial(s)=sum(vec_m_initial(s,:)>M_z_min_bh_test);
toc
 end 
     
%Now, we take the values we obtained before and use them to determine which
%stars are retained and which ones are not:
mat_v_esc=fun_v_esc(r_h,M_cl);
for dd=1:length(r_h(1,:))
for ff=1:length(r_h(:,1))
vec_m_final=[];%We initialize the vector that will store the retained masses.
vec_m_bh_final=[];
tic  
%In order to not use so many if statements we are going to find which
%positions in the vector vec_m_initial have a mass smaller than the minimum
%neutron star mass. Because this stars are retained and considering them in
%an if statement only slows the computation:
pos_stars=vec_m_initial(ff,:)<M_z_min_ns_test;%Positioon of such stars in the vector vec_m_initial. 
%(we get 1's in the positions of the stars that verify the requirement, and 0 for the others)
vec_m_final_i=vec_m_initial(ff,:).*pos_stars;
%But we need to eliminate the 0's and only consider those that are
%different  from 0.
vec_m_final=nonzeros(vec_m_final_i)';
%And the vector of masses for the NS and BHs before considering the
%probabilities for them to be kicked out are:
vec_bh_ns=vec_m_initial(ff,:)-vec_m_final_i;
vec_bh_ns=nonzeros(vec_bh_ns)';%Vector that contains the initial stars that are (or will be, basically, as these are ZAMS masses) BHs or NSs.
%And now we do an if statement for this stars:
ll=length(vec_m_final); %We initialize ll so that we fill the vector after that position.
%We calculate only once the probability for the initial vector of masses:
    f_ret=(1-feval(@prob_escape,Z,vec_bh_ns,1.4,265,mat_v_esc(dd,ff),k)); %As it can be seen by default we choose
    %a velocity dispersion of 265km/s and a mean neutron star mass of 1.4
    %solar masses.
%It is also important to see that later, in order to really compute the
%number of BHs and NSs we need to create a vector that stores the BHs and
%neutron stars:
vec_bh_ns_final=[];
%For that we initialize the variable nn
nn=1;
%But we need to transform this into the masses that the remnant stars will
%really have (not the ZAMS masses that the BHs or NSs previously had):
vec_bh_ns=fun_M_rem(Z,vec_bh_ns);
     for jj=1:length(vec_bh_ns)
         if vec_bh_ns(jj)>3
            rr=rand;
            if rr<=f_ret(jj)
                vec_m_final(ll)=vec_bh_ns(jj);
                vec_bh_ns_final(nn)=vec_bh_ns(jj);
                ll=ll+1;nn=nn+1;
             else
                ll=ll;nn=nn;
             end
         else
            rr=rand;
            if rr<=(1-feval(@prob_esc_ns,265,mat_v_esc(dd,ff)))
                vec_m_final(ll)=vec_bh_ns(jj);
                vec_bh_ns_final(nn)=vec_bh_ns(jj);
                ll=ll+1;nn=nn+1;
            else
                ll=ll;nn=nn;
            end
         end
     end
     %And now we store the values for the fraction of BHs retained in the
     %cluster after considering the supernova kicks
    N_bh_final(dd,ff)=sum(vec_bh_ns_final>3);
    f_N_bh_relative(dd,ff)=N_bh_final(dd,ff)./N_bh_initial(ff);
    %And to obtain the mass fractions:
    vec_m_bh_final=vec_bh_ns_final(find(vec_bh_ns_final>3));
    f_M_bh(dd,ff)=sum(vec_m_bh_final)/(M_cl(dd,ff));
   toc
end
end
f_N_bh_total=f_N_bh_total+f_N_bh_relative;
f_M_bh_total=f_M_bh_total+f_M_bh;
end
%And now we compute the mean values:
f_N_bh_mean=f_N_bh_total./(length(vec_ii));
f_M_bh_mean=f_M_bh_total./(length(vec_ii));
figure()
subplot(1,2,1)
contour(M_cl/(10^4),r_h,f_N_bh_mean);
colorbar
%clabel(C,h)
title(['BH retention fraction. Z=' num2str(Z) '. ' num2str(kick)])
%title(['BH retention fraction. Z=' num2str(Z) '. W/ fallback , sigma 265'])
xlabel('M_{cl} (10^4 M_{sun})')
ylabel('r_h (in pc)')
set(gca, 'YScale', 'log')
%set(gca, 'XScale', 'log')
hold on
subplot(1,2,2)
contour(M_cl/(10^4),r_h,f_M_bh);
colorbar
%clabel(C,h)
title(['BH Mass fraction. Z=' num2str(Z) '. ' num2str(kick)])
%title(['BH retention fraction. Z=' num2str(Z) '. W/ fallback , sigma 265'])
xlabel('M_{cl} (10^4 M_{sun})')
ylabel('r_h (in pc)')
set(gca, 'YScale', 'log')
%set(gca, 'XScale', 'log')
hold off
end
end
