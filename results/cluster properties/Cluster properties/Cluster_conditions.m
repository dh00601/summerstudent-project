clear all; close all; format compact; format long g;
%% PROBABILITY OF ESCAPE AS A FUNCTION OF BH MASS
%We calculate the probability of escape for a BH assuming a Maxwellian
%distribution with velocity dispersion sigma. Assuming same momentum for
%both NS and BH the velocity dispersion for the BH is reduced. The function
%used is:
%{
%Input:
%                M_ns: neutron star mass (in solar masses)
%                M_bh: black hole mass (in solar masses)
%                sig_ns: neutron star velocity dispersion (in km/s)
%                v_esc: escape velocity

%Output:
%                p: probability of v>vesc
function p=prob_esc(M_ns,M_bh,sig_ns,v_esc)
sig_bh=(M_ns./M_bh).*sig_ns;
p=sqrt(2/pi)*(v_esc./sig_bh).*exp(-(v_esc.^2)./(2*(sig_bh).^2))+(1-erf(v_esc./(sqrt(2)*sig_bh)));
end
As it can be seen with the function we still haven't implemented fallback,
that is probably the next step in improving this approximation.


We assume a velocity dispersion of 265km/s as argued by Morscher, Umbreit, Farr & Rasio 2002. We would, in this case,
get higher probabilities of escape for a given mass compared to the
results obtained via a dispersion of 190km/s. The observational evidence
for this value is found in "A statistical study of 233 pulsar proper
motions" by Hobbs, Lorimer,Lyne and Kramer 2005
We could asssume a velocity dispersion of 190km/s as it is used by
Hansen & Phinney in "The Pulsar Kick Velocity Distribution".
%}
M_ns=1.5;%In solar masses
sig_ns=265;vec_M=linspace(0,40);
%We try different values for the escape velocity:
v_esc=50;
vec_p=[];
for ii=1:length(vec_M)
    M_bh=vec_M(ii);
    p=prob_esc(M_ns,M_bh,sig_ns,v_esc);
    vec_p=[vec_p p];
end
figure (1)
plot(vec_M,vec_p,'-b')
title('Probability of escaping as a function of BH mass (w/o fallback')
xlabel('BH mass (in solar masses)')
ylabel('prob(v>vesc)')

hold on
v_esc=40;
vec_p=[];
for ii=1:length(vec_M)
    M_bh=vec_M(ii);
    p=prob_esc(M_ns,M_bh,sig_ns,v_esc);
    vec_p=[vec_p p];
end
figure(1)
plot(vec_M,vec_p,'-r')
hold on

v_esc=30;
vec_p=[];
for ii=1:length(vec_M)
    M_bh=vec_M(ii);
    p=prob_esc(M_ns,M_bh,sig_ns,v_esc);
    vec_p=[vec_p p];
end
figure(1)
plot(vec_M,vec_p,'-k')

legend('vesc=50','vesc=40','vesc=30')

%% CALCULATION OF THE ESCAPE VELOCITY AS A FUNCTION OF DENSITY AND MASS OF THE CLUSTER

%In this section, in order to calculate the escape velocity we use the
%results from Fabio Antonini & Mark Gieles from "Population synthesis of
%black hole binary mergers from star clusters" 2019.

%We start by defining some vectors for the possible masses and radius that
%we are going to give to the cluster.
vec_M=linspace(10^3,10^6,10^3); %In solar masses.
vec_r=linspace(0.5,10,1000); %In parsecs.
%We then define a meshgrid for this two vectors:
[M,r]=meshgrid(vec_M,vec_r);
%We then use the model to determine the escape velocity from the cluster:
rho_h=3*M./(8*pi*(r).^3);
M_5=M./(10^5);
rho_5=rho_h./(10^5);
f_c=1;
v_esc=50*((M_5).^(1/3)).*((rho_5).^(1/6)).*f_c;
%Then we plot it:
figure(2)
contour(M_5,r,v_esc,[10,20,30,40,50,75,100],'ShowText','on')
title('Contour plot for the v escape as a function of M_5 and r_h')
xlabel('M_5    (M_{cl}/(10^5 M_{sun})')
ylabel('r_h (pc)')

%% CONNECTION WITH THE PROBABILITY OF ESCAPE FOR DIFFERENT BLACK HOLE MASSES
%We now try and plot the probability os escape as a function of the BH
%mass. This will be useful if we combine it with the results from the
%previous section in order to set the initial conditions for the globular
%cluster.
%Then:
v_esc=linspace(20,200,1000); %In km/s
M=linspace(3,40,1000); %In solar masses;
[M,v_esc]=meshgrid(M,v_esc);
M_ns=1.5; %Mass of the neutron star (in solar masses)
sig_ns=265;%Velocity dispersion for the neutron star (in km/s);
p=prob_esc(M_ns,M,sig_ns,v_esc); %Probability of escape
%And then we plot it:
figure(3)
contour(M,v_esc,p,[0.01, 0.25, 0.50, 0.75, 0.9],'ShowText','on')
title('Probability of escape for different BH masses and escape velocities (w/o fallback')
xlabel('BH mass (in solar masses)')
ylabel('Escape velocity (in km/s)')

%% PROBABILITIES OF ESCAPING FOR  A CERTAIN BH MASS IN DIFFERENT CLUSTERS
%We start with a BH of 10 solar masses:
v_esc=linspace(0,200,1000); %In km/s;
M_ns=1.5;M_bh=10; %In solar masses
sig_ns=265; %In km/s;
p=prob_esc(M_ns,M_bh,sig_ns,v_esc); %Probability of escape
figure(4)
plot(v_esc,p,'-k')
title('Probabilities of escape (w/o fallback')
xlabel('Escape velocity (km/s)')
ylabel('Probability')
hold on
%20 solar masses:
M_bh=20;
p=prob_esc(M_ns,M_bh,sig_ns,v_esc);
plot(v_esc,p,'-b')
hold on
%30 solar masses:
M_bh=30;
p=prob_esc(M_ns,M_bh,sig_ns,v_esc);
plot(v_esc,p,'-r')
%40 solar masses:
M_bh=40;
p=prob_esc(M_ns,M_bh,sig_ns,v_esc);
plot(v_esc,p,'-g')
legend('M=10','M=20','M=30','M=40')

%% PROBABILITIES OF ESCAPING FOR  A CERTAIN BH MASS IN DIFFERENT CLUSTERS BUT NOW AS A FUNCTION OF M_5 AND r_h IN THE FORM OF A CONTOUR PLOT
vec_M=linspace(10^3,10^6,10^3); %In solar masses.
vec_r=linspace(0.5,5,1000); %In parsecs.
%We then define a meshgrid for this two vectors:
[M,r]=meshgrid(vec_M,vec_r);
%We then use the model to determine the escape velocity from the cluster:
rho_h=3*M./(8*pi*(r).^3);
M_5=M./(10^5);
rho_5=rho_h./(10^5);
f_c=1;
v_esc=50*((M_5).^(1/3)).*((rho_5).^(1/6)).*f_c;
M_ns=1.5;sig_ns=265;

%10 solar mass BH:
M_bh=10;
p=prob_esc(M_ns,M_bh,sig_ns,v_esc);
figure(5)
subplot(2,2,1)
contour(M_5,r,p,[0.01, 0.25, 0.50, 0.75, 0.9],'ShowText','on')
title('Prob escape (w/o fb)')
xlabel('M_5    (M_{cl}/(10^5 M_{sun})')
ylabel('r_h (pc)')
legend('M=10')

%20 solar mass BH:
M_bh=20;
p=prob_esc(M_ns,M_bh,sig_ns,v_esc);
figure(5)
subplot(2,2,2)
contour(M_5,r,p,[0.01, 0.25, 0.50, 0.75, 0.9],'ShowText','on')
title('Prob escape (w/o fb)')
xlabel('M_5    (M_{cl}/(10^5 M_{sun})')
ylabel('r_h (pc)')
legend('M=20')

%30 solar mass BH:
M_bh=30;
p=prob_esc(M_ns,M_bh,sig_ns,v_esc);
figure(5)
subplot(2,2,3)
contour(M_5,r,p,[0.01, 0.25, 0.50, 0.75, 0.9],'ShowText','on')
title('Prob escape (w/o fb)')
xlabel('M_5    (M_{cl}/(10^5 M_{sun})')
ylabel('r_h (pc)')
legend('M=30')

%40 solar mass BH:
M_bh=40;
p=prob_esc(M_ns,M_bh,sig_ns,v_esc);
figure(5)
subplot(2,2,4)
contour(M_5,r,p,[0.01, 0.25, 0.50, 0.75, 0.9],'ShowText','on')
title('Prob escape (w/o fb)')
xlabel('M_5    (M_{cl}/(10^5 M_{sun})')
ylabel('r_h (pc)')
legend('M=40')
%{
%% CONTOUR PLOT OF THE MASS OF THE STELLAR REMNANT AS A FUNCTION OF M_ZAMS AND Z
%{
We get all the fiting formulas for M_rem and M_co from "The mass spectrum
of compact remnants from the PARSEC stellar evolution tracks" from Spera,
Mapelli & Bressan 2015.
We use the values they give for the outputs of SEVN with the delayed SN
model and the PARSEC stellar evolution isochrones.
%}

vec_M_zams=linspace(0.1,100,1000);
vec_Z=linspace(1e-5,1e-2,1000);
%We then define a meshgrid for this two vectors:
[M_zams,Z]=meshgrid(vec_M_zams,vec_Z);
%And then using fun_M_rem we obtain:
M_rem=fun_M_rem(Z,M_zams);
%And then we plot it:
figure(6)
contour(M_zams,Z,M_rem,[20 30 40],'ShowText','on')
title('M_{rem} as a function of Z and M_{zams}')
xlabel('M_{zams} (In solar masses)')
ylabel('Z')
ylim([1e-5,4e-3])
%}
%% M_rem as a function of M_zams for different metallicities
vec_M_zams=linspace(10,100,1000);
vec_M_rem=zeros(1,length(vec_M_zams));
%If we set Z=1e-4:
Z=1e-4;
for ii=1:length(vec_M_zams)
 vec_M_rem(ii)=fun_M_rem(Z,vec_M_zams(ii));  
end
figure(7)
plot(vec_M_zams,vec_M_rem,'-k')
title('M_{rem} as a function of M_{zams}')
xlabel('M_{zams} (In solar masses)')
ylabel('M_{rem} (In solar masses)')
hold on
%Z=1e-3:
Z=1e-3;
for ii=1:length(vec_M_zams)
 vec_M_rem(ii)=fun_M_rem(Z,vec_M_zams(ii));  
end
plot(vec_M_zams,vec_M_rem,'-b')
hold on

%Z=2e-2:
Z=2e-2;
for ii=1:length(vec_M_zams)
 vec_M_rem(ii)=fun_M_rem(Z,vec_M_zams(ii));  
end
plot(vec_M_zams,vec_M_rem,'-r')
hold on

legend('Z=1e-4','Z=1e-3','Z=2e-2')

%% Fallback fraction as a function of M_zams for given metallicities:
vec_M_zams=linspace(10,100,1000);
vec_f_fb=zeros(1,length(vec_M_zams));
vec_M_fb=zeros(1,length(vec_M_zams));
%If we set Z=1e-4:
Z=1e-4;
for ii=1:length(vec_M_zams)
 vec_f_fb(ii)=fun_fb(Z,vec_M_zams(ii));  
end
figure(8)
plot(vec_M_zams,vec_f_fb,'-k')
title('Fallback fraction as a function of M_{zams}')
xlabel('M_{zams} (In solar masses)')
ylabel('f_{fb}')
hold on
%Z=1e-3:
Z=1e-3;
for ii=1:length(vec_M_zams)
 vec_f_fb(ii)=fun_fb(Z,vec_M_zams(ii));   
end
plot(vec_M_zams,vec_f_fb,'-b')
hold on

%Z=2e-2:
Z=2e-2;
for ii=1:length(vec_M_zams)
 vec_f_fb(ii)=fun_fb(Z,vec_M_zams(ii));  
end
plot(vec_M_zams,vec_f_fb,'-r')
hold on

legend('Z=1e-4','Z=1e-3','Z=2e-2')

%% PROBABILITY OF ESCAPE AS A FUNCTION OF BH MASS WITH FALLBACK
%We calculate the probability of escape for a BH assuming a Maxwellian
%distribution with velocity dispersion sigma=265km/s. Assuming same momentum for
%both NS and BH the velocity dispersion for the BH is reduced. We also take
%into account fallback as described in the previous two sections and
%fun_fb.m using the function prob_esc_with_fb.m

%The values that define the distribution are:
M_ns=1.5;%In solar masses
sig_ns=265;
%We start by defining different values for M_zams:
vec_M_zams=linspace(10,200,2000);
%We will assume a standard escape velocity of 30km/s:
v_esc=30;
%This way we compute for different metallicities:

%Z=1e-4:
Z=1e-4;
prob_w_fb=zeros(1,length(vec_M_zams));
vec_M_bh=zeros(1,length(vec_M_zams));
for ii=1:length(vec_M_bh)
    M_zams=vec_M_zams(ii);
    prob_w_fb(ii)=prob_esc_with_fb(Z,M_zams,M_ns,sig_ns,v_esc);
    vec_M_bh(ii)=fun_M_rem(Z,M_zams);
end
figure(9)
plot(vec_M_bh,prob_w_fb,'-k')
title('Probability as a function of M_{bh}')
xlabel('M_{bh}  (in solar masses)')
ylabel('Probability of escaping')
hold on

%Z=1e-3:
Z=1e-3;
prob_w_fb=zeros(1,length(vec_M_zams));
vec_M_bh=zeros(1,length(vec_M_zams));
for ii=1:length(vec_M_bh)
    M_zams=vec_M_zams(ii);
    prob_w_fb(ii)=prob_esc_with_fb(Z,M_zams,M_ns,sig_ns,v_esc);
    vec_M_bh(ii)=fun_M_rem(Z,M_zams);
end
plot(vec_M_bh,prob_w_fb,'-b')
hold on

%Z=2e-2:
Z=2e-2;
prob_w_fb=zeros(1,length(vec_M_zams));
vec_M_bh=zeros(1,length(vec_M_zams));
for ii=1:length(vec_M_bh)
    M_zams=vec_M_zams(ii);
    prob_w_fb(ii)=prob_esc_with_fb(Z,M_zams,M_ns,sig_ns,v_esc);
    vec_M_bh(ii)=fun_M_rem(Z,M_zams);
end
plot(vec_M_bh,prob_w_fb,'-r')
hold on

%And now we plot the probabilities without fallback to compare the results:
vec_p_wo_fb=[];
for ii=1:length(vec_M_zams)
    M_zams=vec_M_zams(ii);
    M_bh=fun_M_rem(Z,M_zams);
    p=prob_esc(M_ns,M_bh,sig_ns,v_esc);
    vec_p_wo_fb=[vec_p_wo_fb p];
end
plot(vec_M_bh,vec_p_wo_fb,'--')

legend('Z=1e-4','Z=1e-3','Z=2e-2','w/o f_{fb}')


%% PROBABILITY OF ESCAPE AS A FUNCTION OF M_zams
%Z=1e-4:
Z=1e-4;
prob_w_fb=zeros(1,length(vec_M_zams));
vec_M_bh=zeros(1,length(vec_M_zams));
for ii=1:length(vec_M_bh)
    M_zams=vec_M_zams(ii);
    prob_w_fb(ii)=prob_esc_with_fb(Z,M_zams,M_ns,sig_ns,v_esc);
    vec_M_bh(ii)=fun_M_rem(Z,M_zams);
end
figure(10)
plot(vec_M_zams,prob_w_fb,'-k')
title('Probability as a function of M_{zams}')
xlabel('M_{zams}  (in solar masses)')
ylabel('Probability of escaping')
hold on

%Z=1e-3:
Z=1e-3;
prob_w_fb=zeros(1,length(vec_M_zams));
vec_M_bh=zeros(1,length(vec_M_zams));
for ii=1:length(vec_M_bh)
    M_zams=vec_M_zams(ii);
    prob_w_fb(ii)=prob_esc_with_fb(Z,M_zams,M_ns,sig_ns,v_esc);
    vec_M_bh(ii)=fun_M_rem(Z,M_zams);
end
plot(vec_M_zams,prob_w_fb,'-b')
hold on

%Z=2e-2:
Z=2e-2;
prob_w_fb=zeros(1,length(vec_M_zams));
vec_M_bh=zeros(1,length(vec_M_zams));
for ii=1:length(vec_M_bh)
    M_zams=vec_M_zams(ii);
    prob_w_fb(ii)=prob_esc_with_fb(Z,M_zams,M_ns,sig_ns,v_esc);
    vec_M_bh(ii)=fun_M_rem(Z,M_zams);
end
plot(vec_M_zams,prob_w_fb,'-r')
hold on

%And now we plot the probabilities without fallback to compare the results:
vec_p_wo_fb=[];
for ii=1:length(vec_M_zams)
    M_zams=vec_M_zams(ii);
    M_bh=fun_M_rem(Z,M_zams);
    p=prob_esc(M_ns,M_bh,sig_ns,v_esc);
    vec_p_wo_fb=[vec_p_wo_fb p];
end
plot(vec_M_zams,vec_p_wo_fb,'--')

legend('Z=1e-4','Z=1e-3','Z=2e-2','w/o f_{fb}')
%% CONNECTION WITH THE PROBABILITY OF ESCAPE FOR DIFFERENT BLACK HOLE MASSES (W/ FALLBACK)
%We now try and plot the probability os escape as a function of the BH
%mass. This will be useful if we combine it with the results from the
%previous section in order to set the initial conditions for the globular
%cluster.
sig_ns=265;M_ns=1.5;Z=2e-2;
%Then:
v_esc=linspace(20,50,1000); %In km/s
M=linspace(3,60,1000); %In solar masses;
[M,v_esc]=meshgrid(M,v_esc);
M_ns=1.5; %Mass of the neutron star (in solar masses)
sig_ns=265;%Velocity dispersion for the neutron star (in km/s);
p=prob_esc_with_fb(Z,M,M_ns,sig_ns,v_esc); %Probability of escape
%And then we plot it:
figure(11)
contour(M,v_esc,p,[0.01, 0.25, 0.50, 0.75, 0.9],'ShowText','on')
title('Probability of escape for different BH masses and escape velocities (w fallback')
xlabel('BH mass (in solar masses)')
ylabel('Escape velocity (in km/s)')

%% CONNECTION WITH THE PROBABILITY OF ESCAPE FOR DIFFERENT BLACK HOLE MASSES
%We now try and plot the probability os escape as a function of the BH
%mass. This will be useful if we combine it with the results from the
%previous section in order to set the initial conditions for the globular
%cluster.
%For solar metallicity:
Z=2e-2;
%Then:
v_esc=linspace(20,200,1000); %In km/s
M=linspace(3,80,1000); %In solar masses;
[M,v_esc]=meshgrid(M,v_esc);
M_bh=fun_M_rem(Z,M);
M_ns=1.5; %Mass of the neutron star (in solar masses)
sig_ns=265;%Velocity dispersion for the neutron star (in km/s);
p=prob_esc_with_fb(Z,M,M_ns,sig_ns,v_esc); %Probability of escape
%And then we plot it:
figure(12)
contour(M_bh,v_esc,p,[0.01, 0.25, 0.50, 0.75, 0.9],'ShowText','on')
title('Probability of escape for different BH masses and escape velocities (w/ fallback')
xlabel('BH mass (in solar masses)')
ylabel('Escape velocity (in km/s)')









