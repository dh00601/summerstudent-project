%% MASS OF THE CO CORE AS A FUNCTON OF Z AND M_zams
%{
Description:
             This function computes the mass of the carbon-oxygen core according to the
             results presented by Spera, Mapelli & Bressan (2015), and Fryer
             et al. (2012) for the delayed supernova mechanism.
%}
%Input:
%      Z:Metallicity
%      M_zams:Zero age main sequence mass (in solar masses)

%Output:
%      M_co:Mass of the CO core (in solar masses)
function M_co=fun_M_co(Z,M_zams)
if Z>(4e-3)
    B1=(59.63)-(2.969e3)*Z+(4.988e4)*Z.^2;
    K1=(45.04)-(2.176e3)*Z+(3.806e4)*Z.^2;
    K2=(1.389e2)-(4.664e3)*Z+(5.106e4)*Z.^2;
    delta1=(2.790e-2)-(1.78e-2)*Z+(77.05)*Z.^2;
    delta2=(6.730e-3)+(2.690)*Z-(52.39)*Z.^2;
elseif Z<(1e-3)
    B1=67.07; K1=46.89; K2=1.138e2; delta1=2.199e-2; delta2=2.602e-2;
else
    B1=(40.98)+(3.415e4)*Z-(8.064e6)*Z.^2;
    K1=(35.17)+(1.548e4)*Z-(3.759e6)*Z.^2;
    K2=(20.36)+(1.162e5)*Z-(2.276e7)*Z.^2;
    delta1=(2.500e-2)-(4.346)*Z+(1.340e3)*Z.^2;
    delta2=(1.750e-2)+(11.39)*Z-(2.902e3)*Z.^2;
end
M_co=-2.0+((B1+2).*(((0.5)./(1+10.^((K1-M_zams).*delta1)))+((0.5)./(1+10.^((K2-M_zams).*delta2)))));
end
