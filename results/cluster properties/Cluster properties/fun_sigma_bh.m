%% FUNCTION THAT YIELDS THE VELOCITY DISPERSION FOR THE DIFFERENT KICK PRESCRIPTIONS
%{
Description:
            This function yields the velocity dispersion for the different
            kick prescriptions.
%}
%Input:
%                Z: Metallicity
%                M_ns: neutron star mass (in solar masses)
%                sig_ns: neutron star velocity dispersion (in km/s)
%                k: parameter that defines the kick prescription:
%                       k=0 ==> neutrino-driven NK
%                       k=1 ==> hybrid model
%                       K=2 ==> standard kick

%Output:
%                sig_bh: the different velocity dispersions according to
%                          the kick

function sig_bh=fun_sigma_bh(Z,M_ns,sig_ns,k)
%In order to compute the BH mass we make use of fun_M_rem.m that gives the
%mass of the stellar remnant as a function of Z and M.
%We start by determining which is the minimum ZAMS mass that yields a BH.
%It is important to do it this way and not just generating the BH masses
%directly because this way we can also now which is the maximum BH mass
%that can be formed according to the fitting formulas for the stellar
%evolution.
vec_M_zams_test=linspace(7,150,50000);
vec_M_bh_test=fun_M_rem(Z,vec_M_zams_test);
ll=find(abs(vec_M_bh_test-3)<1e-2);
M_zams=vec_M_zams_test(ll(1):end);
M_bh=fun_M_rem(Z,M_zams);
%And then we can obtain the BH's velocity dispersion and the fallback
%depending on the prescription:
 if k==1
    f_fb=fun_fb(Z,M_zams);
    sig_bh=(M_ns./M_bh).*(1-f_fb).*sig_ns;
 elseif k==0
    sig_bh=(M_ns./M_bh).*sig_ns;
    f_fb=0;
 elseif k==2
    f_fb=fun_fb(Z,M_zams);
    sig_bh=(1-f_fb).*sig_ns;
 end
end
