clear all; close all; format compact; format long g;
%% SCRIPT TO TEST HOW TO EXTRACT INFORMATION FROM THE SNAP.40_0.h5part
%We start by defining where the file is:
hdf5_filename = '/user/HS103/m13239/Desktop/tests/test_9/snap.40_0.h5part';
%Once we have this we can get some info on the file by doing:
info = h5info(hdf5_filename, '/Step#0');%In this case we are obtaining information for the first step.
%As we can see we have 15 data sets in this group.
%{
%% HISTOGRAM FOR THE INITIAL MASSES
%We can be more specific, for example, to get the different masses by
%doing:
vec_m_initial=h5read(hdf5_filename, '/Step#0/M');%As one can quickly see this command yields the different masses ordered in a decreasing manner.

%And with this information we can easily plot a histogram:
figure(1)
h1=histogram(vec_m_initial,45,'FaceColor','b');
title('Number of stars for N=5000,Z=0.0001 (IMF)')
xlabel('M (M_{sun})')
ylabel('Number of stars')
set(gca, 'YScale', 'log')
%}
%% HISTOGRAM FOR THE MASSES AFTER 100 Myr (last Step)
info = h5info(hdf5_filename);
%We see that it has 274 groups. If we start by Step 0 that means that the
%last step is Step 273:
info = h5info(hdf5_filename, '/Step#273');
%If we then want the dataset of the masses for that step:
vec_m_final=h5read(hdf5_filename, '/Step#273/M');
%And we can then plot the histogram:
%{
figure(2)
h1=histogram(vec_m_final,45,'FaceColor','b');
title('Number of stars for N=5000,Z=0.0001 (100 Myr after)')
xlabel('M (M_{sun})')
ylabel('Number of stars')
set(gca, 'YScale', 'log')
%}
%% FINDING THE BLACK HOLES AND NEUTRON STARS
%The next step is to see the number of black holes (BH) and neutron stars (NS) we have after 100
%Myr and then plot a histogram for them. 
%To do that we need to find the dataset that stores the values for K*, that
%is, the stellar type. In our case for now we care for BHs, NSs, and the
%rest. 
%The values of interest of this K* are for us then:
%        13 ==> NS
%        14 ==> BH
%        the other stars

%Then, to extract that information first we analyze what do we have for
%this step:
h5disp(hdf5_filename, '/Step#273');
%After that we can see that this K* values are stored in KW. Then:
vec_values_K=h5read(hdf5_filename, '/Step#273/KW');
%As expected from the IMF (Kroupa 2001) the majority of stars are still
%low-mass main sequence star (M<0.7), and that is why most K* values are
%0.

%As the stars are ordered in the same way for the different values we can
%then, via these K* values, obtain the BH and NS masses.
vec_m_bh_final=[];vec_m_ns_final=[];

positions_bh_final=(vec_values_K==14); %1's for the stars that verify it, 0 for the others.
vec_m_bh_final=positions_bh_final.*vec_m_final;
%And now we eliminate the 0's:
vec_m_bh_final=nonzeros(vec_m_bh_final)';
N_bh_final=length(vec_m_bh_final);
M_tot_bh_final=sum(vec_m_bh_final);

%And we can do the same for the NSs:
positions_ns_final=(vec_values_K==13);
vec_m_ns_final=positions_ns_final.*vec_m_final;
vec_m_ns_final=nonzeros(vec_m_ns_final)';
N_ns_final=length(vec_m_ns_final);
M_tot_ns_final=sum(vec_m_ns_final);

%And if we want to find the total number of stars after 100 Myr we only
%need to determine the length of the vector of final masses or values of
%K*:
N_tot_final=length(vec_m_final);
%And the final total mass of the cluster:
M_tot_final=sum(vec_m_final);

%And if we then want the numbers and total mass of the stars that are
%neither BHs nor NSs:
N_tot_rest_final=N_tot_final-N_bh_final-N_ns_final;
M_tot_rest_final=M_tot_final-M_tot_bh_final-M_tot_ns_final;


%And we can then plot some pie charts:
X(1)=M_tot_rest_final;X(2)=M_tot_bh_final;X(3)=M_tot_ns_final;
%X=[M_tot_rest_final;M_tot_bh_final;M_tot_ns_final];
Y=[N_tot_rest_final N_bh_final N_ns_final];
%Before doing the pie charts it is important to notice that the values we
%get from our datasets are 'single'. But to plot them in the pie chart we
%need them to be 'double' (more precission but more memory required). To do
%that for X and Y:
X=double(X); Y=double(Y);
figure(3)
%labels = {'Others','BHs','NSs'};
%ax1 = subplot(1,2,1);
pie(X,[0 1 0])
title('Mass of the cluster after 100 Myr');
legend('Others','BHs','NSs')
%{
%It doesn't make a lot of sense to plot this numbers, because you cant
really see anything of value due to the higher number of stars that do not
become neither BHs nor NSs.
ax2 = subplot(1,2,2);
pie(ax2,Y,[0 1 0],labels)
title(ax2,'Numbers of stars in the cluster after 100 Myr');
%}