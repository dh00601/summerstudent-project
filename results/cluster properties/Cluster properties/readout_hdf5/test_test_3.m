clear all; close all; format compact; format long g;
%% SCRIPT TO TEST HOW TO EXTRACT INFORMATION FROM THE SNAP.40_0.h5part
%We start by defining where the file is:
hdf5_filename = '/user/HS103/m13239/Desktop/tests/test_3/snap.40_0.h5part';
%Once we have this we can get some info on the file by doing:
info = h5info(hdf5_filename, '/Step#0') %In this case we are obtaining information for the first step.
%As we can see we have 15 data sets in this group.
%We can be more specific, for example, to get the different masses by
%doing:
vec_m=h5read(hdf5_filename, '/Step#0/M');%As one can quickly see this command yields the different masses ordered in a decreasing manner.

%And with this information we can easily plot a histogram:
figure(1)
h1=histogram(vec_m,45,'FaceColor','b');
title('Number of stars for the cluster of test 3')
xlabel('M (M_{sun})')
ylabel('Number of stars')


