%% CLUSTER PROPERTIES
%{
Description: 
  It shows the remnant mass (M_rem) and CO core
  mass (M_co) as a function of M_zams, as well as the probability of
  escape, and the fallback fraction for different masses according to the
  kick prescription chosen.
  It also shows the probability of escape for a cluster of a certain mass,
  half-mass radius and metallicity for different BH masses, for the
  different prescriptions we have computed
%}
%}
%Input:
%                Z: Metallicity
%                k: parameter that defines the kick prescription:
%                       k=0 ==> no fallback and reduced velocity dispersion
%                       k=1 ==> fallback considered and reduced velocity
%                       dispersion
%                       K=2 ==> fallback considered and not a reduced
%                       velocity dispersion.

%Output:
%                plots explained in the description of the function (see
%                above)
function plots_M_rem_and_probs_report(Z,M_cl,r_h,k)
if k==0
    kick='W/o fallback,reduced sigma';
elseif k==1 
    kick='W/ fallback,reduced sigma';
elseif k==2
    kick='W/fallback,sigma not reduced';
end
%{
%We can also plot some information regarding M_rem, M_co, the probabilities
%of escape and the fallback fraction in one plot by using subplots:
vec_M_zams=linspace(0.07,150,1000);
vec_M_co=zeros(1,length(vec_M_zams));vec_M_rem=zeros(1,length(vec_M_zams));
vec_f_fb=zeros(1,length(vec_M_zams));vec_M_zams=linspace(10,150,2000);

vec_Z=[0.0001 0.001  0.005 0.01];
for kk=1:length(vec_Z)
    Z=vec_Z(kk);
  for ii=1:length(vec_M_zams)
    M_zams=vec_M_zams(ii);
    vec_M_co(ii)=fun_M_co(Z,M_zams);vec_M_rem(ii)=fun_M_rem(Z,M_zams);
    vec_f_fb(ii)=fun_fb(Z,M_zams);
  end
figure(1)
subplot(2,2,kk)
plot(vec_M_zams,vec_M_co,'-k')
title(['M_{rem} & M_{co}. Z=' num2str(Z)])
xlabel('M_{zams} (in M_{sun})')
ylabel('M (in M_{sun})')
hold on
plot(vec_M_zams,vec_M_rem,'-r')
hold on
%We find the M_zams for which the f_fb has certain values.
kk05=find(abs(vec_f_fb-0.5)<1e-2);M_zams_0_5=vec_M_zams(kk05(4));M_rem_0_5=fun_M_rem(Z,M_zams_0_5);
kk075=find(abs(vec_f_fb-0.75)<1e-3);M_zams_0_75=vec_M_zams(kk075(1));M_rem_0_75=fun_M_rem(Z,M_zams_0_75);
kk1=find(abs(vec_f_fb-1)<1e-5);M_zams_1=vec_M_zams(kk1(1));M_rem_1=fun_M_rem(Z,M_zams_1);
plot(M_zams_0_5,M_rem_0_5,'k*','MarkerSize',10)
hold on
plot(M_zams_0_75,M_rem_0_75,'ko','MarkerSize',10)
hold on
plot(M_zams_1,M_rem_1,'k+','MarkerSize',10)
line([M_zams_0_5 M_zams_0_5], get(gca, 'ylim'))
line([M_zams_0_75 M_zams_0_75], get(gca, 'ylim'))
line([M_zams_1 M_zams_1], get(gca, 'ylim'))
legend('M_{co}','M_{rem}','f_{fb}=0.5','f_{fb}=0.75','f_{fb}=1')
end
%}
%{
%And now we look to plot the probabilities of escape for the three
%different kick prescriptions we have computed:

vec_M_examples=linspace(10,150,1000);
vec_M_rem_examples=fun_M_rem(Z,vec_M_examples);
ll=find(abs(vec_M_bh_test-3)<1e-2);
M_z_min_bh_test=vec_M_zams_test(ll(1))
vec_M_zams=vec_M_examples(vec_M_examples>M_z_min_bh);
vec_M_rem=fun_M_rem(Z,vec_M_zams);
for k=0:2
    if k==0
     kick='W/o fallback,reduced sigma';
    elseif k==1 
     kick='W/ fallback,reduced sigma';
    elseif k==2
     kick='W/fallback,sigma not reduced';
    end
   probescape(k+1,:)=prob_escape(Z,M_zams,M_ns,sig_ns,v_esc,k);%We store it by rows
end
figure(2)
plot(vec_M_rem,probescape(1,:),'-k')
title('Probability 
%}
%{
%Now we are interested in plotting the fallback fraction according to the
%BH mass:
vec_M_zams=linspace(10,150,2000);
vec_Z=[0.0001 0.001  0.005 0.01];
for kk=1:length(vec_Z)
    Z=vec_Z(kk);
  for ii=1:length(vec_M_zams)
    M_zams=vec_M_zams(ii);
    vec_M_co(ii)=fun_M_co(Z,M_zams);vec_M_rem(ii)=fun_M_rem(Z,M_zams);
    vec_f_fb(ii)=fun_fb(Z,M_zams);
  end
figure(3)
h1(kk)=plot(vec_M_rem,vec_f_fb);
title('Fallback fraction for different Z')
xlabel('M_{rem} (M_{sun})')
ylabel('Fallback fraction')
xlim([0 40])
hold on
end
%And now we define the legend:
legend(h1,{['Z=' num2str(vec_Z(1))],['Z=' num2str(vec_Z(2))],['Z=' num2str(vec_Z(3))],['Z=' num2str(vec_Z(4))]})
%}



%And the last thing to do is to plot the probabilities themselves. As the
%behaviour is difficult to capture in a single plot these will not appear
%in the main work, but we will rathe put them in an Appendix with
%additional plots.

%We start by plotting the probability of escape as a function of BH mass
%for the three different mechanisms for 3 different escape velocities of
%10, 15 & 20 km/s for a metallicity of Z=0.0001.
v_esc=10; Z=0.0001;
vec_M_examples=linspace(10,150,10000);
vec_M_rem_examples=fun_M_rem(Z,vec_M_examples);
ll=find(abs(vec_M_rem_examples-3)<1e-2);
M_z_min_bh_test=vec_M_examples(ll(1));
vec_M_zams=vec_M_examples(vec_M_examples>M_z_min_bh_test);
vec_M_rem=fun_M_rem(Z,vec_M_zams);
prob_dif_vesc=[];
for k=0:2
   prob_dif_vesc(k+1,:)=prob_escape(Z,vec_M_zams,1.4,265,v_esc,k);
end
figure(1)
subplot(3,1,1)
h1(1)=plot(vec_M_rem,prob_dif_vesc(1,:));
title(['Probability of escape. Z=' num2str(Z)])
xlabel('M_{rem} (M_{sun})')
ylabel('Probability of escape')
xlim([3 100])
hold on
h1(2)=plot(vec_M_rem,prob_dif_vesc(2,:));
hold on
h1(3)=plot(vec_M_rem,prob_dif_vesc(3,:));
hold on
legend(h1,{'Neutrino-driven NK','Hybrid model','Standard fallback-controlled NK'})
%And now for a different escape velocity:
v_esc=15;
prob_dif_vesc=[];
for k=0:2
   prob_dif_vesc(k+1,:)=prob_escape(Z,vec_M_zams,1.4,265,v_esc,k);
end
figure(1)
subplot(3,1,2)
plot(vec_M_rem,prob_dif_vesc(1,:))
title(['Probability of escape. Z=' num2str(Z)])
xlabel('M_{rem} (M_{sun})')
ylabel('Probability of escape')
xlim([3 100])
hold on
plot(vec_M_rem,prob_dif_vesc(2,:))
hold on
plot(vec_M_rem,prob_dif_vesc(3,:))
hold on

%And for another one:
v_esc=20;
prob_dif_vesc=[];
for k=0:2
   prob_dif_vesc(k+1,:)=prob_escape(Z,vec_M_zams,1.4,265,v_esc,k);
end
figure(1)
subplot(3,1,3)
plot(vec_M_rem,prob_dif_vesc(1,:))
title(['Probability of escape. Z=' num2str(Z)])
xlabel('M_{rem} (M_{sun})')
ylabel('Probability of escape')
xlim([3 100])
hold on
plot(vec_M_rem,prob_dif_vesc(2,:))
hold on
plot(vec_M_rem,prob_dif_vesc(3,:))
hold on



%And now would be interesting to plot this probability for a different
%metallicity but for the same three velocities:

v_esc=10; Z=0.001;
vec_M_examples=linspace(10,150,10000);
vec_M_rem_examples=fun_M_rem(Z,vec_M_examples);
ll=find(abs(vec_M_rem_examples-3)<1e-2);
M_z_min_bh_test=vec_M_examples(ll(1));
vec_M_zams=vec_M_examples(vec_M_examples>M_z_min_bh_test);
vec_M_rem=fun_M_rem(Z,vec_M_zams);
prob_dif_vesc=[];
for k=0:2
   prob_dif_vesc(k+1,:)=prob_escape(Z,vec_M_zams,1.4,265,v_esc,k);
end
figure(2)
subplot(3,1,1)
h2(1)=plot(vec_M_rem,prob_dif_vesc(1,:));
title(['Probability of escape. Z=' num2str(Z)])
xlabel('M_{rem} (M_{sun})')
ylabel('Probability of escape')
xlim([3 100])
hold on
h2(2)=plot(vec_M_rem,prob_dif_vesc(2,:));
hold on
h2(3)=plot(vec_M_rem,prob_dif_vesc(3,:));
hold on
legend(h2,{'Neutrino-driven NK','Hybrid model','Standard fallback-controlled NK'})
%And now for a different escape velocity:
v_esc=15;
prob_dif_vesc=[];
for k=0:2
   prob_dif_vesc(k+1,:)=prob_escape(Z,vec_M_zams,1.4,265,v_esc,k);
end
figure(2)
subplot(3,1,2)
plot(vec_M_rem,prob_dif_vesc(1,:))
title(['Probability of escape. Z=' num2str(Z)])
xlabel('M_{rem} (M_{sun})')
ylabel('Probability of escape')
xlim([3 100])
hold on
plot(vec_M_rem,prob_dif_vesc(2,:))
hold on
plot(vec_M_rem,prob_dif_vesc(3,:))
hold on

%And for another one:
v_esc=20;
prob_dif_vesc=[];
for k=0:2
   prob_dif_vesc(k+1,:)=prob_escape(Z,vec_M_zams,1.4,265,v_esc,k);
end
figure(2)
subplot(3,1,3)
plot(vec_M_rem,prob_dif_vesc(1,:))
title(['Probability of escape. Z=' num2str(Z)])
xlabel('M_{rem} (M_{sun})')
ylabel('Probability of escape')
xlim([3 100])
hold on
plot(vec_M_rem,prob_dif_vesc(2,:))
hold on
plot(vec_M_rem,prob_dif_vesc(3,:))
hold on

end
