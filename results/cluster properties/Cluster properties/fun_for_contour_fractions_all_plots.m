%% FUNCTION THAT WILL GIVE US WHAT WE NEED IN ORDER TO FORM THE CONTOUR PLOT FOR ALL PRESCRIPTIONS AT ONCE
%{
Description: This function allows for the plotting of a contour plot for
the retention fraction of BHs for different stellar cluster masses and
half-mass radii.
%}
%{
Inputs:
       M_cl: Mass of the cluster (in solar masses)
       Z: Metallicity
       mat_v_esc: escape velocity (in km/s)
       k: parameter that defines the kick prescription:
            k=0 ==> no fallback and reduced velocity dispersion
            k=1 ==> fallback considered and reduced velocity dispersion
            K=2 ==> fallback considered and not a reduced velocity dispersion.
Output:
        f_N_bh_relative: number fraction of black holes retained compared
        to the initial number of black holes (as defined by the IMF)

IMPORTANT: THIS CODE ONLY WORKS IF THE MATRIX FOR THE POSSIBLE M_cl AND
v_esc HAVE THE SAME SIZE. (SO IN THE END ONLY WORKS AFTER THE MESHGRID IS
DONE)
For more information on the sampling and how we compute the retained stars see sampling.m
%}

function f_N_bh_relative=fun_for_contour_fractions_all_plots(M_cl,Z,r_h)
%We start by sampling the IMF. And for that we need to load the cpdf
%(cumulative probability distribution function):

load('cpdf')

vec_m_initial=[];%We will use this matrix to save the different masses. We initialize this variable so that we store every mass vector in a different row:
vec_M_cl=M_cl(1,:); %We define this vector so that the sampling for every cluster mass is only done once. This way it is faster.
tic
%And then we can sample the IMF:
 for s=1:length(vec_M_cl)
        %M_cl_it=M_cl(dd,ff);
        M_tot_initial=0;
jj=1;%We initialize this variable before starting the while loop.
  while M_tot_initial<vec_M_cl(s)
    r=rand;
    kk=find(abs(eval_fun-r)<1e-3);
    m=vec_M_examples(kk(randi(length(kk))));
    M_tot_initial=M_tot_initial+m;
    vec_m_initial(s,jj)=m;% As it can be seen we store in each row the masses of a given sampling (for a given cluster mass)
    jj=jj+1;
  end  
N_tot_initial(s,:)=length(vec_m_initial(s,:));%In this vector we save the total number of stars.
%We now have to define the minimum M_zams for NS and BHs:
vec_M_zams_test=linspace(7,40,2000);
vec_M_bh_test=fun_M_rem(Z,vec_M_zams_test);
ll=find(abs(vec_M_bh_test-3)<1e-2);
M_z_min_bh_test=vec_M_zams_test(ll(1));
%And to find the minimum NS mass: The minimum mass of 1.17 solar masses is
%given by Yudai Suwa, Takashi Yoshida, Masaru Shibata, Hideyuki Umeda & Koh
%Takahashi in "On the minimum mass of neutron stars" in 2009. The problem
%is that we cannot get M_rem=1.17 with the function we obtained via the
%PARSEC paper.That is why we set a minimum value of 1.4:
ll=find(abs(vec_M_bh_test-1.4)<1e-2);
M_z_min_ns_test=vec_M_zams_test(ll(1));

%And then we compute the total number of BHs (stars that will become one):
N_bh_initial(s)=sum(vec_m_initial(s,:)>M_z_min_bh_test);
toc
 end
 
for k=0:2
   if k==0
    kick='W/o fallback,reduced sigma';
   elseif k==1 
    kick='W/ fallback,reduced sigma';
   elseif k==2
    kick='W/ fallback,sigma not reduced';
   end
%Now, we take the values we obtained before and use them to determine which
%stars are retained and which ones are not:
mat_v_esc=fun_v_esc(r_h,M_cl);
for dd=1:length(r_h(1,:))
for ff=1:length(r_h(:,1))
vec_m_final=[];%We initialize the vector that will store the retained masses.
vec_m_bh_final=[];
tic  
%In order to not use so many if statements we are going to find which
%positions in the vector vec_m_initial have a mass smaller than the minimum
%neutron star mass. Because this stars are retained and considering them in
%an if statement only slows the computation:
pos_stars=vec_m_initial(ff,:)<M_z_min_ns_test;%Positioon of such stars in the vector vec_m_initial. 
%(we get 1's in the positions of the stars that verify the requirement, and 0 for the others)
vec_m_final_i=vec_m_initial(ff,:).*pos_stars;
%But we need to eliminate the 0's and only consider those that are
%different  from 0.
vec_m_final=nonzeros(vec_m_final_i)';
%And the vector of masses for the NS and BHs before considering the
%probabilities for them to be kicked out are:
vec_bh_ns=vec_m_initial(ff,:)-vec_m_final_i;
vec_bh_ns=nonzeros(vec_bh_ns)';%Vector that contains the initial stars that are (or will be, basically, as these are ZAMS masses) BHs or NSs.
%And now we do an if statement for this stars:
ll=length(vec_m_final); %We initialize ll so that we fill the vector after that position.
%We calculate only once the probability for the initial vector of masses:
    f_ret=(1-feval(@prob_escape,Z,vec_bh_ns,1.4,265,mat_v_esc(dd,ff),k));
    f_ret_ns=(1-feval(@prob_esc_ns,265,mat_v_esc(dd,ff)));
    %As it can be seen by default we choose
    %a velocity dispersion of 265km/s and a mean neutron star mass of 1.4
    %solar masses.
%It is also important to see that later, in order to really compute the
%number of BHs and NSs we need to create a vector that stores the BHs and
%neutron stars:
vec_bh_ns_final=[];
%For that we initialize the variable nn
nn=1;
%But we need to transform this into the masses that the remnant stars will
%really have (not the ZAMS masses that the BHs or NSs previously had):
vec_bh_ns=fun_M_rem(Z,vec_bh_ns);
     for jj=1:length(vec_bh_ns)
         if vec_bh_ns(jj)>3
            rr=rand;
            if rr<=f_ret(jj)
                vec_m_final(ll)=vec_bh_ns(jj);
                vec_bh_ns_final(nn)=vec_bh_ns(jj);
                ll=ll+1;nn=nn+1;
             else
                ll=ll;nn=nn;
             end
         else
            rr=rand;
            if rr<=f_ret_ns
                vec_m_final(ll)=vec_bh_ns(jj);
                vec_bh_ns_final(nn)=vec_bh_ns(jj);
                ll=ll+1;nn=nn+1;
            else
                ll=ll;nn=nn;
            end
         end
     end
     %And now we store the values for the fraction of BHs retained in the
     %cluster after considering the supernova kicks
    N_bh_final(dd,ff)=sum(vec_bh_ns_final>3);
    f_N_bh_relative(dd,ff)=N_bh_final(dd,ff)./N_bh_initial(ff);
    %And to obtain the mass fractions:
    vec_m_bh_final=vec_bh_ns_final(find(vec_bh_ns_final>3));
    f_M_bh(dd,ff)=sum(vec_m_bh_final)/(M_cl(dd,ff));
   toc
end
end
figure()
subplot(1,2,1)
contour(M_cl/(10^5),r_h,f_N_bh_relative);
hold on
colorbar
title(['BH retention fraction. Z=' num2str(Z) '. ' num2str(kick)])
xlabel('M_{cl} (10^5 M_{sun})')
ylabel('r_h (in pc)')
%set(gca, 'YScale', 'log')

%And only use this if you want to plot the datapoints for certain clusters: (10^1
%because we are plotting M/10^5)
hold on

h1(1)=plot(0.67*10^1,0.59,'.k');hold on %2298
text(0.67*10^1,0.59,'2298');
h1(2)=plot(0.9*10^1,0.64,'.k');hold on %6218
text(0.9*10^1,0.64,'6218');
h1(3)=plot(0.85*10^1,0.42,'.k');hold on %6254
text(0.85*10^1,0.42,'6254');
h1(4)=plot(1*10^1,0.23,'.k');hold on %6341
text(1*10^1,0.23,'6341');
h1(5)=plot(0.31*10^1,1.08,'.k');hold on %6352
text(0.31*10^1,1.08,'6352');
h1(6)=plot(0.26*10^1,0.17,'.k');hold on %6397
text(0.26*10^1,0.17,'6397');
h1(7)=plot(0.33*10^1,1.18,'.k');hold on %6496
text(0.33*10^1,1.18,'6496');
h1(8)=plot(0.91*10^1,0.42,'.k');hold on %6656
text(0.91*10^1,0.42,'6656');
h1(9)=plot(0.55*10^1,0.24,'.k');hold on %6752
text(0.55*10^1,0.24,'6752');
h1(10)=plot(0.15*10^1,0.47,'.k');hold on %6838
text(0.15*10^1,0.47,'6838');


subplot(1,2,2)
contour(M_cl/(10^5),r_h,f_M_bh);
colorbar
title(['BH Mass fraction. Z=' num2str(Z) '. ' num2str(kick)])
xlabel('M_{cl} (10^5 M_{sun})')
ylabel('r_h (in pc)')
%set(gca, 'YScale', 'log')

%Only use this if you want to plot some clusters on top of the cluster
%fractions

hold on

h1(1)=plot(0.67*10^1,0.59,'.k');hold on %2298
text(0.67*10^1,0.59,'2298');
h1(2)=plot(0.9*10^1,0.64,'.k');hold on %6218
text(0.9*10^1,0.64,'6218');
h1(3)=plot(0.85*10^1,0.42,'.k');hold on %6254
text(0.85*10^1,0.42,'6254');
h1(4)=plot(1*10^1,0.23,'.k');hold on %6341
text(1*10^1,0.23,'6341');
h1(5)=plot(0.31*10^1,1.08,'.k');hold on %6352
text(0.31*10^1,1.08,'6352');
h1(6)=plot(0.26*10^1,0.17,'.k');hold on %6397
text(0.26*10^1,0.17,'6397');
h1(7)=plot(0.33*10^1,1.18,'.k');hold on %6496
text(0.33*10^1,1.18,'6496');
h1(8)=plot(0.91*10^1,0.42,'.k');hold on %6656
text(0.91*10^1,0.42,'6656');
h1(9)=plot(0.55*10^1,0.24,'.k');hold on %6752
text(0.55*10^1,0.24,'6752');
h1(10)=plot(0.15*10^1,0.47,'.k');hold on %6838
text(0.15*10^1,0.47,'6838');

%{
%In order to plot the points themselves and not the isolines formed by the
%contour plot we analyse and plot the matrices row by row:
for l=1:length(M_cl(1,:))
figure(k+2)
subplot(1,2,1)
scatter3(M_cl(l,:),r_h(l,:),f_N_bh_relative(l,:),'x')
title(['BH retention fraction. Z=' num2str(Z) '. ' num2str(kick)])
xlabel('M_{cl} (10^5 M_{sun})')
ylabel('r_h (in pc)')
set(gca, 'YScale', 'log')
colorbar
hold on

subplot(1,2,2)
scatter3(M_cl(l,:),r_h(l,:),f_M_bh(l,:),'x')
title(['BH Mass fraction. Z=' num2str(Z) '. ' num2str(kick)])
xlabel('M_{cl} (10^4 M_{sun})')
ylabel('r_h (in pc)')
set(gca, 'YScale', 'log')
colorbar 
hold on
end
%}
end
end
