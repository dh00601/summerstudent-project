clear all; format compact; format long g
%% CLUSTER PROPERTIES FOR DIFFERENT METALLICITIES
vec_M_zams=linspace(10,100,2000);
M_ns=1.5; %in solar masses
sig_ns=265; %in km/s
%% Z=2E-2:
Z=2e-2;
%We compute M_co and M_rem as a function of M_zams
vec_M_co=zeros(1,length(vec_M_zams));
vec_M_rem=zeros(1,length(vec_M_zams));
vec_f_fb=zeros(1,length(vec_M_zams));
for ii=1:length(vec_M_zams)
    M_zams=vec_M_zams(ii);
    vec_M_co(ii)=fun_M_co(Z,M_zams);
    vec_M_rem(ii)=fun_M_rem(Z,M_zams);
    vec_f_fb(ii)=fun_fb(Z,M_zams);
end
figure(3)

subplot(2,2,[1,3])
plot(vec_M_zams,vec_M_co,'-k')
text(15,25,'Z=2e-2')
title('M_{rem} and M_{co} (M_{zams})')
xlabel('M_{zams} (in M_{sun})')
ylabel('M in solar masses')
hold on
plot(vec_M_zams,vec_M_rem,'-r')
hold on
%We find the M_zams for which the f_fb has certain values.
kk05=find(abs(vec_f_fb-0.5)<1e-2);
M_zams_0_5=vec_M_zams(kk05(4));
M_rem_0_5=fun_M_rem(Z,M_zams_0_5);
kk075=find(abs(vec_f_fb-0.75)<1e-3);
M_zams_0_75=vec_M_zams(kk075(1));
M_rem_0_75=fun_M_rem(Z,M_zams_0_75);
kk1=find(abs(vec_f_fb-1)<1e-5);
M_zams_1=vec_M_zams(kk1(1));
M_rem_1=fun_M_rem(Z,M_zams_1);
plot(M_zams_0_5,M_rem_0_5,'k*','MarkerSize',10)
hold on
plot(M_zams_0_75,M_rem_0_75,'ko','MarkerSize',10)
hold on
plot(M_zams_1,M_rem_1,'k+','MarkerSize',10)
line([M_zams_0_5 M_zams_0_5], get(gca, 'ylim'))
line([M_zams_0_75 M_zams_0_75], get(gca, 'ylim'))
line([M_zams_1 M_zams_1], get(gca, 'ylim'))
legend('M_{co}','M_{rem}','f_{fb}=0.5','f_{fb}=0.75','f_{fb}=1')
%We compute probability of escape for v_esc=30,40,50 with and without
%fallback:
vec_v_esc=[30 40 50];
prob_w_fb=zeros(3,length(vec_v_esc));%We save the probabilites for each v_esc in a given row
prob_wo_fb=zeros(3,length(vec_v_esc));
for ii=1:length(vec_v_esc)
    v_esc=vec_v_esc(ii);
    for jj=1:length(vec_M_rem)
        M_zams=vec_M_zams(jj);
        M_bh=fun_M_rem(Z,M_zams);
        prob_w_fb(ii,jj)=prob_esc_with_fb(Z,M_zams,M_ns,sig_ns,v_esc);
        prob_wo_fb(ii,jj)=prob_esc(M_ns,M_bh,sig_ns,v_esc);
    end
end
%We then plot the probability of escape as a function of BH mass:
subplot(2,2,2)
plot(vec_M_rem,prob_w_fb(1,:),'-b')
title('Probability of escape w/ fb')
xlabel('M_{rem} (in M_{sun})')
ylabel('Probability of escape')
xlim([0 15])
xticks([0 2.5 5 7.5 10 12.5 15 17.5 20])
hold on
plot(vec_M_rem,prob_w_fb(2,:),'-r')
hold on
plot(vec_M_rem,prob_w_fb(3,:),'-k')
legend('v_{esc}=30','v_{esc}=40','v_{esc}=50')


%And now we compute the differences in probability that we get with and
%without fallback:
dif_prob1=abs(prob_w_fb(1,:)-prob_wo_fb(1,:));
dif_prob2=abs(prob_w_fb(2,:)-prob_wo_fb(2,:));
dif_prob3=abs(prob_w_fb(3,:)-prob_wo_fb(3,:));
subplot(2,2,4)
plot(vec_M_rem,dif_prob1,'-.b')
title('Difference in probability w/ and w/o fallback')
xlabel('M_{rem} (in M_{sun})')
ylabel('Difference in probability')
xlim([0 15])%Before 3.5 there is no fallback
xticks([0 2.5 5 7.5 10 12.5 15 17.5 20])
hold on
plot(vec_M_rem,dif_prob2,'-.r')
hold on
plot(vec_M_rem,dif_prob3,'-.k')
legend('v_{esc}=30','v_{esc}=40','v_{esc}=50')
%{
%% Z=1E-3:
Z=1e-3;
%We compute M_co and M_rem as a function of M_zams
vec_M_co=zeros(1,length(vec_M_zams));
vec_M_rem=zeros(1,length(vec_M_zams));
for ii=1:length(vec_M_zams)
    M_zams=vec_M_zams(ii);
    vec_M_co(ii)=fun_M_co(Z,M_zams);
    vec_M_rem(ii)=fun_M_rem(Z,M_zams);
end
figure(2)

subplot(2,2,1)
plot(vec_M_zams,vec_M_co)
text(15,40,'Z=1e-3')
title('M_{co} (M_{zams})')
xlabel('M_{zams} (in M_{sun})')
ylabel('M_{co} (in M_{sun})')
subplot(2,2,2)
plot(vec_M_zams,vec_M_rem)
title('M_{rem} (M_{zams})')
xlabel('M_{zams} (in M_{sun})')
ylabel('M_{rem} (in M_{sun})')
%We compute probability of escape for v_esc=30,40,50 with and without
%fallback:
vec_v_esc=[30 40 50];
prob_w_fb=zeros(3,length(vec_v_esc));%We save the probabilites for each v_esc in a given row
prob_wo_fb=zeros(3,length(vec_v_esc));
for ii=1:length(vec_v_esc)
    v_esc=vec_v_esc(ii);
    for jj=1:length(vec_M_rem)
        M_zams=vec_M_zams(jj);
        M_bh=fun_M_rem(Z,M_zams);
        prob_w_fb(ii,jj)=prob_esc_with_fb(Z,M_zams,M_ns,sig_ns,v_esc);
        prob_wo_fb(ii,jj)=prob_esc(M_ns,M_bh,sig_ns,v_esc);
    end
end
%We then plot the probability of escape as a function of BH mass:
subplot(2,2,3)
plot(vec_M_rem,prob_w_fb(1,:),'-b')
title('Probability of escape')
xlabel('M_{rem} (in M_{sun})')
ylabel('Probability of escape')
xlim([0 20])
hold on
plot(vec_M_rem,prob_w_fb(2,:),'-r')
hold on
plot(vec_M_rem,prob_w_fb(3,:),'-k')
legend('v_{esc}=30','v_{esc}=40','v_{esc}=50')

%And now we compute the differences in probability that we get with and
%without fallback:
dif_prob1=abs(prob_w_fb(1,:)-prob_wo_fb(1,:));
dif_prob2=abs(prob_w_fb(2,:)-prob_wo_fb(2,:));
dif_prob3=abs(prob_w_fb(3,:)-prob_wo_fb(3,:));
subplot(2,2,4)
plot(vec_M_rem,dif_prob1,'-.b')
title('Difference in probability w/ and w/o fallback')
xlabel('M_{rem} (in M_{sun})')
ylabel('Difference in probability')
xlim([3.5 25])%Because befor 3.5 there is no fallback
hold on
plot(vec_M_rem,dif_prob2,'-.r')
hold on
plot(vec_M_rem,dif_prob3,'-.k')
legend('v_{esc}=30','v_{esc}=40','v_{esc}=50')

%% Z=1E-4:
Z=1e-4;
%We compute M_co and M_rem as a function of M_zams
vec_M_co=zeros(1,length(vec_M_zams));
vec_M_rem=zeros(1,length(vec_M_zams));
for ii=1:length(vec_M_zams)
    M_zams=vec_M_zams(ii);
    vec_M_co(ii)=fun_M_co(Z,M_zams);
    vec_M_rem(ii)=fun_M_rem(Z,M_zams);
end
figure(3)

subplot(2,2,1)
plot(vec_M_zams,vec_M_co)
text(15,40,'Z=1e-4')
title('M_{co} (M_{zams})')
xlabel('M_{zams} (in M_{sun})')
ylabel('M_{co} (in M_{sun})')
subplot(2,2,2)
plot(vec_M_zams,vec_M_rem)
title('M_{rem} (M_{zams})')
xlabel('M_{zams} (in M_{sun})')
ylabel('M_{rem} (in M_{sun})')
%We compute probability of escape for v_esc=30,40,50 with and without
%fallback:
vec_v_esc=[30 40 50];
prob_w_fb=zeros(3,length(vec_v_esc));%We save the probabilites for each v_esc in a given row
prob_wo_fb=zeros(3,length(vec_v_esc));
for ii=1:length(vec_v_esc)
    v_esc=vec_v_esc(ii);
    for jj=1:length(vec_M_rem)
        M_zams=vec_M_zams(jj);
        M_bh=fun_M_rem(Z,M_zams);
        prob_w_fb(ii,jj)=prob_esc_with_fb(Z,M_zams,M_ns,sig_ns,v_esc);
        prob_wo_fb(ii,jj)=prob_esc(M_ns,M_bh,sig_ns,v_esc);
    end
end
%We then plot the probability of escape as a function of BH mass:
subplot(2,2,3)
plot(vec_M_rem,prob_w_fb(1,:),'-b')
title('Probability of escape')
xlabel('M_{rem} (in M_{sun})')
ylabel('Probability of escape')
xlim([0 20])
hold on
plot(vec_M_rem,prob_w_fb(2,:),'-r')
hold on
plot(vec_M_rem,prob_w_fb(3,:),'-k')
legend('v_{esc}=30','v_{esc}=40','v_{esc}=50')

%And now we compute the differences in probability that we get with and
%without fallback:
dif_prob1=abs(prob_w_fb(1,:)-prob_wo_fb(1,:));
dif_prob2=abs(prob_w_fb(2,:)-prob_wo_fb(2,:));
dif_prob3=abs(prob_w_fb(3,:)-prob_wo_fb(3,:));
subplot(2,2,4)
plot(vec_M_rem,dif_prob1,'-.b')
title('Difference in probability w/ and w/o fallback')
xlabel('M_{rem} (in M_{sun})')
ylabel('Difference in probability')
xlim([3.5 25])%Because befor 3.5 there is no fallback
hold on
plot(vec_M_rem,dif_prob2,'-.r')
hold on
plot(vec_M_rem,dif_prob3,'-.k')
legend('v_{esc}=30','v_{esc}=40','v_{esc}=50')

%}

%% Z=1E-3:
Z=2e-3;
%We compute M_co and M_rem as a function of M_zams
vec_M_co=zeros(1,length(vec_M_zams));
vec_M_rem=zeros(1,length(vec_M_zams));
vec_f_fb=zeros(1,length(vec_M_zams));
for ii=1:length(vec_M_zams)
    M_zams=vec_M_zams(ii);
    vec_M_co(ii)=fun_M_co(Z,M_zams);
    vec_M_rem(ii)=fun_M_rem(Z,M_zams);
    vec_f_fb(ii)=fun_fb(Z,M_zams);
end
figure(4)

subplot(2,2,[1,3])
plot(vec_M_zams,vec_M_co,'-k')
text(15,65,'Z=1e-3')
title('M_{rem} and M_{co} (M_{zams})')
xlabel('M_{zams} (in M_{sun})')
ylabel('M in solar masses')
hold on
plot(vec_M_zams,vec_M_rem,'-r')
hold on
%We find the M_zams for which the f_fb has certain values.
kk05=find(abs(vec_f_fb-0.5)<1e-2);
M_zams_0_5=vec_M_zams(kk05(4));
M_rem_0_5=fun_M_rem(Z,M_zams_0_5);
kk075=find(abs(vec_f_fb-0.75)<1e-3);
M_zams_0_75=vec_M_zams(kk075(1));
M_rem_0_75=fun_M_rem(Z,M_zams_0_75);
kk1=find(abs(vec_f_fb-1)<1e-5);
M_zams_1=vec_M_zams(kk1(1));
M_rem_1=fun_M_rem(Z,M_zams_1);
plot(M_zams_0_5,M_rem_0_5,'k*','MarkerSize',10)
hold on
plot(M_zams_0_75,M_rem_0_75,'ko','MarkerSize',10)
hold on
plot(M_zams_1,M_rem_1,'k+','MarkerSize',10)
line([M_zams_0_5 M_zams_0_5], get(gca, 'ylim'))
line([M_zams_0_75 M_zams_0_75], get(gca, 'ylim'))
line([M_zams_1 M_zams_1], get(gca, 'ylim'))
legend('M_{co}','M_{rem}','f_{fb}=0.5','f_{fb}=0.75','f_{fb}=1')
%We compute probability of escape for v_esc=30,40,50 with and without
%fallback:
vec_v_esc=[30 40 50];
prob_w_fb=zeros(3,length(vec_v_esc));%We save the probabilites for each v_esc in a given row
prob_wo_fb=zeros(3,length(vec_v_esc));
for ii=1:length(vec_v_esc)
    v_esc=vec_v_esc(ii);
    for jj=1:length(vec_M_rem)
        M_zams=vec_M_zams(jj);
        M_bh=fun_M_rem(Z,M_zams);
        prob_w_fb(ii,jj)=prob_esc_with_fb(Z,M_zams,M_ns,sig_ns,v_esc);
        prob_wo_fb(ii,jj)=prob_esc(M_ns,M_bh,sig_ns,v_esc);
    end
end
%We then plot the probability of escape as a function of BH mass:
subplot(2,2,2)
plot(vec_M_rem,prob_w_fb(1,:),'-b')
title('Probability of escape w/ fb')
xlabel('M_{rem} (in M_{sun})')
ylabel('Probability of escape')
xlim([0 20])
xticks([0 2.5 5 7.5 10 12.5 15 17.5 20])
hold on
plot(vec_M_rem,prob_w_fb(2,:),'-r')
hold on
plot(vec_M_rem,prob_w_fb(3,:),'-k')
legend('v_{esc}=30','v_{esc}=40','v_{esc}=50')


%And now we compute the differences in probability that we get with and
%without fallback:
dif_prob1=abs(prob_w_fb(1,:)-prob_wo_fb(1,:));
dif_prob2=abs(prob_w_fb(2,:)-prob_wo_fb(2,:));
dif_prob3=abs(prob_w_fb(3,:)-prob_wo_fb(3,:));
subplot(2,2,4)
plot(vec_M_rem,dif_prob1,'-.b')
title('Difference in probability w/ and w/o fallback')
xlabel('M_{rem} (in M_{sun})')
ylabel('Difference in probability')
xlim([0 20])%Before 3.5 there is no fallback
xticks([0 2.5 5 7.5 10 12.5 15 17.5 20])
hold on
plot(vec_M_rem,dif_prob2,'-.r')
hold on
plot(vec_M_rem,dif_prob3,'-.k')
legend('v_{esc}=30','v_{esc}=40','v_{esc}=50')

