%% FUNCTION THAT GIVES THE PROBABILITY OF v>vesc FOR A NEUTRON STAR (NS)
%{
Description:
             This function computes the probability of escape for a given
             NS, according to the kick prescription, considering a
             Maxwellian distribution.
%}
%Input:
%                sig_ns: neutron star velocity dispersion (in km/s)
%                v_esc: escape velocity (in km/s)

%Output:
%                p: probability of v>vesc for a neutron star.
function p=prob_esc_ns(sig_ns,v_esc)
p=sqrt(2/pi)*(v_esc./sig_ns).*exp(-(v_esc.^2)./(2*(sig_ns).^2))+(1-erf(v_esc./(sqrt(2)*sig_ns)));
end