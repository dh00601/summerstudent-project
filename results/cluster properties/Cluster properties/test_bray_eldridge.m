%% TEST BRAY & ELDRIDGE (2018)
function [f_N_bh_relative,M_cl,r_h]= test_bray_eldridge(Z,M_cl,r_h)
load('cpdf')
%{
v_esc=fun_v_esc(r_h,M_cl);
vec_ii=[1:300];
mat_m=[];
N_tot=zeros(1,length(vec_ii));
M_tot=zeros(1,length(vec_ii));
for ii=1:length(vec_ii)
  jj=1;
  vec_m=[];%We will use this vector to obtain N_tot because if we try to do it via the matrix m_mat, as it is padded with 0's,we always get the same N_tot.
  while M_tot(ii)<M_cl
    r=rand;
    kk=find(abs(eval_fun-r)<1e-3);
    m=vec_M_examples(kk(randi(length(kk))));%we use this randi command because for a given r, given the resolution we can ask for, 
    %there might be more than one mass that suits such requirements. This
    %way, if there is more than one possible value, we asign that value
    %randomly.
    M_tot(ii)=M_tot(ii)+m;
    mat_m(ii,jj)=m;
    vec_m(jj)=m;
    jj=jj+1;
   end  
   N_tot(ii)=length(vec_m);
   vec_M_zams_test=linspace(7,40,2000);
   vec_M_bh_test=fun_M_rem(Z,vec_M_zams_test);
   ll=find(abs(vec_M_bh_test-3)<1e-2);
   M_z_min_bh_test=vec_M_zams_test(ll(1));
%And to find the minimum NS mass: (The minimum mass of 1.17 solar masses is
%given by Yudai Suwa, Takashi Yoshida, Masaru Shibata, Hideyuki Umeda & Koh
%Takahashi in "On the minimum mass of neutron stars" in 2009. The problem
%is that we cannot get M_rem=1.17 with the functions presented by Spera, Mapelli & Bressan (2015), and Fryer 
%et al. (2012) for the delayed supernova mechanism because the mass is too low (minimum M_zams of 7 solar masses).
%That is why we set a minimum value of 1.4:
ll=find(abs(vec_M_bh_test-1.4)<1e-2);
M_z_min_ns_test=vec_M_zams_test(ll(1));
%And then we compute the total number of stars of each of them:
N_bh(ii)=sum(vec_m>M_z_min_bh_test);
N_stars(ii)=sum(vec_m<M_z_min_ns_test);
N_ns(ii)=N_tot(ii)-N_bh(ii)-N_stars(ii);

M_bh(ii)=sum(vec_m(find(vec_m>M_z_min_bh_test)));
M_stars(ii)=sum(vec_m(find(vec_m<M_z_min_ns_test)));
M_ns(ii)=M_tot(ii)-M_bh(ii)-M_stars(ii);
end

%Now that we have sampled it different times we have to compute the results
%after the supernova kicks according to the prescription chosen:
for ii=1:length(vec_ii)
%In order to not use so many if statements we are going to find which
%positions in the vector vec_m_initial have a mass smaller than the minimum
%neutron star mass. Because this stars are retained and considering them in
%an if statement only slows the computation:
pos_stars=mat_m(ii,:)<M_z_min_ns_test;%Positioon of such stars in the vector vec_m_initial. 
%(we get 1's in the positions of the stars that verify the requirement, and 0 for the others)
vec_m_final_i=mat_m(ii,:).*pos_stars;
%But we need to eliminate the 0's and only consider those that are
%different  from 0.
vec_m_final=nonzeros(vec_m_final_i)';
%And the vector of masses for the NS and BHs before considering the
%probabilities for them to be kicked out are:
vec_bh_ns_i=mat_m(ii,:)-vec_m_final_i;
vec_bh_ns=nonzeros(vec_bh_ns_i)';%Vector that contains the initial stars that are (or will be, basically, as these are ZAMS masses) BHs or NSs.
%And now we do an if statement for this stars:

ll=length(vec_m_final); %We initialize ll so that we fill the vector after that position.
%The thing now is that we do not have a probability, but a function for the
%velocity of the star, so we can compare directly if the star is ejected or
%not.
M_rem=fun_M_rem(Z,vec_bh_ns); M_fin=0.9519.*vec_bh_ns +1.45;
M_eject=M_fin-M_rem;
v_bh_ns=abs(100.*(M_eject./M_rem) -170);
%But what we want to store in vec_m_final are the masses of the remnants,
and not the ZAMS mass that they had. That's why we do:
vec_bh_ns=fun_M_rem(Z,vec_bh_ns);
%It is also important to see that later, in order to really compute the
%number of BHs and NSs we need to create a vector that stores the BHs and
%neutron stars:
vec_bh_ns_final=[];
%For that we initialize the variable nn
nn=1;
     for jj=1:length(v_bh_ns)
         if v_bh_ns(jj)>v_esc
                ll=ll;nn=nn;
         else
                vec_m_final(ll)=vec_bh_ns(jj);
                vec_bh_ns_final(nn)=vec_bh_ns(jj);
                ll=ll+1;nn=nn+1;  
         end
     end
N_tot_final(ii)=length(vec_m_final);
     M_tot_final(ii)=sum(vec_m_final);
     N_bh_final(ii)=sum(vec_bh_ns_final>3);
     N_ns_final(ii)=sum(vec_bh_ns_final<3);
     N_stars_final(ii)=N_tot_final(ii)-N_bh_final(ii)-N_ns_final(ii);
     f_N_bh(ii)=N_bh_final(ii)/N_bh(ii);f_N_ns(ii)=N_ns_final(ii)/N_ns(ii);f_N_stars(ii)=N_stars_final(ii)/N_tot(ii);

     M_bh_final(ii)=sum(vec_bh_ns_final(find(vec_bh_ns_final>3)));
     M_ns_final(ii)=sum(vec_bh_ns_final(find(vec_bh_ns_final<3)));
     M_stars_final(ii)=M_tot_final(ii)-M_bh_final(ii)-M_ns_final(ii);
     f_m_bh(ii)=M_bh_final(ii)/M_bh(ii);f_m_ns(ii)=M_ns_final(ii)/M_ns(ii);f_m_stars=M_stars_final(ii)/M_tot(ii);
end
%And now we can plot this:
figure(2)
plot(vec_ii,N_tot,'-k')
title(['Number of stars in a cluster of M_{cl}=' num2str(M_cl) ' M_{sun}, r_h=' num2str(r_h) ' pc & Z=' num2str(Z)])
xlabel('Different samplings')
ylabel('Number of stars')
hold on
plot(vec_ii,N_tot_final,'-r')
legend('IMF (before kicks)','after kicks (Bray & Eldridge 2018)','Location','southwest')
legend('boxoff')

figure(3)
subplot(2,1,1)
plot(vec_ii,N_bh,'-r')
title(['Number of BHs in a cluster of M_{cl}=' num2str(M_cl) ' M_{sun}, r_h=' num2str(r_h) ' pc & Z=' num2str(Z)])
xlabel('Different samplings')
ylabel('Number of BHs')
hold on
plot(vec_ii,N_bh_final,'-b')
legend('IMF (before kicks)','after kicks (Bray & Eldridge 2018)','Location','southwest')
legend('boxoff')
subplot(2,1,2)
plot(vec_ii,f_N_bh,'-k')
title('Fraction of BHs retained')
xlabel('Different samplings')
ylabel('Fraction of BHs retained')

%And for these calculations we can study the stochastic nature, that is,
%what is the mean value, standard deviations, etc:
%IMF:
disp('                       VARIATIONS IN THE NUMBER OF BHs')
disp('Mean number of initial BHs:')
mean_N_bh_initial=sum(N_bh)/length(vec_ii);
disp(mean_N_bh_initial)
var=(N_bh-mean_N_bh_initial).^2;
sigma_N_bh_initial=sqrt(sum(var)/length(vec_ii));
disp('Standard deviation in the initial number of BHs:')
disp(sigma_N_bh_initial)

%After supernova kicks:
mean_N_bh_final=sum(N_bh_final)/length(vec_ii);
disp('Mean number of BHs after supernova kicks:')
disp(mean_N_bh_final)
var=(N_bh_final-mean_N_bh_final).^2;
sigma_N_bh_final=sqrt(sum(var)/length(vec_ii));
disp('Standard deviation in the number of BHs after supernova kicks:')
disp(sigma_N_bh_final)

%And now we do the same but for the number fraction of black holes:
disp('                       VARIATIONS IN THE NUMBER FRACTION OF BHs RETAINED')
disp('Mean number fraction of BHs retained:')
mean_f_N_bh=sum(f_N_bh)/length(vec_ii);
disp(mean_f_N_bh)
var=(f_N_bh-mean_f_N_bh).^2;
sigma_f_N_bh=sqrt(sum(var)/length(vec_ii));
disp('Standard deviation in the number fraction of BHs retained:')
disp(sigma_f_N_bh)
%}

%And now we rewrite what we have in order to be able to plot the contour
%plot:
%We define the following vectors:
vec_M_cl=linspace(10^3,10^6,100);
vec_v_esc=linspace(5,60,100);
%And with this we form the meshgrid:
[M_cl,mat_v_esc]=meshgrid(vec_M_cl,vec_v_esc);



vec_m_initial=[];%We will use this matrix to save the different masses. We initialize this variable so that we store every mass vector in a different row:
vec_M_cl=M_cl(1,:); %We define this vector so that the sampling for every cluster mass is only done once. This way it is faster.
tic
%And then we can sample the IMF:
 for s=1:length(vec_M_cl)
        %M_cl_it=M_cl(dd,ff);
        M_tot_initial=0;
jj=1;%We initialize this variable before starting the while loop.
  while M_tot_initial<vec_M_cl(s)
    r=rand;
    kk=find(abs(eval_fun-r)<1e-3);
    m=vec_M_examples(kk(randi(length(kk))));
    M_tot_initial=M_tot_initial+m;
    vec_m_initial(s,jj)=m;% As it can be seen we store in each row the masses of a given sampling (for a given cluster mass)
    jj=jj+1;
  end  
N_tot_initial(s,:)=length(vec_m_initial(s,:));%In this vector we save the total number of stars.
%We now have to define the minimum M_zams for NS and BHs:
vec_M_zams_test=linspace(7,40,2000);
vec_M_bh_test=fun_M_rem(Z,vec_M_zams_test);
ll=find(abs(vec_M_bh_test-3)<1e-2);
M_z_min_bh_test=vec_M_zams_test(ll(1));
%And to find the minimum NS mass: The minimum mass of 1.17 solar masses is
%given by Yudai Suwa, Takashi Yoshida, Masaru Shibata, Hideyuki Umeda & Koh
%Takahashi in "On the minimum mass of neutron stars" in 2009. The problem
%is that we cannot get M_rem=1.17 with the function we obtained via the
%PARSEC paper.That is why we set a minimum value of 1.4:
ll=find(abs(vec_M_bh_test-1.4)<1e-2);
M_z_min_ns_test=vec_M_zams_test(ll(1));

%And then we compute the total number of BHs:
N_bh_initial(s)=sum(vec_m_initial(s,:)>M_z_min_bh_test);
toc
 end
 
%Now, we take the values we obtained before and use them to determine which
%stars are retained and which ones are not:

for dd=1:length(mat_v_esc(1,:))
for ff=1:length(mat_v_esc(:,1))
vec_m_final=[];%We initialize the vector that will store the retained masses.
tic  
%In order to not use so many if statements we are going to find which
%positions in the vector vec_m_initial have a mass smaller than the minimum
%neutron star mass. Because this stars are retained and considering them in
%an if statement only slows the computation:
pos_stars=vec_m_initial(ff,:)<M_z_min_ns_test;%Positioon of such stars in the vector vec_m_initial. 
%(we get 1's in the positions of the stars that verify the requirement, and 0 for the others)
vec_m_final_i=vec_m_initial(ff,:).*pos_stars;
%But we need to eliminate the 0's and only consider those that are
%different  from 0.
vec_m_final=nonzeros(vec_m_final_i)';
%And the vector of masses for the NS and BHs before considering the
%probabilities for them to be kicked out are:
vec_bh_ns=vec_m_initial(ff,:)-vec_m_final_i;
vec_bh_ns=nonzeros(vec_bh_ns)';%Vector that contains the initial stars that are (or will be, basically, as these are ZAMS masses) BHs or NSs.
%But we need to transform this into the masses that the remnant stars will
%really have (not the ZAMS masses that the BHs or NSs previously had):
vec_bh_ns=fun_M_rem(Z,vec_bh_ns);
%And now we do an if statement for this stars:
ll=length(vec_m_final); %We initialize ll so that we fill the vector after that position.
%But now we are not dealing with probabilities:
M_rem=fun_M_rem(Z,vec_bh_ns); M_fin=0.9519.*vec_bh_ns +1.45;
M_eject=M_fin-M_rem;
v_bh_ns=abs(100.*(M_eject./M_rem) -170);
%It is also important to see that later, in order to really compute the
%number of BHs and NSs we need to create a vector that stores the BHs and
%neutron stars:
vec_bh_ns_final=[];
%For that we initialize the variable nn
nn=1;
for jj=1:length(v_bh_ns)
         if v_bh_ns(jj)>mat_v_esc(dd,ff)
                ll=ll;nn=nn;
         else
                vec_m_final(ll)=vec_bh_ns(jj);
                vec_bh_ns_final(nn)=vec_bh_ns(jj);
                ll=ll+1;nn=nn+1;
         end
     end

     %And now we store the values for the fraction of BHs retained in the
     %cluster after considering the supernova kick:
     N_bh_final(dd,ff)=sum(vec_bh_ns_final>3);
    f_N_bh_relative(dd,ff)=N_bh_final(dd,ff)./N_bh_initial(ff);
   toc
end
end
%But we want to express it as a function of r_h and nor mat_v_esc. So we
%basically use the function defined in fun_v_esc.m but rewritten so that it
%yields the half-mass radius.
r_h=(1/40)*((3/(8*pi))^(1/3))*((M_cl)./((mat_v_esc).^2));
figure(4)
[C,h]=contour(M_cl/(10^5),r_h,f_N_bh_relative,'ShowText','on');
clabel(C,h)
title(['BH retention fraction. Z=' num2str(Z) 'Bray & Eldridge'])
xlabel('M_{cl} (10^5 M_{sun})')
ylabel('r_h (in pc)')
set(gca, 'YScale', 'log')

end