%% SCRIPT WITH EVERYTHING REGARDING THE RESULTS OBTAINED VIA SAMPLING
%{
Description: 
  This function does different things. First of all it samples a
  cluster with the specifications chosen and then plots a histogram for the
  different BH masses both before and after supernova kicks. Then it also
  does 100 different samplings (vec_ii can be changed if one wants a
  different number of samplings) in order to show how a different sampling
  affects the results obtained. 
  Then it also plots a contour plot for the chosen kick prescription 
  showing the BH retention fraction for different cluster masses and 
  half-mass radii.
%}
%Input:
%                Z: Metallicity
%                M_cl: Mass of the cluster(in solar masses)
%                v_esc: escape velocity (in km/s)
%                k: parameter that defines the kick prescription:
%                       k=0 ==> no fallback and reduced velocity dispersion
%                       k=1 ==> fallback considered and reduced velocity
%                       dispersion
%                       K=2 ==> fallback considered and not a reduced
%                       velocity dispersion.

%Output:
%                plots explained in the description of the function (see
%                above)
%function [f_N_bh_relative,M_cl,r_h]=SAMPLING(Z,M_cl,r_h,k)
function SAMPLING(Z,M_cl,r_h,k)
if k==0
    kick='NEUTRINO-DRIVEN NK';
elseif k==1 
    kick='HYBRID MODEL';
elseif k==2
    kick='STANDARD FALLBACK-CONTROLLED NK';
end
%{
We do the sampling by calculating the cumulative probability distribution
function (cpdf) via the IMF, and then for each star we want to "create" in the
cluster we first asign a random value r between 0 and 1 and then find
which mass yields a value of the cpdf closest to that r. In this way we
generate stars until we reach the mass of the cluster that we are looking
for.
As this is a costly process we only computed that once, and then we simply
load the results that are in the file cpdf.mat saved as eval_fun. (we load
it by using the command load('cpdf')
In any case the code is below:
Below we show what that process is:

%We start by defining the IMF:
alfa1=0.3;alfa2=1.3;alfa3=2.3;
imf= @(M) ((M<0.08).*(M.^(-alfa1))) + ((M<0.5 & M>=0.08).*((M.^(-alfa2))*(0.08))) + ((M>=0.5).*((M.^(-alfa3))*(0.04)));
%We then define the maximum value for the cumulative probability
%distribution
%function so that we can normalise the function itself so it can be treated
%as a probability.
cpdf_max=integral(imf,0.07,150,'RelTol',1e-6,'AbsTol',1e-6);
%Once we have this function defined we need to invert it. That it, we need
%to have it such that we give a random number from 0 to 1 and it returns a
%mass. So that we asign different masses until we reach the maximum value
%we want it to have.
%For example:
vec_M_examples=[linspace(0.07,2,15000) linspace(2,150,25000)];
eval_fun=[];
for ii=1:length(vec_M_examples)
    M_example=vec_M_examples(ii);
    fun=@(M) (integral(imf,0.07,M,'RelTol',1e-6,'AbsTol',1e-6)/(cpdf_max));
    eval_fun(ii)=feval(fun,M_example);
end
%}
load('cpdf')



%In order to do the sampling and then compute how many BHs remain in the
%cluster after the supernova kicks we will use fun_for_mass_histograms.m
%For that we first need to compute the escape velocity:
v_esc=fun_v_esc(r_h,M_cl);
%And then:
[vec_m_bh_initial,vec_m_bh_final]=fun_for_mass_histograms(M_cl,Z,v_esc,k);
figure(1)
h1=histogram(vec_m_bh_initial,20,'FaceColor','b');
h1.BinWidth = 3;
title(['Number of BHs for M_{cl}=' num2str(M_cl) ' M_{sun}, r_h=' num2str(r_h) ' pc & Z=' num2str(Z) '. ' num2str(kick)])
xlabel('M_{BH} (M_{sun})')
ylabel('Number of BHs')
xlim([0 100])
%xticks([0 10 20 30 40 50 60 70 80 90 100])
%set(gca,'YScale','log')
hold on
h2=histogram(vec_m_bh_final,20,'FaceColor','r');
h2.BinWidth = 3;
legend('IMF (before kicks)','after kicks') 
%And now we show the mean mass of BHs before and after the SN kicks:
disp(['FOR THE SECTION ON THE MASS HISTOGRAM: (' num2str(kick) ')'])
disp('                       MEAN MASS OF BHs BEFORE SN KICKS')
mean_m_bh_initial=sum(vec_m_bh_initial)/length(vec_m_bh_initial);
disp(mean_m_bh_initial)
disp('                       MEAN MASS OF BHs AFTER SN KICKS')
mean_m_bh_final=sum(vec_m_bh_final)/length(vec_m_bh_final);
disp(mean_m_bh_final)


%{

%It is also important to study the stochasticity of the sampling method.
%That is why we compute the number stars, BHs and NSs, as well as some
%important fractions for different samplings in order to study these
%variations.

%We initialize the total mass to be 0 before we start "creating" stars:
M_tot=0;

vec_ii=[1:10];
mat_m=[];
N_tot=zeros(1,length(vec_ii));
M_tot=zeros(1,length(vec_ii));
for ii=1:length(vec_ii)
  jj=1;
  vec_m=[];%We will use this vector to obtain N_tot because if we try to do it via the matrix m_mat, as it is padded with 0's,we always get the same N_tot.
  while M_tot(ii)<M_cl
    r=rand;
    kk=find(abs(eval_fun-r)<1e-3);
    m=vec_M_examples(kk(randi(length(kk))));%we use this randi command because for a given r, given the resolution we can ask for, 
    %there might be more than one mass that suits such requirements. This
    %way, if there is more than one possible value, we asign that value
    %randomly.
    M_tot(ii)=M_tot(ii)+m;
    mat_m(ii,jj)=m;
    vec_m(jj)=m;
    jj=jj+1;
   end  
   N_tot(ii)=length(vec_m);
   vec_M_zams_test=linspace(7,40,2000);
   vec_M_bh_test=fun_M_rem(Z,vec_M_zams_test);
   ll=find(abs(vec_M_bh_test-3)<1e-2);
   M_z_min_bh_test=vec_M_zams_test(ll(1));
%And to find the minimum NS mass: (The minimum mass of 1.17 solar masses is
%given by Yudai Suwa, Takashi Yoshida, Masaru Shibata, Hideyuki Umeda & Koh
%Takahashi in "On the minimum mass of neutron stars" in 2009. The problem
%is that we cannot get M_rem=1.17 with the functions presented by Spera, Mapelli & Bressan (2015), and Fryer 
%et al. (2012) for the delayed supernova mechanism because the mass is too low (minimum M_zams of 7 solar masses).
%That is why we set a minimum value of 1.4:
ll=find(abs(vec_M_bh_test-1.4)<1e-2);
M_z_min_ns_test=vec_M_zams_test(ll(1));
%And then we compute the total number of stars of each of them:
N_bh(ii)=sum(vec_m>M_z_min_bh_test);
N_stars(ii)=sum(vec_m<M_z_min_ns_test);
N_ns(ii)=N_tot(ii)-N_bh(ii)-N_stars(ii);

M_bh(ii)=sum(feval(@fun_M_rem,Z,vec_m(find(vec_m>M_z_min_bh_test))));
M_stars(ii)=sum(vec_m(find(vec_m<M_z_min_ns_test)));
M_ns(ii)=M_tot(ii)-M_bh(ii)-M_stars(ii);
end


%{
disp('mean mass of stars')
mean_mass_stars=sum(vec_m)/length(vec_m);
disp(mean_mass_stars)
%}

%Now that we have sampled it different times we have to compute the results
%after the supernova kicks according to the prescription chosen:
for ii=1:length(vec_ii)
%In order to not use so many if statements we are going to find which
%positions in the vector vec_m_initial have a mass smaller than the minimum
%neutron star mass. Because this stars are retained and considering them in
%an if statement only slows the computation:
pos_stars=mat_m(ii,:)<M_z_min_ns_test;%Position of such stars in the vector vec_m_initial. 
%(we get 1's in the positions of the stars that verify the requirement, and 0 for the others)
vec_m_final_i=mat_m(ii,:).*pos_stars;
%But we need to eliminate the 0's and only consider those that are
%different  from 0.
vec_m_final=nonzeros(vec_m_final_i)';
%And the vector of masses for the NS and BHs before considering the
%probabilities for them to be kicked out are:
vec_bh_ns_i=mat_m(ii,:)-vec_m_final_i;
vec_bh_ns=nonzeros(vec_bh_ns_i)';%Vector that contains the initial stars that are (or will be, basically, as these are ZAMS masses) BHs or NSs.
%And now we do an if statement for this stars:
ll=length(vec_m_final); %We initialize ll so that we fill the vector after that position.
%We calculate only once the probability for the initial vector of masses:
    f_ret=(1-feval(@prob_escape,Z,vec_bh_ns,1.4,265,v_esc,k)); 
%It is also important to see that later, in order to really compute the
%number of BHs and NSs we need to create a vector that stores the BHs and
%neutron stars:
vec_bh_ns_final=[];
%For that we initialize the variable nn
nn=1;
%But we need to transform this into the masses that the remnant stars will
%really have after the explosion(not the ZAMS masses that the BHs or NSs previously had):
vec_bh_ns=fun_M_rem(Z,vec_bh_ns);
     for jj=1:length(vec_bh_ns)
         if vec_bh_ns(jj)>3
            rr=rand;
            if rr<=f_ret(jj)
                vec_m_final(ll)=vec_bh_ns(jj);
                vec_bh_ns_final(nn)=vec_bh_ns(jj);
                ll=ll+1;nn=nn+1;
             else
                ll=ll;nn=nn;
             end
         else
            rr=rand;
            if rr<=(1-feval(@prob_esc_ns,265,v_esc))
                vec_m_final(ll)=vec_bh_ns(jj);
                vec_bh_ns_final=vec_bh_ns(jj);
                ll=ll+1;nn=nn+1;
            else
                ll=ll;nn=nn;
            end
         end
     end
     N_tot_final(ii)=length(vec_m_final);
     M_tot_final(ii)=sum(vec_m_final);
     N_bh_final(ii)=sum(vec_bh_ns_final>3);
     N_ns_final(ii)=sum(vec_bh_ns_final<3);
     N_stars_final(ii)=N_tot_final(ii)-N_bh_final(ii)-N_ns_final(ii);
     f_N_bh(ii)=N_bh_final(ii)/N_bh(ii);f_N_ns(ii)=N_ns_final(ii)/N_ns(ii);f_N_stars(ii)=N_stars_final(ii)/N_tot(ii);

     M_bh_final(ii)=sum(vec_bh_ns_final(find(vec_bh_ns_final>3)));
     M_ns_final(ii)=sum(vec_bh_ns_final(find(vec_bh_ns_final<3)));
     M_stars_final(ii)=M_tot_final(ii)-M_bh_final(ii)-M_ns_final(ii);
     f_m_bh(ii)=M_bh_final(ii)/M_tot(ii);f_m_ns(ii)=M_ns_final(ii)/M_ns(ii);f_m_stars=M_stars_final(ii)/M_tot(ii);
end
%And now we can plot this:
figure(2)
plot(vec_ii,N_tot,'-k')
title(['Number of stars in a cluster of M_{cl}=' num2str(M_cl) ' M_{sun}, r_h=' num2str(r_h) ' pc & Z=' num2str(Z)])
xlabel('Different samplings')
ylabel('Number of stars')
hold on
plot(vec_ii,N_tot_final,'-r')
legend('IMF (before kicks)',['after kicks,' num2str(kick)],'Location','southwest(1-feval(@prob_esc_ns,265,mat_v_esc(dd,ff)))')
legend('boxoff')

figure(3)
subplot(2,1,1)
plot(vec_ii,N_bh,'-r')
title(['Number of BHs in a cluster of M_{cl}=' num2str(M_cl) ' M_{sun}, r_h=' num2str(r_h) ' pc & Z=' num2str(Z)])
xlabel('Different samplings')
ylabel('Number of BHs')
hold on
plot(vec_ii,N_bh_final,'-b')
legend('IMF (before kicks)',['after kicks,' num2str(kick)],'Location','southwest')
legend('boxoff')
subplot(2,1,2)
plot(vec_ii,f_N_bh,'-k')
title('Fraction of BHs retained')
xlabel('Different samplings')
ylabel('Fraction of BHs retained')

%And for these calculations we can study the stochastic nature, that is,
%what is the mean value, standard deviations, etc:
%IMF:
disp('                       VARIATIONS IN THE NUMBER OF BHs')
disp('Mean number of initial BHs:')
mean_N_bh_initial=sum(N_bh)/length(vec_ii);
disp(mean_N_bh_initial)
var=(N_bh-mean_N_bh_initial).^2;
sigma_N_bh_initial=sqrt(sum(var)/length(vec_ii));
disp('Standard deviation in the initial number of BHs:')
disp(sigma_N_bh_initial)

%After supernova kicks:
mean_N_bh_final=sum(N_bh_final)/length(vec_ii);
disp('Mean number of BHs after supernova kicks:')
disp(mean_N_bh_final)
var=(N_bh_final-mean_N_bh_final).^2;
sigma_N_bh_final=sqrt(sum(var)/length(vec_ii));
disp('Standard deviation in the number of BHs after supernova kicks:')
disp(sigma_N_bh_final)

%And now we do the same but for the number fraction of black holes:
disp('                       VARIATIONS IN THE NUMBER FRACTION OF BHs RETAINED')
disp('Mean number fraction of BHs retained:')
mean_f_N_bh=sum(f_N_bh)/length(vec_ii);
disp(mean_f_N_bh)
var=(f_N_bh-mean_f_N_bh).^2;
sigma_f_N_bh=sqrt(sum(var)/length(vec_ii));
disp('Standard deviation in the number fraction of BHs retained:')
disp(sigma_f_N_bh)
%}

%And now it would be interesting to do a contour plot for different cluster
%masses and half-mass radii. To do that we use the function
%fun_for_contour_fractions_3.m

%{
%We define the following vectors:
vec_M_cl=linspace(10^4,10^5,10);
vec_r_h=linspace(0.1,15,10);
%vec_v_esc=linspace(5,60,100);
%And with this we form the meshgrid:
[M_cl,r_h]=meshgrid(vec_M_cl,vec_r_h);
mat_v_esc=fun_v_esc(r_h,M_cl);
%So that:
f_N_bh_relative=fun_for_contour_fractions_3(M_cl,Z,r_h,k);
%But we want to express it as a function of r_h and nor mat_v_esc. So we
%basically use the function defined in fun_v_esc.m but rewritten so that it
%yields the half-mass radius.
%r_h=(1/40)*((3/(8*pi))^(1/3))*((M_cl)./((mat_v_esc).^2));
figure(4)
contour(M_cl/(10^5),r_h,f_N_bh_relative);
colorbar
%clabel(C,h)
title(['BH retention fraction. Z=' num2str(Z) '. ' num2str(kick)])
xlabel('M_{cl} (10^5 M_{sun})')
ylabel('r_h (in pc)')
set(gca, 'YScale', 'log')

%set(gca, 'XScale', 'log')

%}
%{
%To obtain the contour plot for all prescriptions at once:
%vec_M_cl=linspace(10^4,10^5,100);
vec_M_cl=logspace(4,6,100);%100 logarithmically spaced numbers.
vec_r_h=linspace(0.1,1.5,100);
%vec_v_esc=linspace(5,60,100);
%And with this we form the meshgrid:
[M_cl,r_h]=meshgrid(vec_M_cl,vec_r_h);
mat_v_esc=fun_v_esc(r_h,M_cl);
fun_for_contour_fractions_all_plots(M_cl,Z,r_h)
%}
%{
%To obtain the results for the mean values of low-mass clusters:
vec_M_cl=linspace(10^4,10^5,100);
%vec_M_cl=logspace(4,5,100);%100 logarithmically spaced numbers.
vec_r_h=linspace(0.1,15,100);
%vec_v_esc=linspace(5,60,100);
%And with this we form the meshgrid:
[M_cl,r_h]=meshgrid(vec_M_cl,vec_r_h);
mat_v_esc=fun_v_esc(r_h,M_cl);
fun_for_contour_fractions_low_mass(M_cl,Z,r_h)
%}
end