%% FUNCTION THAT GIVES THE MASS AND NUMBER FRACTION OF BH BEFORE SUPERNOVA KICKS (AFTER FORMATION)
%Input:
%{
      Z:The metallicity will let us obtain M_zams_min if we consider M_bh_min=4.4
        We get this value from Farr et al.2011. (They say between 4.3 and
        4.5 with 90% confidence.
But the SEVN code cites M_bh_min as 3, and we have to be consistent with
that.
%}
%      alfa1,alfa2,alfa3: in order to use the IMF.
%Output:
%      f_N: number fraction of stars that will become black holes.
%      f_M: mass fraction of stars that will become black holes.
function [M_z_min,f_N,f_m]=fraction_before_kicks(Z,alfa1,alfa2,alfa3)
vec_M_zams=linspace(10,40,2000);
vec_M_bh=fun_M_rem(Z,vec_M_zams);
%Then we find the value for M_zams that gives M_bh=3:
kk=find(abs(vec_M_bh-3)<1e-2);
M_z_min=vec_M_zams(kk(1));
%This way we can compute the number fraction:
%fun is the imf basically.
fun= @(M) (M<0.08).*(M.^(-alfa1)) + (M<0.5 & M>=0.08).*((M.^(-alfa2))*(0.08)) + (M>=0.5).*((M.^(-alfa3))*(0.5));
q1=integral(fun,M_z_min,150,'RelTol',1e-6,'AbsTol',1e-6);
q2=integral(fun,0.07,150,'RelTol',1e-6,'AbsTol',1e-6);
f_N=q1/q2;
%Now, in order to compute the mass fraction we have to multiply the IMF by
%the mass. Meaning:
alfa1=alfa1-1;alfa2=alfa2-1;alfa3=alfa3-1;
%Here fun refers to the imf times the mass (as we are looking for the mass
%fraction).
fun= @(M) (M<0.08).*(M.^(-alfa1)) + (M<0.5 & M>=0.08).*((M.^(-alfa2))*(0.08)) + (M>=0.5).*((M.^(-alfa3))*(0.5));
q1=integral(fun,M_z_min,150,'RelTol',1e-6,'AbsTol',1e-6);
q2=integral(fun,0.07,150,'RelTol',1e-6,'AbsTol',1e-6);
f_m=q1/q2;
end

