%% FUNCTION THAT GIVES THE ESCAPE VELOCITY OF THE CLUSTER AS A FUNCTION OF DENSITY AND MASS OF THE CLUSTER
%{
Description:
             This function computes the escape velocity of a cluster
             according to the results presented by Antonini & Gieles (2019)
%}
%Input:
%      r_h: half-mass radius (in pc)
%      M_cl: cluster mass (in solar masses)

%Output: 
%      v_esc: escape velocity (in km/s)

function v_esc=fun_v_esc(r_h,M_cl)
f_c=1;
v_esc=50*(sqrt(M_cl./(1e5))).*(sqrt(1./r_h))*((3/(8*pi))^(1/6))*f_c;
end

