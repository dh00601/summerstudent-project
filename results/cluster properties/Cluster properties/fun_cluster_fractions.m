%% CLUSTER'S MASS AND NUMBER FRACTIONS
%We rewrite this so it can be run from another script just selecting the
%input variables:
function [results_b_kicks,results_a_kicks]=fun_cluster_fractions(alfa1,alfa2,alfa3,Z,v_esc,k)

%We define in another script the values that we will use in order to stuy
%this mass fractions:

% MASS AND NUMBER FRACTION OF BHs BEFORE SUPERNOVA KICKS (AFTER FORMATION)
%If we use a standard Kroupa IMF with M_min=0.1 solar masses and M_max=150
%solar masses:
%alfa1=0.3;alfa2=1.3;alfa3=2.3;
%We then test it for different metallicites in order to plot the f_N and
%f_M as a function of the metallicity:
%vec_Z=[2e-4,2e-3,5e-3,2e-2];
vec_f_N_b_kicks=zeros(1,length(Z));
vec_f_m_b_kicks=zeros(1,length(Z));
vec_M_z_min=zeros(1,length(Z));
for ii=1:length(Z)
    Zi=Z(ii);
    [vec_M_z_min(ii),vec_f_N_b_kicks(ii),vec_f_m_b_kicks(ii)]=fraction_before_kicks(Zi,alfa1,alfa2,alfa3);
end
disp('FRACTIONS BEFORE SUPERNOVA KICKS')
disp( '                      Z              f_N_b_kicks               f_m_b_kicks                 M_z,min')
results_b_kicks=[Z',vec_f_N_b_kicks',vec_f_m_b_kicks',vec_M_z_min'];
disp(results_b_kicks)

    
% MASS AND NUMBER FRACTION OF BHs AFTER SUPERNOVA KICKS  (AS A FUNCTION OF THE CURRENT MASS OF THE CLUSTER)
%We choose an standard escape velocity of 40 km/s:
%v_esc=20;
%We then test it for different metallicites in order to plot the f_N and
%f_M as a function of the metallicity:
vec_f_N_a_kicks=zeros(1,length(Z));
vec_f_m_a_kicks=zeros(1,length(Z));
vec_M_z_min=zeros(1,length(Z));
for ii=1:length(Z)
    Zi=Z(ii);
    [vec_M_z_min(ii),vec_f_N_a_kicks(ii),vec_f_m_a_kicks(ii)]=fraction_after_kicks(Zi,alfa1,alfa2,alfa3,v_esc,k);
end
disp('FRACTIONS (AS FUNCTIONS OF THE VALUES OF N AND M FOR THE CURRENT CLUSTER)')
disp( '                      Z              f_N_a_kicks               f_m_a_kicks                 M_z,min')
results_a_kicks=[Z',vec_f_N_a_kicks',vec_f_m_a_kicks',vec_M_z_min'];
disp(results_a_kicks)

%{
% MASS AND NUMBER FRACTION OF BHs AFTER SUPERNOVA KICKS  (AS A FUNCTION OF THE CURRENT MASS OF THE CLUSTER)
%We choose an standard escape velocity of 40 km/s:
v_esc=20;
%We then test it for different metallicites in order to plot the f_N and
%f_M as a function of the metallicity:
vec_Z=[2e-4,2e-3,5e-3,2e-2];
vec_f_N_a_kicks=zeros(1,length(vec_Z));
vec_f_m_a_kicks=zeros(1,length(vec_Z));
vec_M_z_min=zeros(1,length(vec_Z));
for ii=1:length(vec_Z)
    Z=vec_Z(ii);
    [vec_M_z_min(ii),vec_f_N_a_kicks(ii),vec_f_m_a_kicks(ii)]=fraction_after_kicks_initial(Z,alfa1,alfa2,alfa3,v_esc);
end
disp('FRACTIONS (AS FUNCTIONS OF THE VALUES OF N AND M FOR THE INITIAL CLUSTER)')
disp( '                      Z              f_N_a_kicks               f_m_a_kicks                 M_z,min')
results_a_kicks=[vec_Z',vec_f_N_a_kicks',vec_f_m_a_kicks',vec_M_z_min'];
disp(results_a_kicks)
%}

% PLOTS OF FRACTIONS AS A FUNCTION OF METALLICITY FOR A KROUPA IMF
vec_Z=linspace(1e-3,1e-2,200);
for ii=1:length(vec_Z)
    Z=vec_Z(ii);
    [vec_M_z_min(ii),vec_f_N_b_kicks(ii),vec_f_m_b_kicks(ii)]=fraction_before_kicks(Z,alfa1,alfa2,alfa3);
    [vec_M_z_min(ii),vec_f_N_a_kicks(ii),vec_f_m_a_kicks(ii)]=fraction_after_kicks(Z,alfa1,alfa2,alfa3,v_esc,k);
end
figure(1);
loglog(vec_Z,vec_f_N_b_kicks,'-r')
xlim([1e-3,1e-2])
title('Number fraction of stars with M<5 M_{sun}')
xlabel('Z')
ylabel('Number fraction')
hold on
loglog(vec_Z,vec_f_N_a_kicks,'-b')
legend('f_N for stars that will be BHs','f_N after kicks (for current GC)')

figure(2)
semilogx(vec_Z,vec_f_m_b_kicks,'-r')
xlim([1e-3,1e-2])
title('Mass fraction of stars with M<5 M_{sun}')
xlabel('Z')
ylabel('Mass fraction')
hold on
semilogx(vec_Z,vec_f_m_a_kicks,'-b')
hold on
legend('f_m for stars that will be BHs','f_m after kicks (for current GC)')
end



