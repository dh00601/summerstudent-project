%% REMNANT MASS (M_rem) AS A FUNCTION OF Z AND M_zams
%{
Description:
             This function computes the mass of the remnant star according to the
             results presented by Spera, Mapelli & Bressan (2015), and Fryer
             et al. (2012) for the delayed supernova mechanism.
%}
%Input:
%      Z:Metallicity
%      M_zams:Zero age main sequence mass (in solar masses)

%Output:
%      M_rem:Mass of the stellar remnant (in solar masses)
function M_rem=fun_M_rem(Z,M_zams)
%We start by computing the M_co via fun_M_co.m:
mat_M_co=fun_M_co(Z,M_zams);
for jj=1:length(mat_M_co(:,1))
for ii=1:length(mat_M_co(jj,:))
    M_co=mat_M_co(jj,ii);
%We then compute m and q as a function of the metallicity Z
if Z>=(2e-3)
    m=1.217;q=1.061;
elseif Z<(1e-3)
    m=-(6.476e2)*Z+(1.911);
    q=(2.300e3)*Z+(11.67);
else
    m=-(43.82)*Z+(1.304);
    q=-(1.296e4)*Z+26.98;
end
%We then compute A1,A2,L,eta:
if Z>=(1e-3)
    A1=(1.340)-((29.46)./(1+(Z./(1.110e-3)).^(2.361)));
    A2=(80.22)-(74.73)*((Z.^(0.965))./((2.720e-3)+(Z.^(0.965))));
    L=(5.683)+((3.533)./(1+(Z./(7.430e-3)).^(1.993)));
    eta=(1.066)-((1.121)./(1+(Z./(2.558e-2)).^(0.609)));
else
    A1=(1.105e5)*Z-(1.258e2);
    A2=(91.56)-(1.957e4)*Z-(1.558e7)*Z.^2;
    L=(1.134e4)*Z-(2.143);
    eta=(3.090e-2)-(22.30)*Z+(7.363e4)*Z.^2;
end
%And for the M_rem:
if Z<=(5e-4)
 %Then we compute p and f:
 p=-2.333+(0.1559)*M_co+(0.2700)*(M_co).^2;
 f=m.*M_co+q;
    if M_co<=5
        M_rem(jj,ii)=max(p,1.27);
    elseif M_co>=10
        M_rem(jj,ii)=min(p,f);
    else
        M_rem(jj,ii)=p;
    end
else
 %We calculate h and f so that:
 h=A1+((A2-A1)./(1+10.^((L-M_co).*eta)));
 f=m.*M_co+q;
    if M_co<=5
        M_rem(jj,ii)=max(h,1.27);
    elseif M_co>=10
        M_rem(jj,ii)=max(h,f);
    else 
        M_rem(jj,ii)=h;
    end
end
end
end
end