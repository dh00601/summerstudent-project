clear all; close all; format compact; format long g;
%% SCRIPT TO PLOT THE RETENTION FRACTION OF BHs FOR THE 4 DIFFERENT KICK PRESCRIPTIONS THAT WE HAVE
%We will load the results from the calculations already done separately:
load('study_variation_final_Z_0001_without_fallback')
clear all
load('study_variation_final_Z_0001')
clear all
load('study_variation_final_Z_0001_fb_265')
%{
clear all
load('study_variation_final_Z_0001_tugboat')
%}