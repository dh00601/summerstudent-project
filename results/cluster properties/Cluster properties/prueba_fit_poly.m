clear all; close all; format compact; format long g;
Z=2e-2;
%But we also need dMzams/dMbh so:
vec_M_zams=linspace(10,100,2000);
vec_M_bh=fun_M_rem(Z,vec_M_zams);
%And then we fit this into a polynomial:
p = polyfit(vec_M_bh,vec_M_zams,15);%In decreasing power
plot(vec_M_bh,vec_M_zams,'-r')
hold on
plot(vec_M_bh,polyval(p,vec_M_bh),'-b')
