clear all; close all; format compact; format long g;
%% CLUSTER PROPERTIES
%% INFORMATION ON INPUT AND OUTPUT VARIABLES
%Input: 
%      alfa1,alfa2,alfa3: exponents of the IMF.
%      Z: metallicity
%      v_esc: escape velocity (in km/s)
%      k: parameter that defines whether there is fallback or not. 
%        k=0 ==> no fallback
%        k=1 ==> fallback considered
%Output:
%      results_b_kicks: mass and number fraction of stars that will be BHs before supernova kicks
%      results_a_kicks: mass and number fraction of BHs after supernova kicks
%% DEFINITION OF INPUT VARIABLES
%For now there are certain values fixed, those are:
%       sig_ns:velocity dispersion for neutron stars, with a value of 265km/s.
%       M_ns: mass of the neutron star, with a value of 1.5 solar masses
%       M_bh_min: minimum mass of a black hole, fixed at 3 solar masses.
%                (we have to follow the SEVN code)

alfa1=0.3;alfa2=1.3;alfa3=2.3;
Z=[2e-4,5e-3,2e-2];
v_esc=30; %One value at a time.
k=0;
%% MASS AND NUMBER FRACTIONS OF BHs IN THE CLUSTER
if k==0
    disp('                                        Without fallback')
end
if k==1
    disp('                                        With fallback')
end
[results_b_kicks,results_a_kicks]=fun_cluster_fractions(alfa1,alfa2,alfa3,Z,v_esc,k);
%% PROBABILITIES OF ESCAPE WITH FALLBACK AND M_co AND M_rem AS A FUNCTION OF M_zams FOR Z=2E-2 AND Z=1E-3
%Cluster_properties_final_version


