clear all; close all; format compact; format long g;
%% EXPULSION OF BLACK HOLES
M_ns=1.5;sig_ns=190;vec_M=linspace(0,40);
%We try different values for the escape velocity:
v_esc=50;
vec_p=[];
for ii=1:length(vec_M)
    M_bh=vec_M(ii);
    p=prob_esc(M_ns,M_bh,sig_ns,v_esc);
    vec_p=[vec_p p];
end
figure (1)
plot(vec_M,vec_p,'-b')
title('Probability of escaping as a function of BH mass')
xlabel('BH mass (in solar masses)')
ylabel('prob(v>vesc)')

hold on

v_esc=40;
vec_p=[];
for ii=1:length(vec_M)
    M_bh=vec_M(ii);
    p=prob_esc(M_ns,M_bh,sig_ns,v_esc);
    vec_p=[vec_p p];
end
figure(1)
plot(vec_M,vec_p,'-r')
hold on

v_esc=30;
vec_p=[];
for ii=1:length(vec_M)
    M_bh=vec_M(ii);
    p=prob_esc(M_ns,M_bh,sig_ns,v_esc);
    vec_p=[vec_p p];
end
figure(1)
plot(vec_M,vec_p,'-k')

%If we want to try and fit it to a Plummer Model:
M=(10^5)*2*10^30;%Total mass of the cluster;
b=5*3.086*10^16;
G=6.67408*10^-11;
v_esc=(1/1000)*sqrt(2*G*M/b);
vec_p=[];
for ii=1:length(vec_M)
    M_bh=vec_M(ii);
    p=prob_esc(M_ns,M_bh,sig_ns,v_esc);
    vec_p=[vec_p p];
end
figure(1)
%plot(vec_M,vec_p,'-g')


legend('vesc=50','vesc=40','vesc=30','Plummer model,M=10^5')


    
