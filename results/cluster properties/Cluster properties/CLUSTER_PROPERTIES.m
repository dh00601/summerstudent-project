%% CLUSTER PROPERTIES
%{
Description: 
  This function does different things. First of all if shows a contour plot
  for the retention fraction of stars in a cluster ,according to the
  probability function derived (as compared to the more 'random' approach
  considered in the SAMPLING.m code where we have defined stars), as a
  function of the mass of the cluster and the half-mass radii for different
  black hole masses. It also plots the escape velocity as a function of 
  these same variables.
  Moreover, it computes the probability of escape as a function of the
  black hole mass and escape velocity.
  Finally, in a last plot, it shows the remnant mass (M_rem) and CO core
  mass (M_co) as a function of M_zams, as well as the probability of
  escape, and the fallback fraction for different masses according to the
  kick prescription chosen.
%}
%Input:
%                Z: Metallicity
%                k: parameter that defines the kick prescription:
%                       k=0 ==> no fallback and reduced velocity dispersion
%                       k=1 ==> fallback considered and reduced velocity
%                       dispersion
%                       K=2 ==> fallback considered and not a reduced
%                       velocity dispersion.

%Output:
%                plots explained in the description of the function (see
%                above)
function CLUSTER_PROPERTIES(Z,k)
if k==0
    kick='W/o fallback,reduced sigma';
elseif k==1 
    kick='W/ fallback,reduced sigma';
elseif k==2
    kick='W/fallback,sigma not reduced';
end


%The first thing to compute is the probability of escape as a function of
%the BH mass and escape velocity according to the metallicity and the kick
%prescription chosen. This way we can use it in conjunction with the next
%plot to know which cluster parameters yield such escape velocity.

%To do that we first compute the minimum ZAMS mass that yields a BH, as now
%we are only interested in studying those stars that become BHs.
vec_M_zams_test=linspace(7,40,2000);
vec_M_bh_test=fun_M_rem(Z,vec_M_zams_test);
ll=find(abs(vec_M_bh_test-3)<1e-2);
M_z_min_bh_test=vec_M_zams_test(ll(1));

%Then we can start doing the contour plot:
vec_M_zams=linspace(M_z_min_bh_test,150,2000);
vec_v_esc=linspace(5,60,2000);
[M_zams,v_esc]=meshgrid(vec_M_zams,vec_v_esc);
M_bh=fun_M_rem(Z,M_zams);
%For the probability we use M_ns=1.4, sig_ns=265km/s. As proposed by Hobbs,
%Lorimer,Lyne and Kramer (2005).
M_ns=1.4;sig_ns=265;
prob=prob_escape(Z,M_zams,M_ns,sig_ns,v_esc,k);
%And then:
figure(1)
contour(M_zams,v_esc,prob,'ShowText','on')
title(['Probability of escape. ' num2str(kick)])
xlabel('M_{BH} (M_{sun})')
ylabel('v_{esc} (km/s)')


%{
vec_M_zams=[10 20 30 40];
p=prob_escape(Z,vec_M_bh(1),M_ns,sig_ns,v_esc,k);f_ret=1-p;
figure(1)
subplot(2,2,1)
contour(M_cl./(10^5),r_h,p,'ShowText','on')
title(['Retention fraction of stars. ' num2str(kick) '. Z=' num2str(Z)])
xlabel('M_5 (M_{cl}/(10^5 M_{sun})')
ylabel('r_h (pc)')
set(gca, 'YScale', 'log')
legend(['M=' num2str(vec_M_bh(1))])

p=prob_escape(Z,vec_M_bh(2),1.4,265,v_esc,k);f_ret=1-p;
subplot(2,2,2)
contour(M_cl./(10^5),r_h,f_ret,'ShowText','on')
title(['Retention fraction of stars. ' num2str(kick) '. Z=' num2str(Z)])
xlabel('M_5 (M_{cl}/(10^5 M_{sun})')
ylabel('r_h (pc)')
set(gca, 'YScale', 'log')
legend(['M=' num2str(vec_M_bh(2))])

p=prob_escape(Z,vec_M_bh(3),1.4,265,v_esc,k);f_ret=1-p;
subplot(2,2,3)
contour(M_cl./(10^5),r_h,f_ret,'ShowText','on')
title(['Retention fraction of stars. ' num2str(kick) '. Z=' num2str(Z)])
xlabel('M_5 (M_{cl}/(10^5 M_{sun})')
ylabel('r_h (pc)')
set(gca, 'YScale', 'log')
legend(['M=' num2str(vec_M_bh(3))])

p=prob_escape(Z,vec_M_bh(3),1.4,265,v_esc,k);f_ret=1-p;
subplot(2,2,4)
contour(M_cl./(10^5),r_h,f_ret,'ShowText','on')
title(['Retention fraction of stars. ' num2str(kick) '. Z=' num2str(Z)])
xlabel('M_5 (M_{cl}/(10^5 M_{sun})')
ylabel('r_h (pc)')
set(gca, 'YScale', 'log')
legend(['M=' num2str(vec_M_bh(4))])

%}

%{
%We then want to plot the escape velocity as a function of the mass of the
%cluster and the radius in parsecs:
M_cl=linspace(10^3,10^6,1000);r_h=linspace(0.2,5,1000);
[M_cl,r_h]=meshgrid(M_cl,r_h);
v_esc=fun_v_esc(r_h,M_cl);
figure(2)
contour(M_cl./(10^5),r_h,v_esc,[10,20,30,40,50,75,100],'ShowText','on')
title('Contour plot for the escape velocity (km/s) as a function of M_5 and r_h')
xlabel('M_5 (M_{cl}/(10^5 M_{sun})')
ylabel('r_h (pc)')


%{
%We then plot the probability of escape for different BH masses and escape
%velocities:
v_esc=linspace(20,200,1000);M_bh=linspace(3,50,1000);
[M_bh,v_esc]=meshgrid(M_bh,v_esc); p=prob_escape(Z,M_bh,M_ns,sig_ns,v_esc,k); 
figure(3)
contour(M_bh,v_esc,p,'ShowText','on')
title(['Probability of escape. ' num2str(kick) '. Z=' num2str(Z)])
xlabel('M_{BH} (in M_{sun})')
ylabel('Escape velocity (in km/s)')
%set(gca, 'YScale', 'log')
%}
%We can also plot some information regarding M_rem, M_co, the probabilities
%of escape and the fallback fraction in one plot by using subplots:
vec_M_zams=linspace(0.07,150,1000);
vec_M_co=zeros(1,length(vec_M_zams));vec_M_rem=zeros(1,length(vec_M_zams));
vec_f_fb=zeros(1,length(vec_M_zams));vec_M_zams=linspace(10,150,2000);
for ii=1:length(vec_M_zams)
M_zams=vec_M_zams(ii);
vec_M_co(ii)=fun_M_co(Z,M_zams);vec_M_rem(ii)=fun_M_rem(Z,M_zams);
vec_f_fb(ii)=fun_fb(Z,M_zams);
end
figure(3)
subplot(2,2,[1,3])
plot(vec_M_zams,vec_M_co,'-k')
%text(15,25,'Z=2e-2')
title(['M_{rem} & M_{co}. Z=' num2str(Z)])
xlabel('M_{zams} (in M_{sun})')
ylabel('M (in M_{sun})')
hold on
plot(vec_M_zams,vec_M_rem,'-r')
hold on
%We find the M_zams for which the f_fb has certain values.
kk05=find(abs(vec_f_fb-0.5)<1e-2);M_zams_0_5=vec_M_zams(kk05(4));M_rem_0_5=fun_M_rem(Z,M_zams_0_5);
kk075=find(abs(vec_f_fb-0.75)<1e-3);M_zams_0_75=vec_M_zams(kk075(1));M_rem_0_75=fun_M_rem(Z,M_zams_0_75);
kk1=find(abs(vec_f_fb-1)<1e-5);M_zams_1=vec_M_zams(kk1(1));M_rem_1=fun_M_rem(Z,M_zams_1);
plot(M_zams_0_5,M_rem_0_5,'k*','MarkerSize',10)
hold on
plot(M_zams_0_75,M_rem_0_75,'ko','MarkerSize',10)
hold on
plot(M_zams_1,M_rem_1,'k+','MarkerSize',10)
line([M_zams_0_5 M_zams_0_5], get(gca, 'ylim'))
line([M_zams_0_75 M_zams_0_75], get(gca, 'ylim'))
line([M_zams_1 M_zams_1], get(gca, 'ylim'))
legend('M_{co}','M_{rem}','f_{fb}=0.5','f_{fb}=0.75','f_{fb}=1')
%We compute probability of escape for different escape velocities with the kick
%prescription chosen:
vec_v_esc=[30 40 50];
for ii=1:length(vec_v_esc)
v_esc=vec_v_esc(ii);
  for jj=1:length(vec_M_rem)
  M_zams=vec_M_zams(jj);M_bh=fun_M_rem(Z,M_zams);
  p(ii,jj)=prob_escape(Z,M_bh,M_ns,sig_ns,v_esc,k);
  end
end
%We then plot the probability of escape as a function of BH mass:
subplot(2,2,2)
plot(vec_M_rem,p(1,:),'-k')
title(['Probability of escape' num2str(kick), ' Z=' num2str(Z)])
xlabel('M_{rem} (in M_{sun})')
ylabel('Probability of escape')
xlim([0 vec_M_rem(end)])
%xticks([0 2.5 5 7.5 10 12.5 15 17.5 20])
hold on
plot(vec_M_rem,p(2,:),'-r')
hold on
plot(vec_M_rem,p(3,:),'-b')
legend('v_{esc}=30','v_{esc}=40','v_{esc}=50')
%And the next thing is to plot the fallback fraction:
subplot(2,2,4)
plot(vec_M_rem,vec_f_fb,'-k','LineWidth',1.2)
title(['Fallback fraction. Z=' num2str(Z)])
xlabel('M_{rem} (in M_{sun})')
ylabel('Fallback fraction')

%}
end