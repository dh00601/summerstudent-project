%% GUIDE FOR THE USE OF THE CODES
%{ 
In order to compute the different magnitudes of interest there are
different codes one needs to consider. Below we describe the important
ones. (There are others, but are very especific, and only used for the
computation of some plots in some order as to put them in the report).
%}

%% ESSENTIAL FUNCTIONS
%{
The following functions are the essential ones
%}
%% prob_escape.m
%{
It computes the probability of escape for a given star.
It requires: 
             fun_M_rem.m
             fun_fb_rem.m
And it gives the option to use three different kick prescriptions with the
change of the variable k, as explained in the function script itself
%}
%% prob_esc_ns.m
%{ 
It computes the probability of escape for a neutron star.
IT does not require any additional function.
%}
%% fun_fb.m
%{
It computes the fallback fraction after a supernova kick.
It requires:
             fun_M_co.m
%}
%% fun_M_co.m
%{
It computes the mass of the CO core
It does not require any additional function.
%}
%% fun_M_rem.m
%{
It computes the remnant mass.
It requires:
             fun_M_co.m
%}
%% fun_v_esc.m
%{
It computes the escape velocity of a cluster.
It does not require any additional function
%}
%% fun_for_contour_fractions_3.m
%{
It gives the number fraction of BHs compared to the initial ones (just the
IMF)
It requires loading cpdf.mat and it is only usable after a meshgrid
command. That is, it only works if the matrix for the escape velocity and
mass of the cluster have the same size.
It requires:
             fun_M_rem.m
             prob_escape.m
             prob_esc_ns.m
%}
%% fun_for_mass_histograms.m
%{
It yields a vector of the final and initial masses of BHs in a cluster.
IMPORTANT: Only works for scalar M_{cl} and v_{esc}, does not work if one
uses vectors as inputs.
%}
%% fun_for_contour_fractions_all_plots
%{
This function allows for the plotting of a contour plot for
the retention fraction of BHs for different stellar cluster masses and
half-mass radii for the three different kick prescriptions at once.
%}
%% ESSENTIAL SCRIPTS
%{
And now some functions that use the ones defined above to compute the main
magnitudes we are interested in.
%}
%% CLUSTER_PROPERTIES.m
%{
This function does different things. First of all if shows a contour plot
  for the retention fraction of stars in a cluster ,according to the
  probability function derived (as compared to the more 'random' approach
  considered in the SAMPLING.m code where we have defined stars), as a
  function of the mass of the cluster and the half-mass radii for different
  black hole masses. It also plots the escape velocity as a function of 
  these same variables.
  Moreover, it computes the probability of escape as a function of the
  black hole mass and escape velocity.
  Finally, in a last plot, it shows the remnant mass (M_rem) and CO core
  mass (M_co) as a function of M_zams, as well as the probability of
  escape, and the fallback fraction for different masses according to the
  kick prescription chosen.
%}
%% SAMPLING.m
%{
 This function does different things. First of all it samples a
  cluster with the specifications chosen and then plots a histogram for the
  different BH masses both before and after supernova kicks. Then it also
  does 100 different samplings (vec_ii can be changed if one wants a
  different number of samplings) in order to show how a different sampling
  affects the results obtained. 
  Then it also plots a contour plot for the chosen kick prescription 
  showing the BH retention fraction for different cluster masses and 
  half-mass radii.

There is also the option to compute the retention fraction and mass
fraction for the three different kick prescriptions by using:
            fun_for_contour_fractions_all_plots.m
And one can also focus on those same results but for lower mass clusters by
using:
               fun_for_contour_fractions_low_mass.m
%}
