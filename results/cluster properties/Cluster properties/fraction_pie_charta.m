clear all; close all; format compact; format long g;
%% PIE CHARTS OF MASS AND NUMBER FRACTIONS FOR CONTINUOUS CASE (NO SAMPLING)
%If we use a Kroupa IMF:
alfa1=0.3;alfa2=1.3;alfa3=2.3;
%We define the metallicity:
Z=2e-2;
%We want fallback:
k=1;
%We define the escape velocity: (in km/s)
v_esc=30;
%% UPON FORMATION (JUST AS A FUNCTION OF THE IMF
%We then get:
[M_z_min,f_N_bh_bef_kicks,f_m_bh_bef_kicks]=fraction_before_kicks(Z,alfa1,alfa2,alfa3);
[f_N_ns_bef_kicks,f_m_ns_bef_kicks]=fraction_before_kicks_ns(Z,alfa1,alfa2,alfa3);
f_N_stars_bef_kicks=1-f_N_bh_bef_kicks-f_N_ns_bef_kicks;
f_m_stars_bef_kicks=1-f_m_bh_bef_kicks-f_m_ns_bef_kicks;
%We plot the number fraction:
figure(1)
labels={'BHs','NSs','stars'};
results_N_bef=[f_N_bh_bef_kicks f_N_ns_bef_kicks f_N_stars_bef_kicks];
pie(results_N_bef,labels)
title('Number fraction (just the IMF)')
%We plot the mass fraction:
figure(2)
labels={'BHs','NSs','stars'};
results_m_bef=[f_m_bh_bef_kicks f_m_ns_bef_kicks f_m_stars_bef_kicks];
pie(results_m_bef,labels)
title('Mass fraction (just the IMF)')
%% AFTER SUPERNOVA KICKS
[M_z_min,f_N_bh_after_kicks,f_m_bh_after_kicks]=fraction_after_kicks(Z,alfa1,alfa2,alfa3,v_esc,k);
[f_N_ns_after_kicks,f_m_ns_after_kicks]=fraction_after_kicks_ns(Z,alfa1,alfa2,alfa3,v_esc,k);
f_N_stars_after_kicks=1-f_N_bh_after_kicks-f_N_ns_after_kicks;
f_m_stars_after_kicks=1-f_m_bh_after_kicks-f_m_ns_after_kicks;
%We plot the number fraction:
figure(3)
labels={'BHs','NSs','stars'};
results_N_aft=[f_N_bh_after_kicks f_N_ns_after_kicks f_N_stars_after_kicks];
pie(results_N_aft,labels)
title('Number fraction (after supernova kicks)')
%We plot the mass fraction:
figure(4)
labels={'BHs','NSs','stars'};
results_m_aft=[f_m_bh_after_kicks f_m_ns_after_kicks f_m_stars_after_kicks];
pie(results_m_aft,labels)
title('Mass fraction (after supernova kicks)')

