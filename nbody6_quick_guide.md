# Quick guide to running nbody6++

## prerequisites/documents/scripts:
* This doc
* nbody6++\_manual.pdf
* nbody6++ itself: todo
* readout scripts: todo 
* Mcluster
* python, pip, a virtualenvironment and having the content of requirements.txt installed

## Tutorial
### General
We will go through the configuration file, and what all the variables mean, but they are also described in nbody6++\_manual.pdf. 

The formatting for the config file is very plain, but we utilise a handy software package called mcluster (@Mcluster) to generate initial settings for nbody6++ simulations. It accepts command line arguments to construct these config files, which saves us a headache in arranging the numbers correctly. We will use a script made by David that does some extra things for you. The usage is explained below (@custom script and usage). After the configuration has been generated, we can start an nbody6 simulation with this configuration. The steps are explained bellow (@feeding config to nbody6))

#### Mcluster
To run mcluster: 
```
./vol/ph/astro/dhendriks/mcluster/mcluster [OPTIONS]
```
It will generate (at least) 2 files, one of which should be used as an input for nbody6
With `mcluster -h` you can see the options that are customizable.

#### Custom script and usage:
To generate a set up:
* Run `<path to mclusterscript dir>/mcluster_script.sh <name of setup>` in the directory you want nbody6 to store your data. 

You want to run the above command in the directory you want the data of nbody6 to be outputted.

#### feeding config to nbody6
IMPORTANT: execute the below commands in a directory designed to store the output. Nbody6++ automatically outputs the files to the directory it has been called from.

To run a simulation we must pass the config file to the executable of nbody6++. Like this:
`<path to nbody6> < <name of setup>.input`

An example would be:
`/vol/ph/astro_code/dhendriks/nbody6 < /vol/ph/astro_code/dhendriks/work_dir/nbody6/default.input`
Based on the input configuration this file will run until it gets terminated (by the code or  by you). 

Optionally, you can do the following command: `screen -S <session name>' and then run '<path to nbody6> < <name of setup>.input` (type `man screen` for some explanation, or look it up on google)

Now, the code runs, and you should leave it alone (or detach the terminal) until it is done.

### Reading out the data:
Nbody6 outputs in a couple of data formats: ascii/plain text, binary, and optionally hdf5. We have configured it to output the main file to hdf5, so that we dont have to manually read out the binaries. Some other files are plaintext so we can access them quite easily. We created 2 example scripts: `python_scripts_for_nbody6/hdf5_readout_nbody6pp_example.py` for reading out hdf5 files, and `python_scripts_for_nbody6/ascii_readout_nbody6pp_example.py` for reading out normal files with pandas.