import h5py
import os
from extra_functions import *

source_dir = '/vol/ph/astro_code/dhendriks/work_dir/nbody6'
filename = 'snap.40_0.h5part'

f = h5py.File(os.path.join(source_dir, filename), 'r')

# For a tree view of the file
for (path, dset) in hdf_recursive(f):
    print(path, dset)

# # List all groups
print("Keys: %s" % f.keys())

a_group_key = list(f.keys())[0]

# Get the data of e.g. step 0
data_step0 = list(f['Step#0'])
print(data_step0)

# get the data of e.g. binaries in step 0
data_step0_binaries = list(f['Step#0']['Binaries'])
print(data_step0_binaries)

# get the data of e.g. the eccentricity of binaries in step 0
data_step0_binaries_ecc = list(f['Step#0']['Binaries']['ECC'])
print(data_step0_binaries_ecc)



