#!/usr TODO: put  correct 

"""
Script to generate a configuration file for nbody6
Based on chap 4 of nbody6++_manual.pdf

todo:
- get some inspiration from mcluster
"""

# Todo: finish defaults.py:
    # Finish input.F entry
    # Finish data.F entry
    # Finish setup.F entry
    # Finish scale.F entry
    # Finish xtrnl0.F entry
    # Finish binpop.F entry
    # Finish hipop.F entry
    # Finish imbhinit.F entry
    # Finish cloud0.F entry

    # Put help texts at every key like above
    # GO through the default values


# Todo: we need a way to handle the logic and give some warnings based on the input
# Go through them to get the formatting right


import json







from config_defaults import defaults

# Make a copy of the defaults
config = defaults.copy()


# Override the values of the defaults config
#print(config['nbody6'])

def display_config(config):
    """
    Function to display the config.
    For now just jsondump that thing. not exactly how i want tho.
    """
    print(json.dumps(config, indent=4))
# display_config(config)


def generate_config(config, outfile):
    """
    Function to write the config output to a file. 
    TODO: Correctly put the logic of the stuff in it. See page 10 of nbody6 manual
    """

    # Write nbody6.F line:
    # Write input.F line:
    # Write data.F line:
    # Write setup.F line:
    # Write scale.F line:
    # Write xtrnl0.F line:
    # Write binpop.F line:
    # Write hipop.F line:
    # Write imbhinit.F line:
    # Write cloud0.F line:

