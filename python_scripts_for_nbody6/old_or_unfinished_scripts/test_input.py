from scipy.io import FortranFile
import struct

fname = '/vol/ph/astro_code/dhendriks/work_dir/nbody6/conf.3_0'
f = FortranFile('/vol/ph/astro_code/dhendriks/work_dir/nbody6/conf.3_0', 'r')

f.read_ints()

# a = f.read_reals(dtype=float)
# print(a)

# import numpy as np
# with open(fname,'rb') as f:
#     for k in range(4):
#         data = np.fromfile(f, dtype=np.float32, count = 2*3)
#         print(np.reshape(data,(2,3)))

# with open(fname, 'rb') as f:
#     while True:
#         raw = f.read(4)
#         if len(raw)!=4:
#             break # ignore the incomplete "record" if any
#         record = struct.unpack("I", raw )
#         print(record)

# with open(fname, 'rb') as f:
#     for line in f:
#         print(f.readline())