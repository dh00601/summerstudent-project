# Ascii readout script for nbody6++ files. Leading it into something that caries the dataframe
# Will prove as an example to other likewise scripts

import os
import pandas as pd

# TODO: put this into a function or a class.

# Point to the source directory.
source_dir = '/vol/ph/astro_code/dhendriks/work_dir/nbody6'
filename = 'sev.83_0'


# Create the header line labels and data line labels
header_line_labels = ['NS', 'TIME[Myr]']
N_LABEL = ['TIME[NB]', 'I', 'NAME', 'K*', 'RI[RC]', 'M[M*]', 'Log10(L[L*])', 'LOG10(RS[R*])' , 'LOG10(Teff[K]']

# Go through file
with open(os.path.join(source_dir, filename), 'r') as f:
    header_1_data = f.readline().split()

    data = []
    for line in f:
        data.append(line.split())

df = pd.DataFrame(data, columns=N_LABEL)
