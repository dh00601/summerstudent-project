import h5py

def hdf_recursive(hdf_obj, prefix=''):
    for key in list(hdf_obj.keys()):
        item = hdf_obj[key]
        path = '{}/{}'.format(prefix, key)
        if isinstance(item, h5py.Dataset):
            yield (path, item)
        elif isinstance(item, h5py.Group):
            yield from hdf_recursive(item, path)